using Linf_voronoi

F = Linf_voronoi.Face[]

d = [ 0., 13., 25., 0., 0., 10., 5., 8., 12., 5., 5., 8. ]
f1 =  [ (0., 0.), (10., 0.), (10., 25.), (0., 25.)]
f2 = [ (0., 0.), (10., 0.), (10., 25.), (0., 25.)]
f3 = [ (0., 0.), (10., 0.), (10., 13.), (0., 13.)]
f4 = [ (0., 0.), (10., 0.), (10., 13.), (0., 13.)]
f5 = [  (0., 0.), (25., 0.), (25., 13.),(0., 13.)]
f6 = [  (0., 0.), (25., 0.), (25., 13.),(0., 13.)]
f7=  [ (5., 5.), (8., 5.), (8., 12.), (5., 12.)]
f8 = [ (5., 5.), (8., 5.), (8., 12.), (5., 12.)]
f9 = [ (5., 5.), (8., 5.), (8., 8.), (5., 8.)]
f10 = [ (5., 5.), (8., 5.), (8., 8.), (5., 8.)]
f11 = [  (5., 5.), (12., 5.), (12., 8.),(5., 8.)]
f12 = [  (5., 5.), (12., 5.), (12., 8.),(5., 8.)]
push!(F, Linf_voronoi.Face(f1, 3, 1, d[1]))
push!(F, Linf_voronoi.Face(f2, 3, -1, d[2]))
push!(F, Linf_voronoi.Face(f3, 2, -1, d[3]))
push!(F, Linf_voronoi.Face(f4, 2, 1, d[4]))
push!(F, Linf_voronoi.Face(f5, 1, 1, d[5]))
push!(F, Linf_voronoi.Face(f6, 1, -1, d[6]))
push!(F, Linf_voronoi.Face(f7, 3, -1, d[7]))
push!(F, Linf_voronoi.Face(f8, 3, 1, d[8]))
push!(F, Linf_voronoi.Face(f9, 2, 1, d[9]))
push!(F, Linf_voronoi.Face(f10, 2, -1, d[10]))
push!(F, Linf_voronoi.Face(f11, 1, -1, d[11]))
push!(F, Linf_voronoi.Face(f12, 1, 1, d[12]))


r = MyRefinery(0.05)
root = subdivide!(F,r)

m = mesh(Float64)

cellProc(m, root, F)

@axl start
@axl m


for f in F
    @axl poly_facet(f)
end

@axl view



#visualize
function cube2(c::Vector{T}, r::T; args...) where T
    m = mesh(T)
    push_vertex!(m,c+[-r,-r,-r])
    push_vertex!(m,c+[r,-r,-r])
    push_vertex!(m,c+[r,r,-r])
    push_vertex!(m,c+[-r,r,-r])

    push_vertex!(m,c+[-r,-r,r])
    push_vertex!(m,c+[r,-r,r])
    push_vertex!(m,c+[r,r,r])
    push_vertex!(m,c+[-r,r,r])

    push_edge!(m, [1,2])
    push_edge!(m, [2,3])
    push_edge!(m, [3,4])
    push_edge!(m, [1,4])
    push_edge!(m, [5,6])
    push_edge!(m, [6,7])
    push_edge!(m, [7,8])
    push_edge!(m, [5,8])
    push_edge!(m, [1,5])
    push_edge!(m, [2,6])
    push_edge!(m, [3,7])
    push_edge!(m, [4,8])

    for arg in args m[arg[1]]=arg[2] end
    return m
end


m = mesh(Float64)
for cell in allleaves(root)
    wd = cell.boundary.widths[1]/2
    c = cube2([cell.boundary.origin[1]+wd, cell.boundary.origin[2]+wd, cell.boundary.origin[3]+wd ], wd)
    if cell.data.out == true
        c[:color] = Color(255, 0,0)
        c[:size] = 0.3
        #@axl c
    elseif cell.data.in == true
        c[:color] = Color(0, 255,0)
        c[:size] = 0.3
        #@axl c
    elseif length(cell.data.int)+length(cell.data.near) ==1
        c[:color] = Color(255, 0,0)
        c[:size] = 0.3
        #@axl c
    elseif length(cell.data.int)+length(cell.data.near) ==4
        c[:color] = Color(0, 0, 255)
        c[:size] = 0.3
        #@axl c
    elseif length(cell.data.int)+length(cell.data.near) >=5 #&& length(cell.data.int)>0
        #println(cell.data.in || cell.data.out)
        c[:color] = Color(255, 0, 0)
        c[:size] = 0.3
        #@axl c
    end
end
