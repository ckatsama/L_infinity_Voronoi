using Linf_voronoi
global K = 3 # number of different closed polygon contours
F =  Vector{Array{Tuple{Float64,Float64}}}(K) # vector to store all the contours

F[1] = [(2.0, 5.0), (0.3, 5.0), (0.3, 7.0), (4.0, 7.0), (4.0, 9.0), (0.0, 9.0), (0.0, 11.0),(3.,11.), (3., 10.),(18., 10.),
(18., 12.) , (24.0, 12.0),(24.0, 8.0),
(17.0, 8.0), (17.0, 6.0), (14.0, 6.0),(14.0, 3.0), (20.0, 3.0), (20.0, 1.0), (16.0, 1.0), (16.0, 2.0), (4.1, 2.),
(4.1, 0.0), (2.0, 0.0)]
F[2] = [(5.0, 14.0), (-2.0,14.0), (-2.,17.), (0.,17.), (0., 16.), (2.,16.), (2.,20.), (-1.,20.), (-1.,22.),
(7., 22.), (7.,20.), (6., 20.), (6., 15.), (5., 15.)]
F[3] = [ (-3.0, -3.0),  (25.0, -3.0),  (25.0, 24.0), (-3.0, 24.0)]

E = Vector{Vector{MySegment}}(K) # vector to store Voronoi sites (polygon edges)
c = 0
for i=1:K
    E[i] = getEdges(F[i],c)
    c += length(E[i])
end

# specify minimum cell size (side length)
# if chosen sufficiently small, algorithm will terminate before reaching
# this cell size
r = MyRefinery(0.0005)

# plane subdivision
root = subdivide!(E, r)

# m is a mesh representing the Voronoi diagram
m = reconstruct!(root, E)

# visualisation:
@axl start

# 1) of the subdivision cells
for c in allleaves(root)
    @axl vis_cell(c)
end

# 2) of the polygon
for f in F
    @axl vis_face(f)
end

# 3) of the Voronoi diagram
@axl vis_voronoi(m)

@axl view
