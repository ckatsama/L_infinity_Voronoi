using Linf_voronoi
global K = 2 # number of different closed polygon contours
F =  Vector{Array{Tuple{Float64,Float64}}}(K) # vector to store all the contours

F[1] = [(0.0,0.0), (2.0, 0.0), (2.0, 14.0), (4.0, 14.0), (4.0, 0.0), (6.0, 0.0), (6.0, 14.20), (8.0, 14.20), (8.0, 0.0), (10.0, 0.0),
       (10.0, 14.0), (12.0, 14.0), (12.0, 0.0), (14.0, 0.0), (14.0, 14.20), (16.0, 14.20), (16.0, 0.0), (18.0, 0.0), (18.0, 21.0),
       (0.0, 21.0)]
F[2] = [(16.0, 18.0), (2.0, 18.0), (2.0, 19.0), (16.0, 19.0)]

E = Vector{Vector{MySegment}}(K) # vector to store Voronoi sites (polygon edges)
c = 0
for i=1:K
    E[i] = getEdges(F[i],c)
    c += length(E[i])
end

# specify minimum cell size (side length)
# if chosen sufficiently small, algorithm will terminate before reaching
# this cell size
r = MyRefinery(0.0005)

# plane subdivision
root = subdivide!(E, r)

# m is a mesh representing the Voronoi diagram
m = reconstruct!(root, E)

# visualisation:
@axl start

# 1) of the subdivision cells
for c in allleaves(root)
    @axl vis_cell(c)
end

# 2) of the polygon
for f in F
    @axl vis_face(f)
end

# 3) of the Voronoi diagram
@axl vis_voronoi(m)

@axl view
