using Linf_voronoi
global K = 2 # number of different closed polygon contours
F =  Vector{Array{Tuple{Float64,Float64}}}(K) # vector to store all the contours

F[1] = [(0.0, 0.0),(2.0, 0.0), (2.0, 2.0), (10.0, 2.0), (10.0, 4.0), (14.0, 4.0), (14.0, 0.0), (18.0, 0.0),
(18.0, 10.0), (12.0, 10.0), (12.0, 15.0), (15.0, 15.0), (15.0, 13.0), (22.0, 13.0), (22.0, 22.0), (-2.0, 22.0), (-2.0, 14.0),
(2.0, 14.0), (2.0, 9.0), (-2.0, 9.0), (-2.0, 5.0), (0.0, 5.0) ]

F[2] = [ (9.0, 7.0),  (6.0, 7.0),  (6.0, 12.0), (8.0, 12.0),  (8.0, 8.0),  (9.0, 8.0)]

E = Vector{Vector{MySegment}}(K) # vector to store Voronoi sites (polygon edges)
c = 0
for i=1:K
    E[i] = getEdges(F[i],c)
    c += length(E[i])
end

# specify minimum cell size (side length)
# if chosen sufficiently small, algorithm will terminate before reaching
# this cell size
r = MyRefinery(0.0005)

# plane subdivision
root = subdivide!(E, r)

# m is a mesh representing the Voronoi diagram
m = reconstruct!(root, E)

# visualisation:
@axl start

# 1) of the subdivision cells
for c in allleaves(root)
    @axl vis_cell(c)
end

# 2) of the polygon
for f in F
    @axl vis_face(f)
end

# 3) of the Voronoi diagram
@axl vis_voronoi(m)

@axl view
