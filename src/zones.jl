# returns true if q is in Z(s), false otherwise
function Zone(s::MySegment, q::Tuple{Float64, Float64})
    m = min(s.v1,s.v2)
    M = max(s.v1, s.v2)
        if q[s.orient+1] < m
            return (m - q[s.orient+1] <= abs(q[2-s.orient] - s.aff)) || (abs(m - q[s.orient+1] - abs(q[2-s.orient] - s.aff)) <= 0.0000000000009)
        elseif q[s.orient+1] <= M
            return true
        else
            return (q[s.orient+1]-M <= abs(q[2-s.orient] - s.aff)) || (abs(q[s.orient+1]-M - abs(q[2-s.orient] - s.aff)) <= 0.00000000000009)
        end
end


# returns true if points are in CCW turn (det(p1,p2,p3)>0) or collinear (det(p1,p2,p3)=0)
function ccw(p1::Tuple{Float64, Float64}, p2::Tuple{Float64, Float64}, p3::Tuple{Float64, Float64})
    res =  (p2[1] - p1[1])*(p3[2] - p1[2]) - (p2[2] - p1[2])*(p3[1] - p1[1])
    if res >= 0
        return true
    else
        return false
    end
end

# returns true if q is in Z*(s), false otherwise
function OrientedZone(s::MySegment, q::Tuple{Float64, Float64})
    return Zone(s,q) && ccw(point_on_segment(s, s.v1), point_on_segment(s,s.v2), q)
end
