module Linf_voronoi

using RegionTrees
using SemiAlgebraicTypes
using StaticArrays
import RegionTrees: AbstractRefinery, needs_refinement, refine_data, adaptivesampling!, HyperRectangle, parent, isleaf, children, vertices, allleaves
using Axl
using DataStructures
export MySegment, getEdges, subdivide!, reconstruct!, visualize!, MyRefinery, allleaves, vis_cell, vis_face, vis_voronoi, subdivide2!, KDTree, searchTree, 
set_rects, get_rect, decomposition!, cellProc, Face, poly_facet, cube2

include("structures.jl") # structures to represent vertices and segments and their constructors
include("edges.jl")
include("zones.jl")
include("decomposition.jl")
include("subdivision.jl")
include("reconstruction.jl")
include("subdivision3d.jl")
include("reconstruction3d.jl")
end
