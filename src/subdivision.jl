using StaticArrays

# computes closest Linf distance of p to segments in segs
function closest_dist_unoriented(p::Tuple{Float64, Float64}, segs::Array{Tuple{Int64,Int64}, 1}, E::Vector{Vector{MySegment}})
    dist = Inf

    for i = 1:length(segs)
        seg = E[segs[i][1]][segs[i][2]]
        if Zone(seg, p) && dist_from_seg(seg, p) < dist
            dist = dist_from_seg(seg, p)
        end
    end

    return dist
end

# returns true if the segment indicated by t intersects the closed cell
function is_intersecting(t::Tuple{Int64,Int64}, cell::Cell, E::Vector{Vector{MySegment}})
    s = E[t[1]][t[2]]
    a = vertices(cell)[1][1+s.orient]
    b = vertices(cell)[1][2-s.orient]
    if (b<= s.aff && s.aff <= b + cell.boundary.widths[1]) && min(s.v1, s.v2) <= a + cell.boundary.widths[1] && a <= max(s.v1, s.v2)
        return true
    else
        return false
    end
end

function is_intersecting(s::MySegment, cell::Cell)
    a = vertices(cell)[1][1+s.orient]
    b = vertices(cell)[1][2-s.orient]
    if (b<= s.aff && s.aff <= b + cell.boundary.widths[1]) && min(s.v1, s.v2) <= a + cell.boundary.widths[1] && a <= max(s.v1, s.v2)
        return true
    else
        return false
    end
end

# returns true if the segment indicated by t intersects the (closed) square with
# corner : left-bottom vertex
# wd : side length
function is_intersecting(t::Tuple{Int64,Int64}, wd::Float64, corner::Tuple{Float64,Float64}, E::Vector{Vector{MySegment}})
    s = E[t[1]][t[2]]
    a = corner[1+s.orient]
    b = corner[2-s.orient]
    if (b<= s.aff && s.aff <= b + wd) && min(s.v1, s.v2) <= a + wd && a <= max(s.v1, s.v2)
        return true
    else
        return false
    end
end

# returns true if the segment indicated by t intersects the open cell
function is_strictly_intersecting(t::Tuple{Int64,Int64}, cell::Cell, E::Vector{Vector{MySegment}})
    s = E[t[1]][t[2]]
    a = vertices(cell)[1][1+s.orient]
    b = vertices(cell)[1][2-s.orient]
    if (b< s.aff && s.aff < b + cell.boundary.widths[1]) && min(s.v1, s.v2) < a + cell.boundary.widths[1] && a < max(s.v1, s.v2)
        return true
    else
        return false
    end
end

function locate(v::StaticArrays.SArray{Tuple{2},Float64,1,2}, sites::Array{Tuple{Int64,Int64},1}, d::Float64, E::Vector{Vector{MySegment}})
    p = (v[1], v[2])
    s = Tuple{Int64,Int64}[]
    output = MySegment[]
    if d == Inf
        return output
    end

    for i=1:length(sites)
        w = E[sites[i][1]][sites[i][2]]
        if Zone(w,p) && dist_from_seg(w, p) == d
                push!(s, sites[i])
        end
    end

    # find consecutive edges
    flag = false
    i = 1
    j = i+1
    while i<= length(s) && !flag
        #j = i+1
        while j<=length(s) && !flag
            if s[i][1] == s[j][1] && abs(s[i][2]- s[j][2])==1
                flag = true
            end
        j +=1
        end
    i +=1
    j = i+1
    end

    if flag == true
    #if length(s) >= 2
        i += -1
        j += -1
        if s[i][2]< s[j][2]
            s1 = E[s[i][1]][s[i][2]]
            s2 = E[s[j][1]][s[j][2]]
        else
            s1 = E[s[j][1]][s[j][2]]
            s2 = E[s[i][1]][s[i][2]]
        end
        #e1 = E[s[1][1]][s[1][2]]
        #if s[1].orient != s[2].orient
           #if s[1].v2 == s[2].aff && s[1].aff == s[2].v1
               if ccw(point_on_segment(s1,s1.v1), point_on_segment(s1,s1.v2), point_on_segment(s2,s2.v2)) # ccw so angle is convex
                   if !OrientedZone(s1,p) || !OrientedZone(s2,p) #eprepe na isuoyn kai ta 2
                       s = MySegment[]
                       return s
                   end
             else # angle is concave
                 if !OrientedZone(s1,p) && !OrientedZone(s2,p) #eprepe na isuoyn kai ta 2
                     s = MySegment[]
                     return s
                 end
            end
        #elseif s[1].v1 == s[2].aff && s[1].aff == s[2].v2
            #   if ccw(point_on_segment(s[2],s[2].v1), point_on_segment(s[2],s[2].v2), point_on_segment(s[1],s[1].v1)) # ccw so angle is convex
            #       if !OrientedZone(s[1],p) || !OrientedZone(s[2],p) #eprepe na isuoyn kai ta 2
            #           s = MySegment[]
            #           return s
            #       end
            #   else # angle is concave
            #       if !OrientedZone(s[1],p) && !OrientedZone(s[2],p) #eprepe na isuoyn kai ta 2
            #           s = MySegment[]
            #           return s
            #       end
            #  end
         # end
      end

    i=1
    while i <=length(s)
        s3 = E[s[i][1]][s[i][2]]
        if !OrientedZone(s3, p)
            deleteat!(s,i)
            i += -1
        end
    i +=1
    end
    for i = 1:length(s)
        push!(output, E[s[i][1]][s[i][2]])
    end
    return output
end


function locate(p::Tuple{Float64,Float64}, sites::Array{Tuple{Int64,Int64},1}, d::Float64, E::Vector{Vector{MySegment}})
    #p = (v[1], v[2])
    s = Tuple{Int64,Int64}[]
    output = MySegment[]
    #for i=1:length(sites)
    #    if OrientedZone(sites[1][i],p) && abs(p.y-sites[1][i].aff) == d
    #            push!(s, sites[1][i])
    #    end
    #end

    #for i=1:length(sites[2])
    #    if OrientedZone(sites[2][i],p) && abs(p.x-sites[2][i].aff) == d
    #            push!(s, sites[2][i])
    #    end
    #end

    if d == Inf
        return output
    end

    for i=1:length(sites)
        w = E[sites[i][1]][sites[i][2]]
        if Zone(w,p) && dist_from_seg(w, p) == d
                push!(s, sites[i])
        end
    end

    # find consecutive edges
    flag = false
    i = 1
    j = i+1
    while i<= length(s) && !flag
        #j = i+1
        while j<=length(s) && !flag
            if s[i][1] == s[j][1] && abs(s[i][2]- s[j][2])==1
                flag = true
            end
        j +=1
        end
    i +=1
    j = i+1
    end

    if flag == true
    #if length(s) >= 2
        i += -1
        j += -1
        if s[i][2]< s[j][2]
            s1 = E[s[i][1]][s[i][2]]
            s2 = E[s[j][1]][s[j][2]]
        else
            s1 = E[s[j][1]][s[j][2]]
            s2 = E[s[i][1]][s[i][2]]
        end
        #e1 = E[s[1][1]][s[1][2]]
        #if s[1].orient != s[2].orient
           #if s[1].v2 == s[2].aff && s[1].aff == s[2].v1
               if ccw(point_on_segment(s1,s1.v1), point_on_segment(s1,s1.v2), point_on_segment(s2,s2.v2)) # ccw so angle is convex
                   if !OrientedZone(s1,p) || !OrientedZone(s2,p) #eprepe na isuoyn kai ta 2
                       s = MySegment[]
                       return s
                   end
             else # angle is concave
                 if !OrientedZone(s1,p) && !OrientedZone(s2,p) #eprepe na isuoyn kai ta 2
                     s = MySegment[]
                     return s
                 end
            end
        #elseif s[1].v1 == s[2].aff && s[1].aff == s[2].v2
            #   if ccw(point_on_segment(s[2],s[2].v1), point_on_segment(s[2],s[2].v2), point_on_segment(s[1],s[1].v1)) # ccw so angle is convex
            #       if !OrientedZone(s[1],p) || !OrientedZone(s[2],p) #eprepe na isuoyn kai ta 2
            #           s = MySegment[]
            #           return s
            #       end
            #   else # angle is concave
            #       if !OrientedZone(s[1],p) && !OrientedZone(s[2],p) #eprepe na isuoyn kai ta 2
            #           s = MySegment[]
            #           return s
            #       end
            #  end
         # end
      end

    i=1
    while i <=length(s)
        s3 = E[s[i][1]][s[i][2]]
        if !OrientedZone(s3, p)
            deleteat!(s,i)
            i += -1
        end
    i +=1
    end
    for i = 1:length(s)
        push!(output, E[s[i][1]][s[i][2]])
    end
    return output
end

mutable struct MyData
    dist::Array{Float64,1}
    vertices::NTuple{4, Array{MySegment,1}}#{4, Array{Tuple{Int64, Int64},1}}
    int::Array{Tuple{Int64, Int64},1}
    near::Array{Tuple{Int64, Int64},1}
    points::Array{Tuple{Tuple{Float64, Float64}, Array{Tuple{Int64, Int64},1}, Int64},1}
    out::Bool
    in::Bool
    MyData() = new()
end


function getsites(cell::Cell)
    hor = MySegment[]
    ver = MySegment[]
    if length(cell.data.int)>0
        for i = 1:length(cell.data.int)
            if cell.data.int[i].orient == 0
                push!(hor, cell.data.int[i])
            else
                push!(ver, cell.data.int[i])
            end
        end
    end
    if length(cell.data.near)>0
        for i = 1:length(cell.data.near)
            if cell.data.near[i].orient == 0
                push!(hor, cell.data.near[i])
            else
                push!(ver, cell.data.near[i])
            end
        end
    end
return (hor, ver)
end


function inOZ(cell::Cell,s::MySegment)
    v1 = (vertices(cell)[1][1], vertices(cell)[1][2])
    v2 = (vertices(cell)[2][1], vertices(cell)[2][2])
    v3 = (vertices(cell)[3][1], vertices(cell)[3][2])
    v4 = (vertices(cell)[4][1], vertices(cell)[4][2])
    return OrientedZone(s,v1) || OrientedZone(s,v2) || OrientedZone(s,v3) || OrientedZone(s,v4) || is_intersecting(s, cell)
end

function refine_cell!(cell::Cell, E::Vector{Vector{MySegment}})
    cell.data.int = Tuple{Int64, Int64}[]
    cell.data.near = Tuple{Int64, Int64}[]

    temp = deepcopy(parent(cell).data.int)
    temp = append!(temp, parent(cell).data.near)
    center = (cell.boundary.origin[1]+cell.boundary.widths[1]/2, cell.boundary.origin[2]+cell.boundary.widths[2]/2)
    if length(parent(cell).data.int) == 0
        temp1 = closest_dist_unoriented(center, temp,E)
        temp2 = locate(center, temp, temp1, E)
        if length(temp2) == 0 #point is out
            d = cell.boundary.widths[1]
        else
        #temp1 = closest_dist_unoriented(center, temp,E)
            d = cell.boundary.widths[1] + temp1
        end
    else
        temp1 = closest_dist_unoriented(center, parent(cell).data.int,E)
        temp2 = locate(center, parent(cell).data.int, temp1, E)
        if length(temp2) == 0 #point is out
            d = cell.boundary.widths[1]
        else
            temp1 = closest_dist_unoriented(center, temp,E)
            d = cell.boundary.widths[1] + temp1
        end
    end

    for i=1:length(temp)
        #temp[i] is a tuple (face, segment number)

        if is_intersecting(temp[i], cell,E)
            #if temp[i] == MySegment(MyPoint(8.0, 3.0), MyPoint(8.0,-1.0))
            #    println("INTERSECT")
            #    println(cell)
            #end
            push!(cell.data.int, temp[i])
        elseif is_intersecting(temp[i], 2*d, (center[1]-d,center[2]-d), E) && inOZ(cell,E[temp[i][1]][temp[i][2]])
            #    println("NEAR")
            #    println(cell)
            #end
            push!(cell.data.near, temp[i])
        end
end
end

#-----For refinement
type MyRefinery <: AbstractRefinery
    tolerance::Float64
end

function find_vor_vertex2(t1::Tuple{Int64, Int64}, t2::Tuple{Int64, Int64}, t3::Tuple{Int64, Int64}, E::Vector{Vector{MySegment}})
    # intersect the bisectors (s1,s2) and (s1, s3)
    # bis of (s1, s2): a1y+b1x+c1=0
    s1 = E[t1[1]][t1[2]]
    s2 = E[t2[1]][t2[2]]
    s3 = E[t3[1]][t3[2]]

    a1 = Float64
    b1 = Float64
    c1 = Float64
    a2 = Float64
    b2 = Float64
    c2 = Float64
    if s1.orient == s2.orient # sites are parallel
        c1 = -s1.aff/2 - s2.aff/2
        if  s1.orient == 0
            a1 = 1.0
            b1 = 0.0
        else
            a1 = 0.0
            b1 = 1.0
        end
    else # sites are perpendicular
        if s1.orient == 0
            a = s2.aff
            b = s1.aff
        else
            a = s1.aff
            b = s2.aff
        end

        if  (s1.v1-s1.v2)*(s2.v1-s2.v2) < 0 # y - x-b+a =0 (1rst and 3rd quadrant)
            a1 = 1.0
            b1 = -1.0
            c1 = -b+a
        else # y +x-b-a =0 (2nd and 4rth quadrant)
            a1 = 1.0
            b1 = 1.0
            c1 = -b -a
        end
    end

    #coefficients of the(s1, s3) bisector
    if s1.orient == s3.orient # sites are parallel
        c2 = -s1.aff/2 - s3.aff/2
        if  s1.orient == 0
            a2 = 1.0
            b2 = 0.0
        else
            a2 = 0.0
            b2 = 1.0
        end
    else # sites are perpendicular

        if s1.orient == 0
            a = s3.aff
            b = s1.aff
        else
            a = s1.aff
            b = s3.aff
        end

        if  (s1.v1-s1.v2)*(s3.v1-s3.v2) < 0 # y - x-b+a =0 (1rst and 3rd quadrant)
            a2 = 1.0
            b2 = -1.0
            c2 = - b + a
        else # y +x-b-a =0 (2nd and 4rth quadrant)
            a2 = 1.0
            b2 = 1.0
            c2 = - b - a
        end
    end

    # now i determine the coordinates of the voronoi vertex

    if a1 == 0 && b1 !=0
        x = - c1 / b1
        y = ( - c2 - b2 * x )  / a2
    elseif a1 != 0 && b1 == 0
        y = -c1/a1
        x = ( - c2 - a2 * y ) / b2
    elseif a1 !=0 && b1 !=0
        x = ( - c2 +  a2 * c1  / a1 ) / ( b2 -  a2 * b1  / a1 )
        y = ( - b1 * x - c1 ) / a1
    end
    return (x,y)
end


function subset(l1::Array{Tuple{Int64, Int64},1}, l2::Array{Tuple{Int64, Int64},1})
    a = findin(l1, l2)
    if length(a) < length(l1)
        return false
    else
        return true
    end
end

function indexInArray(E::Array{MySegment,1}, s::MySegment)
    i =  Inf
    flag = false
    j = 1
    while j <=length(E) && !flag
        if E[j].ind1 == s.ind1 && E[j].ind2 == s.ind2
            i = j
            flag = true
        end
        j +=1
    end
    return i
end

function segm2tuple(S::Array{MySegment, 1}, E::Vector{Vector{MySegment}} )
    output = Tuple{Int64, Int64}[]
    for i =1:length(S)
        s = S[i]
        if indexInArray(E[1], s) != Inf
            push!(output, (1, indexInArray(E[1], s)))
        elseif indexInArray(E[2], s) != Inf
            push!(output, (2, indexInArray(E[2], s)))
        end
    end
    return output
end

function needs_refinement(r::MyRefinery, cell::Cell, E::Vector{Vector{MySegment}})

    if length(cell.data.vertices[1]) == 0 || length(cell.data.vertices[2]) == 0 || length(cell.data.vertices[3]) == 0 ||length(cell.data.vertices[4]) ==0 #at least one vertex is out
        flag = true
        i = 1
        while i <= length(cell.data.int) && flag
            flag = !is_strictly_intersecting(cell.data.int[i],cell,E)
            i +=1
        end
        if flag == true
            cell.data.out = 1
            return false
        end
    end
    if length(cell.data.vertices[1]) == length(cell.data.vertices[2]) == length(cell.data.vertices[3]) == length(cell.data.vertices[4]) == 1 && cell.data.vertices[1] == cell.data.vertices[2] == cell.data.vertices[3] == cell.data.vertices[4]
    # cell is inside a voronoi region
        cell.data.in = 1
        return false
    end
    if length(cell.data.int)+ length(cell.data.near) <= 3
        return false
    elseif length(cell.data.int)+ length(cell.data.near) <= 8
        temp = copy(cell.data.int)
        temp = append!(temp, cell.data.near)
        t1 = temp[1]
        t2 = temp[2]
        flag = true
        if E[t1[1]][t1[2]].orient != E[t2[1]][t2[2]].orient
            t3 = temp[3]
        else
            i = 3
            while i <= length(temp) && flag
                if E[t1[1]][t1[2]].orient != E[temp[i][1]][temp[i][2]].orient
                    t3 = temp[i]
                    flag = false
                end
                i +=1
            end
        end
        if flag == false || E[t1[1]][t1[2]].orient != E[t2[1]][t2[2]].orient
                vertex = find_vor_vertex2(t1, t2, t3,E)
                if OrientedZone(E[t1[1]][t1[2]],vertex) && inCell(vertex,cell)
                    d = dist_from_seg(E[t1[1]][t1[2]],vertex)
                    flag2 = false
                    for t in temp
                        if !OrientedZone(E[t[1]][t[2]],vertex) || (OrientedZone(E[t[1]][t[2]],vertex) && d != dist_from_seg(E[t[1]][t[2]],vertex))
                            flag2 = true
                        end
                    end
                    if flag2 == false
                        return false
                    end
                #else
                #    return true
                end
        end
    # one label set contains all the others and has less than 3 sites - so I will have at most one vertex
    #if length(cell.data.vertices[1]) > 0 && length(cell.data.vertices[2]) >0 && length(cell.data.vertices[3]) >0 && length(cell.data.vertices[4]) > 0
    #    if length(intersect(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[3], cell.data.vertices[4]))== 1 && length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[3], cell.data.vertices[4])) <=3
    #        cell.data.near = deepcopy(union(segm2tuple(cell.data.vertices[1], E), segm2tuple(cell.data.vertices[2], E), segm2tuple(cell.data.vertices[3], E), segm2tuple(cell.data.vertices[4], E)))
    #        cell.data.int = Tuple{Int64, Int64}[]
    #        return false
    #    end
    #end
    end
    if maximum(cell.boundary.widths) > r.tolerance && length(cell.data.int)+ length(cell.data.near) >=4
        return true
    else
        return false
    end
end


function adaptivesampling!(root::Cell, r::AbstractRefinery, E::Vector{Vector{MySegment}})

    refinement_queue = [root]
    while !isempty(refinement_queue)
        cell = pop!(refinement_queue)

        if needs_refinement(r, cell, E)

            #split!(cell, refine_function)
            L1 = deepcopy(cell.data)
            L2 = deepcopy(cell.data)
            L3 = deepcopy(cell.data)
            L4 = deepcopy(cell.data)

            split!(cell, [L1, L2, L3, L4])

            a = Array{Tuple,1}(4)
            a[1] = (1,2)
            a[2] = (1,3)
            a[3] = (2,4)
            a[4] = (3,4)

            temp = deepcopy(cell.data.int)
            temp = append!(temp, cell.data.near)

            children(cell)[1].data.vertices = (MySegment[], MySegment[], MySegment[], MySegment[])
            children(cell)[2].data.vertices = (MySegment[], MySegment[], MySegment[], MySegment[])
            children(cell)[3].data.vertices = (MySegment[], MySegment[], MySegment[], MySegment[])
            children(cell)[4].data.vertices = (MySegment[], MySegment[], MySegment[], MySegment[])

            for k=1:4
                append!(children(cell)[k].data.vertices[k],cell.data.vertices[k])
            end
            # in last_ver3, I changed the cell.data.int to temp in this loop-------------------------------
        #if length(cell.data.int) + length(cell.data.near) <= 10
            #println("using active segments")
            #if length(cell.data.int)>0
            for k=1:4
                i = a[k][1]
                j = a[k][2]

                    # test if point is in or out
                    if length(cell.data.int)>0
                        temp1 = closest_dist_unoriented((vertices(children(cell)[i])[j][1], vertices(children(cell)[i])[j][2]), cell.data.int,E)
                        temp2 = locate(vertices(children(cell)[i])[j], cell.data.int, temp1, E)
                        if length(temp2) == 0 #point is out
                            children(cell)[i].data.dist[j] = 0.0
                            children(cell)[j].data.dist[i] = 0.0
                        else
                            temp1 = closest_dist_unoriented((vertices(children(cell)[i])[j][1], vertices(children(cell)[i])[j][2]), temp,E)
                            children(cell)[i].data.dist[j] = deepcopy(temp1)
                            children(cell)[j].data.dist[i] = deepcopy(temp1)
                            temp2 = locate(vertices(children(cell)[i])[j], temp, children(cell)[i].data.dist[j], E)
                        end
                    else
                        temp1 = closest_dist_unoriented((vertices(children(cell)[i])[j][1], vertices(children(cell)[i])[j][2]), temp,E)
                        temp2 = locate(vertices(children(cell)[i])[j], temp, temp1, E)
                        if length(temp2) == 0 #point is out
                            children(cell)[i].data.dist[j] = 0.0
                            children(cell)[j].data.dist[i] = 0.0
                        else
                            #temp1 = closest_dist_unoriented((vertices(children(cell)[i])[j][1], vertices(children(cell)[i])[j][2]), temp,E)
                            children(cell)[i].data.dist[j] = deepcopy(temp1)
                            children(cell)[j].data.dist[i] = deepcopy(temp1)
                            #temp2 = locate(vertices(children(cell)[i])[j], temp, children(cell)[i].data.dist[j], E)
                        end
                    end
                    #------- there is no reason for the radius to be positive

                    append!(children(cell)[i].data.vertices[j],temp2)
                    append!(children(cell)[j].data.vertices[i],temp2)
            end # for

            # for the central vertex
            if length(cell.data.int) >0
                temp1 = closest_dist_unoriented((vertices(children(cell)[1])[4][1], vertices(children(cell)[1])[4][2]), cell.data.int,E)
                temp2 = locate(vertices(children(cell)[1])[4], cell.data.int, temp1,E)
                if length(temp2)== 0
                    children(cell)[1].data.dist[4] = 0.0
                    children(cell)[2].data.dist[3] = 0.0
                    children(cell)[3].data.dist[2] = 0.0
                    children(cell)[4].data.dist[1] = 0.0
                else
                    temp1 = closest_dist_unoriented((vertices(children(cell)[1])[4][1], vertices(children(cell)[1])[4][2]), temp,E)
                    temp2 = locate(vertices(children(cell)[1])[4], temp, temp1,E)
                    children(cell)[1].data.dist[4] = deepcopy(temp1)
                    children(cell)[2].data.dist[3] = deepcopy(temp1)
                    children(cell)[3].data.dist[2] = deepcopy(temp1)
                    children(cell)[4].data.dist[1] = deepcopy(temp1)
                end
            else
                temp1 = closest_dist_unoriented((vertices(children(cell)[1])[4][1], vertices(children(cell)[1])[4][2]), temp,E)
                temp2 = locate(vertices(children(cell)[1])[4], temp, temp1,E)
                if length(temp2)== 0
                    children(cell)[1].data.dist[4] = 0.0
                    children(cell)[2].data.dist[3] = 0.0
                    children(cell)[3].data.dist[2] = 0.0
                    children(cell)[4].data.dist[1] = 0.0
                else
                    #temp1 = closest_dist_unoriented((vertices(children(cell)[1])[4][1], vertices(children(cell)[1])[4][2]), temp,E)
                    #temp2 = locate(vertices(children(cell)[1])[4], temp, temp1,E)
                    children(cell)[1].data.dist[4] = deepcopy(temp1)
                    children(cell)[2].data.dist[3] = deepcopy(temp1)
                    children(cell)[3].data.dist[2] = deepcopy(temp1)
                    children(cell)[4].data.dist[1] = deepcopy(temp1)
                end
            end
                #temp2 = locate(vertices(children(cell)[1])[4], temp, children(cell)[1].data.dist[4],E)
            append!(children(cell)[1].data.vertices[4],temp2)
            append!(children(cell)[2].data.vertices[3],temp2)
            append!(children(cell)[3].data.vertices[2],temp2)
            append!(children(cell)[4].data.vertices[1],temp2)

            for c in children(cell)
            refine_cell!(c, E)
            end
            append!(refinement_queue, children(cell))
        end
   end
    root
end


function subdivide!(E::Vector{Vector{MySegment}},r::AbstractRefinery)
    # define bounding box
    x_min = Inf
    y_min = Inf
    x_max = -Inf
    y_max = -Inf

    for f in E
        for s in f
            if s.orient && s.aff < x_min
                x_min = s.aff
            end
            if s.orient && s.aff > x_max
                x_max = s.aff
            end
            if !s.orient && s.aff < y_min
                y_min = s.aff
            end
            if !s.orient && s.aff > y_max
                y_max = s.aff
            end
        end
    end
    #x_min = E_v[1].aff
    #x_max = E_v[length(E_h)].aff
    #y_min = E_h[1].aff
    #y_max = E_h[length(E_h)].aff
    l = max(abs(x_max- x_min), abs(y_max - y_min) )

    # Cell which spans from the bottom-left corner and has
    # length l along each axis

    #L = Tuple{Array{MySegment,1}, Array{MySegment, 1}}
    #L = (E_h, E_v)

    d = MyData()

    d.int = Tuple{Int64, Int64}[]
    for j=1:length(E)
        for i=1:length(E[j])
            push!(d.int, (j,i))
        end
    end
    d.out = 0
    d.in = 0
    d.near = Tuple{Int64, Int64}[]
    d.dist = Array{Float64,1}(4)
    d.points = Tuple{Tuple{Float64, Float64}, Array{Tuple{Int64, Int64},1}, Int64}[]
    s1 = MySegment[]
    s2 = MySegment[]
    s3 = MySegment[]
    s4 = MySegment[]
    d.vertices = (s1, s2, s3, s4)

    root = Cell(SVector(x_min-0.5, y_min-0.5), SVector(l+2.,l+2.), d)
    #root = Cell(SVector(x_min, y_min), SVector(l,l), d)

    for i=1:4
        root.data.dist[i] = closest_dist_unoriented((vertices(root)[i][1], vertices(root)[i][2]), root.data.int,E)
        append!(root.data.vertices[i], locate(vertices(root)[i], root.data.int, root.data.dist[i],E))
    end

    tic()
    adaptivesampling!(root, r, E) #, root2)
    toc()
    return root
end

#---------------------USING THE BVH----------------------------------------------------------------
function is_intersecting(s::MySegment, r::Rectangle)
    v = (r.x_min, r.y_min)
    width = abs(r.x_min - r.x_max)
    a = v[1+s.orient]
    b = v[2-s.orient]
    if (b<= s.aff && s.aff <= b + width) && min(s.v1, s.v2) <= a + width && a <= max(s.v1, s.v2)
        #(a <= s.v1 && s.v1 <= a + cell.boundary.widths[1] || a <= s.v2 && s.v2 <= a + cell.boundary.widths[1]) || (b == s.aff &&
            return true
    else
        return false
    end
end

function dist_and_label(root::KDNode, p::Tuple{Float64,Float64})
    d = estimateDist(root, p)
    rectan = findIntersectingRects(root, Rectangle(p[1]-d, p[1]+d, p[2]-d, p[2]+d))
    S = MySegment[]
    for r in rectan
        est = 0.0
        if inside(p[1], p[2], r)
            est =  0.0
        else
            #changed to min
            est = min(min(abs(r.x_min-p[1]), abs(r.x_max-p[1])), min(abs(r.y_min-p[2]), abs(r.y_max-p[2])))
        end
        #println(est)
        if est <=d#estimation(r, p) <= d
            for s in r.segs
               if OrientedZone(s, p) && dist_from_seg(s,p) <= d
                    #println(s)
                    #println(dist_from_seg(s,p)-d < 0)
                    #println("d:", d)
                    if dist_from_seg(s,p) == d
                        #println("sto 1o")
                        if length(findin(S, [s])) == 0
                            push!(S, s)
                        end
                    else
                        #println("fff")
                       d = dist_from_seg(s,p)
                        S = MySegment[]
                        push!(S, s)
                    end
                end
            end
       end
    end
    return (d, S)
end

function subdivide2!(E::Vector{Vector{MySegment}},r::AbstractRefinery, root2::KDNode)
    # define bounding box
    x_min = Inf
    y_min = Inf
    x_max = -Inf
    y_max = -Inf

    for f in E
        for s in f
            if s.orient && s.aff < x_min
                x_min = s.aff
            elseif s.orient && s.aff > x_max
                x_max = s.aff
            elseif !s.orient && s.aff < y_min
                y_min = s.aff
            elseif !s.orient && s.aff > y_max
                y_max = s.aff
            end
        end
    end
    #x_min = E_v[1].aff
    #x_max = E_v[length(E_h)].aff
    #y_min = E_h[1].aff
    #y_max = E_h[length(E_h)].aff
    l = max(abs(x_max- x_min), abs(y_max - y_min) )

    # Cell which spans from the bottom-left corner and has
    # length l along each axis

    #L = Tuple{Array{MySegment,1}, Array{MySegment, 1}}
    #L = (E_h, E_v)

    d = MyData()

    d.int = Tuple{Int64, Int64}[]
    for j=1:length(E)
        for i=1:length(E[j])
            push!(d.int, (j,i))
        end
    end
    d.out = 0
    d.in = 0
    d.near = Tuple{Int64, Int64}[]
    d.dist = Array{Float64,1}(4)
    d.points = Tuple{Tuple{Float64, Float64}, Array{Tuple{Int64, Int64},1}, Int64}[]
    s1 = MySegment[]
    s2 = MySegment[]
    s3 = MySegment[]
    s4 = MySegment[]
    d.vertices = (s1, s2, s3, s4)

    root = Cell(SVector(x_min-0.5, y_min-0.5), SVector(l+2.,l+2.), d)
    #root = Cell(SVector(x_min, y_min), SVector(l,l), d)

    for i=1:4
        root.data.dist[i] = closest_dist_unoriented((vertices(root)[i][1], vertices(root)[i][2]), root.data.int,E)
        append!(root.data.vertices[i], locate(vertices(root)[i], root.data.int, root.data.dist[i],E))
    end

    tic()
    adaptivesampling2!(root, r, E, root2)
    toc()
    return root
end
function refine_cell2!(cell::Cell, E::Vector{Vector{MySegment}}, root2::KDNode)
    cell.data.int = Tuple{Int64, Int64}[]
    cell.data.near = Tuple{Int64, Int64}[]

    temp = deepcopy(parent(cell).data.int)
    #temp = append!(temp, parent(cell).data.near)


    for i=1:length(temp)
        #temp[i] is a tuple (face, segment number)

        if is_intersecting(temp[i], cell,E)
            #if temp[i] == MySegment(MyPoint(8.0, 3.0), MyPoint(8.0,-1.0))
            #    println("INTERSECT")
            #    println(cell)
            #end
            push!(cell.data.int, temp[i])
        end
    end
        #if is_in_radius(vertices(cell)[1], cell.data.dist[1] + 2*cell.boundary.widths[1], temp[i], E) && cell.data.dist[1]!= Inf || is_in_radius(vertices(cell)[2], cell.data.dist[2] + 2*cell.boundary.widths[1], temp[i],E) && cell.data.dist[2]!= Inf || is_in_radius(vertices(cell)[3], cell.data.dist[3] + 2*cell.boundary.widths[1], temp[i],E) && cell.data.dist[3]!= Inf ||is_in_radius(vertices(cell)[4], cell.data.dist[4] + 2*cell.boundary.widths[1], temp[i],E) && cell.data.dist[4]!= Inf
            #if temp[i] == MySegment(MyPoint(8.0, 3.0), MyPoint(8.0,-1.0))
            #    println("NEAR")
            #    println(cell)
            #end
            center = (cell.boundary.origin[1]+cell.boundary.widths[1]/2, cell.boundary.origin[2]+cell.boundary.widths[2]/2)
            if pointIsOut(root2, center)
                rad = cell.boundary.widths[1]
            else
                rad = dist_and_label(root2, center)[1] + cell.boundary.widths[1]
            end
            #println(rad)
            q = Rectangle(center[1]-rad,center[1]+rad,center[2]-rad,center[2]+rad)
            rects = findIntersectingRects(root2, q)
            for r in rects
                for s in r.segs
                    #println("kkkk")
                    if is_intersecting(s, q) && inOZ(cell,s)
                        #if indexInArray(E[1], s) != Inf && length(findin([(1, indexInArray(E[1], s))], cell.data.int)) == 0 && length(findin([(1, indexInArray(E[1], s))], cell.data.near)) == 0
                        #    push!(cell.data.near, (1,indexInArray(E[1], s)))
                        #elseif indexInArray(E[2], s) != Inf && length(findin([(2, indexInArray(E[2], s))], cell.data.int)) == 0 && length(findin([(2, indexInArray(E[2], s))], cell.data.near)) == 0
                        #    push!(cell.data.near, (2,indexInArray(E[2], s)))
                        #end
                        i = 1
                        flag = true
                        while i <= K && flag
                            if indexInArray(E[i], s) != Inf && length(findin([(i, indexInArray(E[i], s))], cell.data.int)) == 0 && length(findin([(i, indexInArray(E[i], s))], cell.data.near)) == 0
                                push!(cell.data.near, (i,indexInArray(E[i], s)))
                                flag = false
                            end
                            i +=1
                        end
                    end
                end
            end
            #println("---------------", length(cell.data.near), "-----------------------------------------------------------------")
end

function adaptivesampling2!(root::Cell, r::AbstractRefinery, E::Vector{Vector{MySegment}}, root2::KDNode, num::Int64)
    #println("lll")
    refinement_queue = [root]
    while !isempty(refinement_queue)
        cell = pop!(refinement_queue)

        if needs_refinement(r, cell, E)

            #split!(cell, refine_function)
            L1 = deepcopy(cell.data)
            L2 = deepcopy(cell.data)
            L3 = deepcopy(cell.data)
            L4 = deepcopy(cell.data)

            split!(cell, [L1, L2, L3, L4])

            a = Array{Tuple,1}(4)
            a[1] = (1,2)
            a[2] = (1,3)
            a[3] = (2,4)
            a[4] = (3,4)

            temp = deepcopy(cell.data.int)
            temp = append!(temp, cell.data.near)

            children(cell)[1].data.vertices = (MySegment[], MySegment[], MySegment[], MySegment[])
            children(cell)[2].data.vertices = (MySegment[], MySegment[], MySegment[], MySegment[])
            children(cell)[3].data.vertices = (MySegment[], MySegment[], MySegment[], MySegment[])
            children(cell)[4].data.vertices = (MySegment[], MySegment[], MySegment[], MySegment[])

            for k=1:4
                append!(children(cell)[k].data.vertices[k],cell.data.vertices[k])
            end
            # in last_ver3, I changed the cell.data.int to temp in this loop-------------------------------
        #if length(cell.data.int) + length(cell.data.near) <= 10
            #println("using active segments")
            #if length(cell.data.int)>0
            for k=1:4
                i = a[k][1]
                j = a[k][2]

                    # test if point is in or out
                    if length(cell.data.int)>0
                        temp1 = closest_dist_unoriented((vertices(children(cell)[i])[j][1], vertices(children(cell)[i])[j][2]), cell.data.int,E)
                        temp2 = locate(vertices(children(cell)[i])[j], cell.data.int, temp1, E)
                        if length(temp2) == 0 #point is out
                            children(cell)[i].data.dist[j] = 0.0
                            children(cell)[j].data.dist[i] = 0.0
                        else
                            temp1 = closest_dist_unoriented((vertices(children(cell)[i])[j][1], vertices(children(cell)[i])[j][2]), temp,E)
                            children(cell)[i].data.dist[j] = deepcopy(temp1)
                            children(cell)[j].data.dist[i] = deepcopy(temp1)
                            temp2 = locate(vertices(children(cell)[i])[j], temp, children(cell)[i].data.dist[j], E)
                        end
                    else
                        temp1 = closest_dist_unoriented((vertices(children(cell)[i])[j][1], vertices(children(cell)[i])[j][2]), temp,E)
                        temp2 = locate(vertices(children(cell)[i])[j], temp, temp1, E)
                        if length(temp2) == 0 #point is out
                            children(cell)[i].data.dist[j] = 0.0
                            children(cell)[j].data.dist[i] = 0.0
                        else
                            #temp1 = closest_dist_unoriented((vertices(children(cell)[i])[j][1], vertices(children(cell)[i])[j][2]), temp,E)
                            children(cell)[i].data.dist[j] = deepcopy(temp1)
                            children(cell)[j].data.dist[i] = deepcopy(temp1)
                            #temp2 = locate(vertices(children(cell)[i])[j], temp, children(cell)[i].data.dist[j], E)
                        end
                    end
                    #------- there is no reason for the radius to be positive

                    append!(children(cell)[i].data.vertices[j],temp2)
                    append!(children(cell)[j].data.vertices[i],temp2)
            end # for

            # for the central vertex
            if length(cell.data.int) >0
                temp1 = closest_dist_unoriented((vertices(children(cell)[1])[4][1], vertices(children(cell)[1])[4][2]), cell.data.int,E)
                temp2 = locate(vertices(children(cell)[1])[4], cell.data.int, temp1,E)
                if length(temp2)== 0
                    children(cell)[1].data.dist[4] = 0.0
                    children(cell)[2].data.dist[3] = 0.0
                    children(cell)[3].data.dist[2] = 0.0
                    children(cell)[4].data.dist[1] = 0.0
                else
                    temp1 = closest_dist_unoriented((vertices(children(cell)[1])[4][1], vertices(children(cell)[1])[4][2]), temp,E)
                    temp2 = locate(vertices(children(cell)[1])[4], temp, temp1,E)
                    children(cell)[1].data.dist[4] = deepcopy(temp1)
                    children(cell)[2].data.dist[3] = deepcopy(temp1)
                    children(cell)[3].data.dist[2] = deepcopy(temp1)
                    children(cell)[4].data.dist[1] = deepcopy(temp1)
                end
            else
                temp1 = closest_dist_unoriented((vertices(children(cell)[1])[4][1], vertices(children(cell)[1])[4][2]), temp,E)
                temp2 = locate(vertices(children(cell)[1])[4], temp, temp1,E)
                if length(temp2)== 0
                    children(cell)[1].data.dist[4] = 0.0
                    children(cell)[2].data.dist[3] = 0.0
                    children(cell)[3].data.dist[2] = 0.0
                    children(cell)[4].data.dist[1] = 0.0
                else
                    #temp1 = closest_dist_unoriented((vertices(children(cell)[1])[4][1], vertices(children(cell)[1])[4][2]), temp,E)
                    #temp2 = locate(vertices(children(cell)[1])[4], temp, temp1,E)
                    children(cell)[1].data.dist[4] = deepcopy(temp1)
                    children(cell)[2].data.dist[3] = deepcopy(temp1)
                    children(cell)[3].data.dist[2] = deepcopy(temp1)
                    children(cell)[4].data.dist[1] = deepcopy(temp1)
                end
            end
                #temp2 = locate(vertices(children(cell)[1])[4], temp, children(cell)[1].data.dist[4],E)
            append!(children(cell)[1].data.vertices[4],temp2)
            append!(children(cell)[2].data.vertices[3],temp2)
            append!(children(cell)[3].data.vertices[2],temp2)
            append!(children(cell)[4].data.vertices[1],temp2)

            for c in children(cell)
                if length(c.data.int)+length(c.data.near) > num
                    refine_cell2!(c, E, root2)
                else
                    refine_cell!(c,E)
                end
            end
            append!(refinement_queue, children(cell))
        end
   end
    root
end
