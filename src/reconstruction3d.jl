function f_1_3(z::Float64, a::Float64, b::Float64, coord::Int64)
    if coord ==1
        return z+b-a
    else
        return z-b+a
    end
end

function f_2_4(z::Float64, a::Float64, b::Float64, coord::Int64)
    if coord ==1
        return -z+b+a
    else
        return -z+b+a
    end
end

function inCell(cell::Cell, p::Tuple{Float64, Float64, Float64})
    return cell.boundary.origin[1] <= p[1] && p[1] <= cell.boundary.origin[1] + cell.boundary.widths[1] && cell.boundary.origin[2] <= p[2] && p[2] <= cell.boundary.origin[2] + cell.boundary.widths[1] && cell.boundary.origin[3] <= p[3] && p[3] <= cell.boundary.origin[3] + cell.boundary.widths[1]
end

function corner(cell::Cell, t1::Int64, t2::Int64, t3::Int64, F::Array{Face, 1})
    output = (Inf,Inf,Inf)
    r1 = F[t1]
    r2 = F[t2]
    r3 = F[t3]
    if length(union(r1.coord, r2.coord, r3.coord)) < 3
        return output
    else
        x = r1
        y= r2
        z=r3
        if r1.coord ==1
            x= r1
        elseif r1.coord ==2
            y=r1
        else
            z=r1
        end
        if r2.coord ==1
            x= r2
        elseif r2.coord ==2
            y=r2
        else
            z=r2
        end
        if r3.coord ==1
            x= r3
        elseif r3.coord ==2
            y=r3
        else
            z=r3
        end
        p = (x.aff, y.aff, z.aff)
        if inCell(cell,p) && Zone(x,p) && Zone(y,p) && Zone(z,p)
            return p
        else
            return output
        end
    end
end


function subset(l1::Array{Int64,1}, l2::Array{Int64,1})
    a = findin(l1, l2)
    if length(a) < length(l1)
        return false
    else
        return true
    end
end


#-----------TRISECTOR---------------------------------------------------------
function create_right_points(pl::Int64, cell::Cell, c::Float64)
    #creates the points on the boundary and in the middle of the intersection of the cell and the plane with constant value c
    # parallel to the pl axis
    P = Tuple{Float64,Float64, Float64}[]
    if pl==1
        push!(P, (c, cell.boundary.origin[2], cell.boundary.origin[3]))
        push!(P, (c, cell.boundary.origin[2]+cell.boundary.widths[2], cell.boundary.origin[3]))
        push!(P, (c, cell.boundary.origin[2]+cell.boundary.widths[2], cell.boundary.origin[3]+cell.boundary.widths[3]))
        push!(P, (c, cell.boundary.origin[2], cell.boundary.origin[3]+cell.boundary.widths[3]))
        push!(P, (c, cell.boundary.origin[2]+cell.boundary.widths[2]/2, cell.boundary.origin[3]+cell.boundary.widths[3]/2))
    elseif pl==2
        push!(P, (cell.boundary.origin[1],c, cell.boundary.origin[3]))
        push!(P, (cell.boundary.origin[1]+cell.boundary.widths[1],c, cell.boundary.origin[3]))
        push!(P, (cell.boundary.origin[1]+cell.boundary.widths[1],c, cell.boundary.origin[3]+cell.boundary.widths[3]))
        push!(P, (cell.boundary.origin[1],c, cell.boundary.origin[3]+cell.boundary.widths[3]))
        push!(P, (cell.boundary.origin[1]+cell.boundary.widths[1]/2,c, cell.boundary.origin[3]+cell.boundary.widths[3]/2))
    else
        push!(P, (cell.boundary.origin[1], cell.boundary.origin[2],c))
        push!(P, (cell.boundary.origin[1]+cell.boundary.widths[1], cell.boundary.origin[2],c))
        push!(P, (cell.boundary.origin[1]+cell.boundary.widths[1], cell.boundary.origin[2]+cell.boundary.widths[2],c))
        push!(P, (cell.boundary.origin[1], cell.boundary.origin[2]+cell.boundary.widths[2],c))
        push!(P, (cell.boundary.origin[1]+cell.boundary.widths[1]/2, cell.boundary.origin[2]+cell.boundary.widths[2]/2,c))
    end
end

function intersection_points_perp(cell::Cell,t1::Int64, t2::Int64, F::Array{Face, 1}, a::Float64, b::Float64, c::Int64, x0::Float64, y0::Float64, e::Int64)
    #a,b, c, x0,y0 are input
    output = Tuple{Float64, Float64, Float64}[]
    r1 = F[t1]
    r2 = F[t2]
    y = y0
    if c == 0 # 1-3 quadrants
        int1 = f_1_3(y, a, b, 2)

        if x0 <= int1 && int1 <= x0 + cell.boundary.widths[1] #it intersects faces 1-2
            x = x0 + cell.boundary.widths[1]
            #@eval g(x)=$expr[1]
            #int2 = g(x)
            int2 = f_1_3(x, a, b, 1)

            p1 = (int1,y0)
            p2 = (x, int2)
            if e ==1
                pp1 = (cell.boundary.origin[1], int1, y0)
                pp2 = (cell.boundary.origin[1], x, int2)
                corner = (cell.boundary.origin[1],a, b)
            elseif e == 2
                pp1 = (int1, cell.boundary.origin[2], y0)
                pp2 = (x, cell.boundary.origin[2], int2)
                corner = (a, cell.boundary.origin[2], b)
            else
                pp1 = (int1,y0,cell.boundary.origin[3] )
                pp2 = (x, int2, cell.boundary.origin[3])
                corner = (a, b, cell.boundary.origin[3])
            end
            if OrientedZone(r1, pp1) && OrientedZone(r1, pp2) && OrientedZone(r2, pp1) && OrientedZone(r2, pp2)
               # n = mesh( [[p1.x, p1.y, 0.0], [p2.x, p2.y, 0.0]], [[1,2]])
               if pp1 != pp2
                      # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                       push!(output,pp1)
                      # push_vertex!(m, [pp2[1], pp2[2], pp2[3]])
                       push!(output,pp2)
               else #p1 == p2
                  # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                   push!(output,pp1)
                end
                #println("printed for perp c1")
            elseif x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1]
               #push_vertex!(m, [a, b, 0.0])
               push!(output,corner)
               if OrientedZone(r1, pp1) && OrientedZone(r2, pp1)
                          push!(output,pp1)
                else
                     push!(output,pp2)
                end
            end
            if e ==1
                pp1 = (cell.boundary.origin[1]+cell.boundary.widths[1], int1, y0)
                pp2 = (cell.boundary.origin[1]+cell.boundary.widths[1], x, int2)
                corner = (cell.boundary.origin[1]+cell.boundary.widths[1],a, b)
            elseif e == 2
                pp1 = (int1, cell.boundary.origin[2]+cell.boundary.widths[1], y0)
                pp2 = (x, cell.boundary.origin[2]+cell.boundary.widths[1], int2)
                corner = (a, cell.boundary.origin[2]+cell.boundary.widths[1], b)
            else
                pp1 = (int1,y0,cell.boundary.origin[3]+cell.boundary.widths[1] )
                pp2 = (x, int2, cell.boundary.origin[3]+cell.boundary.widths[1])
                corner = (a, b, cell.boundary.origin[3]+cell.boundary.widths[1])
            end
            if OrientedZone(r1, pp1) && OrientedZone(r1, pp2) && OrientedZone(r2, pp1) && OrientedZone(r2, pp2)
               # n = mesh( [[p1.x, p1.y, 0.0], [p2.x, p2.y, 0.0]], [[1,2]])
               if pp1 != pp2
                      # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                       push!(output,pp1)
                      # push_vertex!(m, [pp2[1], pp2[2], pp2[3]])
                       push!(output,pp2)
               else #p1 == p2
                  # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                   push!(output,pp1)
                end
                #println("printed for perp c1")
            elseif x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1]
               #push_vertex!(m, [a, b, 0.0])
               push!(output,corner)
               if OrientedZone(r1, pp1) && OrientedZone(r2, pp1)
                          push!(output,pp1)
                else
                     push!(output,pp2)
                end
            end
       else  #i must see if it intersects faces 3-4

        y = y0 +cell.boundary.widths[1]

        int1 = f_1_3(y, a, b, 2)

        if x0 <= int1 && int1 <= x0 + cell.boundary.widths[1] #it intersects faces 3-4
            x = x0

            int2 = f_1_3(x, a, b, 1)

            pp1 = (int1,y0+ cell.boundary.widths[1])
            pp2 = (x, int2)
            if e ==1
                pp1 = (cell.boundary.origin[1], int1, y0+ cell.boundary.widths[1])
                pp2 = (cell.boundary.origin[1], x, int2)
                corner = (cell.boundary.origin[1],a, b)
            elseif e == 2
                pp1 = (int1, cell.boundary.origin[2], y0+ cell.boundary.widths[1])
                pp2 = (x, cell.boundary.origin[2], int2)
                corner = (a, cell.boundary.origin[2], b)
            else
                pp1 = (int1,y0+ cell.boundary.widths[1],cell.boundary.origin[3] )
                pp2 = (x, int2, cell.boundary.origin[3])
                corner = (a, b, cell.boundary.origin[3])
            end

            if OrientedZone(r1, pp1) && OrientedZone(r1, pp2) && OrientedZone(r2, pp1) && OrientedZone(r2, pp2)

                    if pp1 != pp2
                           # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                            push!(output,pp1)
                           # push_vertex!(m, [pp2[1], pp2[2], pp2[3]])
                            push!(output,pp2)
                    else #p1 == p2
                       # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                        push!(output,pp1)
                     end
                #println("printed for perp c4")
            elseif x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1]

                push!(output,corner)
                if OrientedZone(r1, pp1) && OrientedZone(r2, pp1)
                           push!(output,pp1)
                 else
                      push!(output,pp2)
                 end
              end
              if e ==1
                  pp1 = (cell.boundary.origin[1]+ cell.boundary.widths[1], int1, y0+ cell.boundary.widths[1])
                  pp2 = (cell.boundary.origin[1]+ cell.boundary.widths[1], x, int2)
                  corner = (cell.boundary.origin[1]+ cell.boundary.widths[1],a, b)
              elseif e == 2
                  pp1 = (int1, cell.boundary.origin[2]+ cell.boundary.widths[1], y0+ cell.boundary.widths[1])
                  pp2 = (x, cell.boundary.origin[2]+ cell.boundary.widths[1], int2)
                  corner = (a, cell.boundary.origin[2]+ cell.boundary.widths[1], b)
              else
                  pp1 = (int1,y0+ cell.boundary.widths[1],cell.boundary.origin[3]+ cell.boundary.widths[1] )
                  pp2 = (x, int2, cell.boundary.origin[3]+ cell.boundary.widths[1])
                  corner = (a, b, cell.boundary.origin[3]+ cell.boundary.widths[1])
              end

              if OrientedZone(r1, pp1) && OrientedZone(r1, pp2) && OrientedZone(r2, pp1) && OrientedZone(r2, pp2)

                      if pp1 != pp2
                             # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                              push!(output,pp1)
                             # push_vertex!(m, [pp2[1], pp2[2], pp2[3]])
                              push!(output,pp2)
                      else #p1 == p2
                         # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                          push!(output,pp1)
                       end
                  #println("printed for perp c4")
              elseif x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1]

                  push!(output,corner)
                  if OrientedZone(r1, pp1) && OrientedZone(r2, pp1)
                             push!(output,pp1)
                   else
                        push!(output,pp2)
                   end
                end
          end
         end # end of subcase with intersections of faces1-2 , 3-4
    else # c ==1

        int1 = f_2_4(y, a, b, 1)

        if x0 <= int1 && int1 <= x0 + cell.boundary.widths[1] #it intersects faces 1-4
            x = x0

            int2 = f_2_4(x, a, b, 2)

            p1 = (int1,y0)
            p2 = (x, int2)
            if e ==1
                pp1 = (cell.boundary.origin[1], int1, y0)
                pp2 = (cell.boundary.origin[1], x, int2)
                corner = (cell.boundary.origin[1],a, b)
            elseif e == 2
                pp1 = (int1, cell.boundary.origin[2], y0)
                pp2 = (x, cell.boundary.origin[2], int2)
                corner = (a, cell.boundary.origin[2], b)
            else
                pp1 = (int1,y0,cell.boundary.origin[3] )
                pp2 = (x, int2, cell.boundary.origin[3])
                corner = (a, b, cell.boundary.origin[3])
            end

            if OrientedZone(r1, pp1) && OrientedZone(r1, pp2) && OrientedZone(r2, pp1) && OrientedZone(r2, pp2)
                #n = mesh( [[p1.x, p1.y, 0.0], [p2.x, p2.y, 0.0]], [[1,2]])
                if pp1 != pp2
                       # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                        push!(output,pp1)
                       # push_vertex!(m, [pp2[1], pp2[2], pp2[3]])
                        push!(output,pp2)
                else #p1 == p2
                   # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                    push!(output,pp1)
                 end
            elseif x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1]

                push!(output,corner)
                if OrientedZone(r1, pp1) && OrientedZone(r2, pp1)
                           push!(output,pp1)
                 else
                      push!(output,pp2)
                 end
            end
            if e ==1
                pp1 = (cell.boundary.origin[1]+cell.boundary.widths[1], int1, y0)
                pp2 = (cell.boundary.origin[1]+cell.boundary.widths[1], x, int2)
                corner = (cell.boundary.origin[1]+cell.boundary.widths[1],a, b)
            elseif e == 2
                pp1 = (int1, cell.boundary.origin[2]+cell.boundary.widths[1], y0)
                pp2 = (x, cell.boundary.origin[2]+cell.boundary.widths[1], int2)
                corner = (a, cell.boundary.origin[2]+cell.boundary.widths[1], b)
            else
                pp1 = (int1,y0,cell.boundary.origin[3]+cell.boundary.widths[1] )
                pp2 = (x, int2, cell.boundary.origin[3]+cell.boundary.widths[1])
                corner = (a, b, cell.boundary.origin[3]+cell.boundary.widths[1])
            end

            if OrientedZone(r1, pp1) && OrientedZone(r1, pp2) && OrientedZone(r2, pp1) && OrientedZone(r2, pp2)
                #n = mesh( [[p1.x, p1.y, 0.0], [p2.x, p2.y, 0.0]], [[1,2]])
                if pp1 != pp2
                       # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                        push!(output,pp1)
                       # push_vertex!(m, [pp2[1], pp2[2], pp2[3]])
                        push!(output,pp2)
                else #p1 == p2
                   # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                    push!(output,pp1)
                 end
            elseif x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1]

                push!(output,corner)
                if OrientedZone(r1, pp1) && OrientedZone(r2, pp1)
                           push!(output,pp1)
                 else
                      push!(output,pp2)
                 end
            end
        else  #i must see if it intersects faces 2-3

            x = x0 +cell.boundary.widths[1]
            #int1 = eval(expr[2])
            int1 = f_2_4(x, a, b, 2)

            if y0 <= int1 && int1 <= y0 + cell.boundary.widths[1] #it intersects faces 3-2
                y = y0 + cell.boundary.widths[1]
                #int2 = eval(expr[1])
                int2 = f_2_4(y, a, b, 1)

                p1 = (x0+ cell.boundary.widths[1], int1)
                p2 = (int2,y)
                if e ==1
                    pp1 = (cell.boundary.origin[1], x0+cell.boundary.widths[1], int1)
                    pp2 = (cell.boundary.origin[1], int2, y)
                    corner = (cell.boundary.origin[1],a, b)
                elseif e == 2
                    pp1 = (x0+cell.boundary.widths[1], cell.boundary.origin[2], int1)
                    pp2 = (int2, cell.boundary.origin[2], y)
                    corner = (a, cell.boundary.origin[2], b)
                else
                    pp1 = (x0+cell.boundary.widths[1],int1,cell.boundary.origin[3] )
                    pp2 = (int2, y, cell.boundary.origin[3])
                    corner = (a, b, cell.boundary.origin[3])
                end

                if OrientedZone(r1, pp1) && OrientedZone(r1, pp2) && OrientedZone(r2, pp1) && OrientedZone(r2, pp2)
                    if pp1 != pp2
                           # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                            push!(output,pp1)
                           # push_vertex!(m, [pp2[1], pp2[2], pp2[3]])
                            push!(output,pp2)
                    else #p1 == p2
                       # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                        push!(output,pp1)
                     end
                elseif x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1]

                    push!(output,corner)
                    if OrientedZone(r1, pp1) && OrientedZone(r2, pp1)
                               push!(output,pp1)
                     else
                          push!(output,pp2)
                     end
                end
                if e ==1
                    pp1 = (cell.boundary.origin[1]+cell.boundary.widths[1], x0+cell.boundary.widths[1], int1)
                    pp2 = (cell.boundary.origin[1]+cell.boundary.widths[1], int2, y)
                    corner = (cell.boundary.origin[1]+cell.boundary.widths[1],a, b)
                elseif e == 2
                    pp1 = (x0+cell.boundary.widths[1], cell.boundary.origin[2]+cell.boundary.widths[1], int1)
                    pp2 = (int2, cell.boundary.origin[2]+cell.boundary.widths[1], y)
                    corner = (a, cell.boundary.origin[2]+cell.boundary.widths[1], b)
                else
                    pp1 = (x0+cell.boundary.widths[1],int1,cell.boundary.origin[3]+cell.boundary.widths[1] )
                    pp2 = (int2, y, cell.boundary.origin[3]+cell.boundary.widths[1])
                    corner = (a, b, cell.boundary.origin[3]+cell.boundary.widths[1])
                end

                if OrientedZone(r1, pp1) && OrientedZone(r1, pp2) && OrientedZone(r2, pp1) && OrientedZone(r2, pp2)
                    if pp1 != pp2
                           # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                            push!(output,pp1)
                           # push_vertex!(m, [pp2[1], pp2[2], pp2[3]])
                            push!(output,pp2)
                    else #p1 == p2
                       # push_vertex!(m, [pp1[1], pp1[2], pp1[3]])
                        push!(output,pp1)
                     end
                elseif x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1]

                    push!(output,corner)
                    if OrientedZone(r1, pp1) && OrientedZone(r2, pp1)
                               push!(output,pp1)
                     else
                          push!(output,pp2)
                     end
                end
            end
        end # end of subcases with intesections of faces 2-3 or 1-4
   end  # end of if c==0 or 1
  return output
end

function intersection_points(cell::Cell,t1::Int64, t2::Int64, F::Array{Face, 1})
    output = Tuple{Float64, Float64, Float64}[]
    r1 = F[t1]
    r2 = F[t2]
    if r1.coord == r2.coord # sites are parallel
    c = r1.aff/2 + r2.aff/2
    if  cell.boundary.origin[r1.coord] <= c && c <= cell.boundary.origin[r1.coord]+ cell.boundary.widths[r1.coord]
        P = create_right_points(r1.coord, cell, c)
        push!(output,P[1])
        push!(output,P[2])
        push!(output,P[3])
        push!(output,P[4])
    end
    else # faces perpendicular
    a = Float64
    b = Float64
    c = Int64
    x0 = Float64
    y0 = Float64
    z0 = Float64
    if union(r1.coord, r2.coord) == [1,2] || union(r1.coord, r2.coord) == [2,1] # bisector of x = x.aff , y = y.aff
        if r1.coord == 1 # x= x.aff
            a=r1.aff
            b= r2.aff
            x0 = cell.boundary.origin[1]
            y0 = cell.boundary.origin[2]

            if r1.dir * r2.dir >0
                c=0
            else
                c=1
            end
        else #y = y.aff
            a=r2.aff
            b= r1.aff
            x0 = cell.boundary.origin[1]
            y0 = cell.boundary.origin[2]
            if r1.dir * r2.dir >0
                c=0
            else
                c=1
            end
        end
            ar = intersection_points_perp(cell,t1,t2,F,a,b,c,x0,y0,3)
            append!(output,ar)
    elseif union(r1.coord, r2.coord) == [1,3] || union(r1.coord, r2.coord) == [3,1] # bisector of x = x0 , z = z0
        if r1.coord == 1 # x= x.aff
            a=r1.aff
            b= r2.aff
            x0 = cell.boundary.origin[1]
            z0 = cell.boundary.origin[3]
            if r1.dir * r2.dir >0
                c=0
            else
                c=1
            end
        else #y = y.aff
            a=r2.aff
            b= r1.aff
            x0 = cell.boundary.origin[1]
            z0 = cell.boundary.origin[3]
            if r1.dir * r2.dir >0
                c=0
            else
                c=1
            end
        end
        ar = intersection_points_perp(cell,t1,t2,F,a,b,c,x0,z0,2)
        append!(output,ar)
    else #union(r1.coord, r2.coord) == [2,3] || union(r1.coord, r2.coord) == [3,2] # bisector of y = y0 , z = z0
        if r1.coord == 2 # y= y.aff
            a=r1.aff
            b= r2.aff
            y0 = cell.boundary.origin[2]
            z0 = cell.boundary.origin[3]
            if r1.dir * r2.dir >0
                c=0
            else
                c=1
            end
        else #y = y.aff
            a=r2.aff
            b= r1.aff
            y0 = cell.boundary.origin[2]
            z0 = cell.boundary.origin[3]
            if r1.dir * r2.dir >0
                c=0
            else
                c=1
            end
        end
        ar = intersection_points_perp(cell,t1,t2,F,a,b,c,y0,z0,1)
        append!(output,ar)
    end
    end
    return output
end

function trisector!(cell::Cell, t1::Int64, t2::Int64, t3::Int64,F::Array{Face, 1})
    r1 = F[t1]
    r2 = F[t2]
    r3 = F[t3]
    # p1 and p2 are the endpoints of the voronoi edge
    p1 = [Inf, Inf, Inf]
    p2 = [Inf, Inf, Inf]
    if length(union(r1.coord, r2.coord, r3.coord))==2
        if r1.coord == r2.coord
            p1[r1.coord] = (r1.aff +r2.aff)/2
            p2[r1.coord] = (r1.aff +r2.aff)/2
            p1[r3.coord] = r3.dir*abs(p1[r1.coord]-r1.aff) + r3.aff
            p2[r3.coord] = r3.dir*abs(p1[r1.coord]-r1.aff) + r3.aff
            coord = setdiff([1,2,3],union(r1.coord, r2.coord, r3.coord))
            p1[coord] = cell.boundary.origin[coord]
            p2[coord] = cell.boundary.origin[coord] +cell.boundary.widths[1]
        elseif r1.coord == r3.coord
            p1[r1.coord] = (r1.aff +r3.aff)/2
            p2[r1.coord] = (r1.aff +r3.aff)/2
            p1[r2.coord] = r2.dir*abs(p1[r1.coord]-r1.aff) + r2.aff
            p2[r2.coord] = r2.dir*abs(p1[r1.coord]-r1.aff) + r2.aff
            coord = setdiff([1,2,3],union(r1.coord, r3.coord, r2.coord))
            p1[coord] = cell.boundary.origin[coord]
            p2[coord] = cell.boundary.origin[coord] +cell.boundary.widths[1]
        else #r2.coord == r3.coord
            p1[r2.coord] = (r2.aff +r3.aff)/2
            p2[r2.coord] = (r2.aff +r3.aff)/2
            p1[r1.coord] = r1.dir*abs(p1[r2.coord]-r2.aff) + r1.aff
            p2[r1.coord] = r1.dir*abs(p1[r2.coord]-r2.aff) + r1.aff
            coord = setdiff([1,2,3],union(r1.coord, r2.coord, r3.coord))
            p1[coord] = cell.boundary.origin[coord]
            p2[coord] = cell.boundary.origin[coord] +cell.boundary.widths[1]
        end

        p1 = (p1[1], p1[2], p1[3])
        p2 = (p2[1], p2[2], p2[3])
        if inCell(cell,p1) && inCell(cell,p2) && OrientedZone(r1, p1) && OrientedZone(r2, p1) && OrientedZone(r3, p1) && OrientedZone(r1, p2) && OrientedZone(r2, p2) && OrientedZone(r3, p2)
            if p1 != p2
                push!(cell.data.points, (p1, [t1,t2,t3], 0))
                push!(cell.data.points,  (p2, [t1,t2,t3], 0))
                push!(cell.data.cpoints,  ((p1.+p2)./2, [t1,t2,t3], 0))
            else
                # this cannot happen: if I have one intersection point, then there must be a voronoi vertex; but a
                # voronoi vertex is defined by three planes orthogonal per 2
                push!(cell.data.points, (p1, [t1,t2,t3], 0))
            end

            # now I have to all the intersection points of the bisectors with the cell
            #=I1 = intersection_points(cell, t1,t2,F)
            for p in I1
                if p !=p1 && p!=p2 && OrientedZone(r1, p) && OrientedZone(r2, p) && (!Zone(r3,p) || (Zone(r3,p) && abs(p[r3.coord]- r3.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t2], 0))
                end
            end
            I2 = intersection_points(cell, t1,t3,F)
            for p in I2
                if p !=p1 && p!=p2 && OrientedZone(r1, p) && OrientedZone(r3, p) && (!Zone(r2,p) || (Zone(r2,p) && abs(p[r2.coord]- r2.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t3], 0))
                end
            end
            I3 = intersection_points(cell, t2,t3,F)
            for p in I3
                if p !=p1 && p!=p2 && OrientedZone(r2, p) && OrientedZone(r3, p) && (!Zone(r1,p) || (Zone(r1,p) && abs(p[r1.coord]- r1.aff)> abs(p[r2.coord]- r2.aff)))
                    push!(cell.data.points, (p, [t2,t3], 0))
                end
            end =#
        else # there is no voronoi edge
            #bisector!(cell, t1,t2,t3,F)
            #bisector!(cell, t1,t3,t2,F)
            #bisector!(cell, t2,t3,t1,F)
        end

    elseif length(union(r1.coord, r2.coord, r3.coord))==3
        output = Tuple{Float64,Float64, Float64}[]
        p = [Inf, Inf, Inf]
        if r1.coord ==1
            x= r1
        elseif r1.coord ==2
            y=r1
        else
            z=r1
        end
        if r2.coord ==1
            x= r2
        elseif r2.coord ==2
            y=r2
        else
            z=r2
        end
        if r3.coord ==1
            x= r3
        elseif r3.coord ==2
            y=r3
        else
            z=r3
        end
  #-----------------------------
  #if length(union(cell.data.vertices[1], cell.data.vertices[3], cell.data.vertices[5], cell.data.vertices[7])) >=1 && length(union(cell.data.vertices[1], cell.data.vertices[3], cell.data.vertices[5], cell.data.vertices[7], cell.data.int )) >= 3 #x = xmin
      p[1] = cell.boundary.origin[1]
      c = abs(p[1]-x.aff)
      p[2] = y.dir*c+y.aff
      p[3] = z.dir*c+ z.aff
      if OrientedZone(x, (p[1], p[2], p[3])) && OrientedZone(y, (p[1], p[2], p[3])) && OrientedZone(z, (p[1], p[2], p[3]))
      push!(output, (p[1], p[2], p[3]))
      end
  #if length(union(cell.data.vertices[2], cell.data.vertices[4], cell.data.vertices[6], cell.data.vertices[8])) >=1 && length(union(cell.data.vertices[2], cell.data.vertices[4], cell.data.vertices[6], cell.data.vertices[8], cell.data.int )) >= 3 # x = xmax
      p[1] = cell.boundary.origin[1]+cell.boundary.widths[1]
      c = abs(p[1]-x.aff)
      p[2] = y.dir*c+ y.aff
      p[3] = z.dir*c+z.aff
      if OrientedZone(x, (p[1], p[2], p[3])) && OrientedZone(y, (p[1], p[2], p[3])) && OrientedZone(z, (p[1], p[2], p[3]))
      push!(output, (p[1], p[2], p[3]))
      end
  #if length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[5], cell.data.vertices[6])) >= 1&& length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[5], cell.data.vertices[6], cell.data.int )) >= 3 # y = ymin
      p[2] = cell.boundary.origin[2]
      c = abs(p[2]-y.aff)
      p[3] = z.dir*c+ z.aff
      p[1] = x.dir*c+ x.aff
      if OrientedZone(x, (p[1], p[2], p[3])) && OrientedZone(y, (p[1], p[2], p[3])) && OrientedZone(z, (p[1], p[2], p[3]))
      push!(output, (p[1], p[2], p[3]))
      end
  #if length(union(cell.data.vertices[3], cell.data.vertices[4], cell.data.vertices[7], cell.data.vertices[8]))>=1 && length(union(cell.data.vertices[3], cell.data.vertices[4], cell.data.vertices[7], cell.data.vertices[8], cell.data.int )) >= 3 # y = ymax
      p[2] = cell.boundary.origin[2]+cell.boundary.widths[1]
      c = abs(p[2]-y.aff)
      p[3] = z.dir*c+ z.aff
      p[1] = x.dir*c+ x.aff
      if OrientedZone(x, (p[1], p[2], p[3])) && OrientedZone(y, (p[1], p[2], p[3])) && OrientedZone(z, (p[1], p[2], p[3]))
      push!(output, (p[1], p[2], p[3]))
      end
  #if length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[3], cell.data.vertices[4])) >=1 && length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[3], cell.data.vertices[4], cell.data.int )) >= 3 # z = zmin
      p[3] = cell.boundary.origin[3]
      c = abs(p[3]-z.aff)
      p[2] = y.dir*c+ y.aff
      p[1] = x.dir*c+ x.aff
      if OrientedZone(x, (p[1], p[2], p[3])) && OrientedZone(y, (p[1], p[2], p[3])) && OrientedZone(z, (p[1], p[2], p[3]))
      push!(output, (p[1], p[2], p[3]))
      end
  #if length(union(cell.data.vertices[5], cell.data.vertices[6], cell.data.vertices[7], cell.data.vertices[8])) >=1 && length(union(cell.data.vertices[5], cell.data.vertices[6], cell.data.vertices[7], cell.data.vertices[8], cell.data.int )) >= 3 # z = zmax
      p[3] = cell.boundary.origin[3]+cell.boundary.widths[1]
      c = abs(p[3]-z.aff)
      p[2] = y.dir*c+ y.aff
      p[1] = x.dir*c+ x.aff
      if OrientedZone(x, (p[1], p[2], p[3])) && OrientedZone(y, (p[1], p[2], p[3])) && OrientedZone(z, (p[1], p[2], p[3]))
      push!(output, (p[1], p[2], p[3]))
      end
        i = 1
        while i <= length(output)
            if !inCell(cell, output[i]) || !OrientedZone(x, output[i]) || !OrientedZone(y, output[i]) || !OrientedZone(z, output[i])
                deleteat!(output, i)
                i = i-1
            end
            i +=1
        end

        output = unique(output)

        if length(output)>0 #there is a voronoi edge
            if length(output) == 1 # there must be a corner
                if inCell(cell, (x.aff, y.aff, z.aff)) && output[1] != (x.aff, y.aff, z.aff)
                    push!(cell.data.points, ((x.aff, y.aff, z.aff), [t1,t2,t3], 0))
                    push!(cell.data.points, (output[1], [t1,t2,t3], 0))
                else
                    # this cannot happen (?)
                    push!(cell.data.points, (output[1], [t1,t2,t3], 0))
                end
            else
                push!(cell.data.points, (output[1], [t1,t2,t3], 0))
                push!(cell.data.points,  (output[2], [t1,t2,t3], 0))
                #push!(cell.data.cpoints,  ((output[1].+output[2])./2, [t1,t2,t3], 0))
            end
            # now I have to find all the intersection points of the bisectors with the cell
            # changed orientedZone ---->>> to Zone
            #p1 = output[1]
            #p2 = output[2]
            #=
            I1 = intersection_points(cell, t1,t2,F)
            for p in I1
                if length(findin(output,p))==0 && OrientedZone(r1, p) && OrientedZone(r2, p) && (!Zone(r3,p) || (Zone(r3,p) && abs(p[r3.coord]- r3.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t2], 0))
                end
            end
            I2 = intersection_points(cell, t1,t3,F)
            for p in I2
                if length(findin(output,p))==0 && OrientedZone(r3, p) && (!Zone(r2,p) || (Zone(r2,p) && abs(p[r2.coord]- r2.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t3], 0))
                end
            end
            I3 = intersection_points(cell, t2,t3,F)
            for p in I3
                if length(findin(output,p))==0 && OrientedZone(r2, p) && OrientedZone(r3, p) && (!Zone(r1,p) || (Zone(r1,p) && abs(p[r1.coord]- r1.aff)> abs(p[r2.coord]- r2.aff)))
                    push!(cell.data.points, (p, [t2,t3], 0))
                end
            end
            =#
        else # there is no voronoi edge
            #bisector!(cell, t1,t2,t3,F)
            #bisector!(cell, t1,t3,t2,F)
            #bisector!(cell, t2,t3,t1,F)
        end

    end # if
end


function degen_trisector!(cell::Cell, t1::Int64, t2::Int64, t3::Int64,t4::Int64, F::Array{Face, 1})
    r1 = F[t1]
    r2 = F[t2]
    r3 = F[t3]
    r4 = F[t4]
    p1 = [Inf, Inf, Inf]
    p2 = [Inf, Inf, Inf]
    if r1.coord == r2.coord
        p1[r1.coord] = (r1.aff +r2.aff)/2
        p2[r1.coord] = (r1.aff +r2.aff)/2
        p1[r3.coord] = (r3.aff +r4.aff)/2
        p2[r3.coord] = (r3.aff +r4.aff)/2
        coord = setdiff([1,2,3],union(r1.coord, r2.coord, r3.coord))
        p1[coord] = cell.boundary.origin[coord]
        p2[coord] = cell.boundary.origin[coord] +cell.boundary.widths[1]
    elseif r1.coord == r3.coord
        p1[r1.coord] = (r1.aff +r3.aff)/2
        p2[r1.coord] = (r1.aff +r3.aff)/2
        p1[r2.coord] = (r2.aff +r4.aff)/2
        p2[r2.coord] = (r2.aff +r4.aff)/2
        coord = setdiff([1,2,3],union(r1.coord, r3.coord, r2.coord))
        p1[coord] = cell.boundary.origin[coord]
        p2[coord] = cell.boundary.origin[coord] +cell.boundary.widths[1]
    else #r1.coord == r4.coord
        p1[r1.coord] = (r1.aff +r4.aff)/2
        p2[r1.coord] = (r1.aff +r4.aff)/2
        p1[r2.coord] = (r3.aff +r2.aff)/2
        p2[r2.coord] = (r3.aff +r2.aff)/2
        coord = setdiff([1,2,3],union(r1.coord, r2.coord, r3.coord))
        p1[coord] = cell.boundary.origin[coord]
        p2[coord] = cell.boundary.origin[coord] +cell.boundary.widths[1]
    end
    p1 = (p1[1], p1[2], p1[3])
    p2 = (p2[1], p2[2], p2[3])
    if OrientedZone(r1, p1) && OrientedZone(r2, p1) && OrientedZone(r3, p1) && OrientedZone(r4, p1) && OrientedZone(r1, p2) && OrientedZone(r2, p2) && OrientedZone(r3, p2) && OrientedZone(r4, p2)
        push!(cell.data.points,  ((p1.+p2)./2, [t1,t2,t3, t4], 0))
    end
end

function trisector!(cell::Cell, t1::Int64, t2::Int64, t3::Int64,t4::Int64, F::Array{Face, 1})
    r1 = F[t1]
    r2 = F[t2]
    r3 = F[t3]
    r4 = F[t4]
    p1 = [Inf, Inf, Inf]
    p2 = [Inf, Inf, Inf]
    if length(union(r1.coord, r2.coord, r3.coord))==2
        if r1.coord == r2.coord
            p1[r1.coord] = (r1.aff +r2.aff)/2
            p2[r1.coord] = (r1.aff +r2.aff)/2
            p1[r3.coord] = r3.dir*abs(p1[r1.coord]-r1.aff) + r3.aff
            p2[r3.coord] = r3.dir*abs(p1[r1.coord]-r1.aff) + r3.aff
            coord = setdiff([1,2,3],union(r1.coord, r2.coord, r3.coord))
            p1[coord] = cell.boundary.origin[coord]
            p2[coord] = cell.boundary.origin[coord] +cell.boundary.widths[1]
        elseif r1.coord == r3.coord
            p1[r1.coord] = (r1.aff +r3.aff)/2
            p2[r1.coord] = (r1.aff +r3.aff)/2
            p1[r2.coord] = r2.dir*abs(p1[r1.coord]-r1.aff) + r2.aff
            p2[r2.coord] = r2.dir*abs(p1[r1.coord]-r1.aff) + r2.aff
            coord = setdiff([1,2,3],union(r1.coord, r3.coord, r2.coord))
            p1[coord] = cell.boundary.origin[coord]
            p2[coord] = cell.boundary.origin[coord] +cell.boundary.widths[1]
        else #r2.coord == r3.coord
            p1[r2.coord] = (r2.aff +r3.aff)/2
            p2[r2.coord] = (r2.aff +r3.aff)/2
            p1[r1.coord] = r1.dir*abs(p1[r2.coord]-r2.aff) + r1.aff
            p2[r1.coord] = r1.dir*abs(p1[r2.coord]-r2.aff) + r1.aff
            coord = setdiff([1,2,3],union(r1.coord, r2.coord, r3.coord))
            p1[coord] = cell.boundary.origin[coord]
            p2[coord] = cell.boundary.origin[coord] +cell.boundary.widths[1]
        end

        p1 = (p1[1], p1[2], p1[3])
        p2 = (p2[1], p2[2], p2[3])
        if inCell(cell,p1) && inCell(cell,p2) && OrientedZone(r1, p1) && OrientedZone(r2, p1) && OrientedZone(r3, p1) && OrientedZone(r1, p2) && OrientedZone(r2, p2) && OrientedZone(r3, p2) && (!Zone(r4,p1) || (Zone(r4,p1) && abs(p1[r4.coord]- r4.aff)> abs(p1[r2.coord]- r2.aff))) && (!Zone(r4,p2) || (Zone(r4,p2) && abs(p2[r4.coord]- r4.aff)> abs(p1[r2.coord]- r2.aff)))
            if p1 != p2
                push!(cell.data.points, (p1, [t1,t2,t3], 0))
                push!(cell.data.points,  (p2, [t1,t2,t3], 0))
                #push!(cell.data.cpoints,  ((p1.+p2)./2, [t1,t2,t3], 0))
            else
                push!(cell.data.points, (p1, [t1,t2,t3], 0))
            end

            # now I have to all the intersection points of the bisectors with the cell
            #=I1 = intersection_points(cell, t1,t2,F)
            for p in I1
                if p !=p1 && p!=p2 && OrientedZone(r1, p) && OrientedZone(r2, p) && (!Zone(r3,p) || (Zone(r3,p) && abs(p[r3.coord]- r3.aff)> abs(p[r1.coord]- r1.aff))) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t2], 0))
                end
            end
            I2 = intersection_points(cell, t1,t3,F)
            for p in I2
                if p !=p1 && p!=p2 && OrientedZone(r1, p) && OrientedZone(r3, p) && (!Zone(r2,p) || (Zone(r2,p) && abs(p[r2.coord]- r2.aff)> abs(p[r1.coord]- r1.aff))) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t3], 0))
                end
            end
            I3 = intersection_points(cell, t2,t3,F)
            for p in I3
                if p !=p1 && p!=p2 && OrientedZone(r2, p) && OrientedZone(r3, p) && (!Zone(r1,p) || (Zone(r1,p) && abs(p[r1.coord]- r1.aff)> abs(p[r2.coord]- r2.aff))) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r2.coord]- r2.aff)))
                    push!(cell.data.points, (p, [t2,t3], 0))
                end
            end
            =#
        else # there is no voronoi edge
            #bisector!(cell, t1, t2, t3, t4, F)
            #bisector!(cell, t1, t3, t2, t4, F)
            #bisector!(cell, t2, t3, t1, t4, F)
        end

    elseif length(union(r1.coord, r2.coord, r3.coord))==3
        output = Tuple{Float64,Float64, Float64}[]
        p = [Inf, Inf, Inf]
        if r1.coord ==1
            x= r1
        elseif r1.coord ==2
            y=r1
        else
            z=r1
        end
        if r2.coord ==1
            x= r2
        elseif r2.coord ==2
            y=r2
        else
            z=r2
        end
        if r3.coord ==1
            x= r3
        elseif r3.coord ==2
            y=r3
        else
            z=r3
        end
  #-----------------------------
        #if length(union(cell.data.vertices[1], cell.data.vertices[3], cell.data.vertices[5], cell.data.vertices[7])) >=1 && length(union(cell.data.vertices[1], cell.data.vertices[3], cell.data.vertices[5], cell.data.vertices[7], cell.data.int )) >= 3 #x = xmin
            p[1] = cell.boundary.origin[1]
            c = abs(p[1]-x.aff)
            p[2] = y.dir*c+y.aff
            p[3] = z.dir*c+ z.aff
            if OrientedZone(x, (p[1], p[2], p[3])) && OrientedZone(y, (p[1], p[2], p[3])) && OrientedZone(z, (p[1], p[2], p[3]))
            push!(output, (p[1], p[2], p[3]))
            end
        #if length(union(cell.data.vertices[2], cell.data.vertices[4], cell.data.vertices[6], cell.data.vertices[8])) >=1 && length(union(cell.data.vertices[2], cell.data.vertices[4], cell.data.vertices[6], cell.data.vertices[8], cell.data.int )) >= 3 # x = xmax
            p[1] = cell.boundary.origin[1]+cell.boundary.widths[1]
            c = abs(p[1]-x.aff)
            p[2] = y.dir*c+ y.aff
            p[3] = z.dir*c+z.aff
            if OrientedZone(x, (p[1], p[2], p[3])) && OrientedZone(y, (p[1], p[2], p[3])) && OrientedZone(z, (p[1], p[2], p[3]))
            push!(output, (p[1], p[2], p[3]))
            end
        #if length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[5], cell.data.vertices[6])) >= 1&& length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[5], cell.data.vertices[6], cell.data.int )) >= 3 # y = ymin
            p[2] = cell.boundary.origin[2]
            c = abs(p[2]-y.aff)
            p[3] = z.dir*c+ z.aff
            p[1] = x.dir*c+ x.aff
            if OrientedZone(x, (p[1], p[2], p[3])) && OrientedZone(y, (p[1], p[2], p[3])) && OrientedZone(z, (p[1], p[2], p[3]))
            push!(output, (p[1], p[2], p[3]))
            end
        #if length(union(cell.data.vertices[3], cell.data.vertices[4], cell.data.vertices[7], cell.data.vertices[8]))>=1 && length(union(cell.data.vertices[3], cell.data.vertices[4], cell.data.vertices[7], cell.data.vertices[8], cell.data.int )) >= 3 # y = ymax
            p[2] = cell.boundary.origin[2]+cell.boundary.widths[1]
            c = abs(p[2]-y.aff)
            p[3] = z.dir*c+ z.aff
            p[1] = x.dir*c+ x.aff
            if OrientedZone(x, (p[1], p[2], p[3])) && OrientedZone(y, (p[1], p[2], p[3])) && OrientedZone(z, (p[1], p[2], p[3]))
            push!(output, (p[1], p[2], p[3]))
            end
        #if length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[3], cell.data.vertices[4])) >=1 && length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[3], cell.data.vertices[4], cell.data.int )) >= 3 # z = zmin
            p[3] = cell.boundary.origin[3]
            c = abs(p[3]-z.aff)
            p[2] = y.dir*c+ y.aff
            p[1] = x.dir*c+ x.aff
            if OrientedZone(x, (p[1], p[2], p[3])) && OrientedZone(y, (p[1], p[2], p[3])) && OrientedZone(z, (p[1], p[2], p[3]))
            push!(output, (p[1], p[2], p[3]))
            end
        #if length(union(cell.data.vertices[5], cell.data.vertices[6], cell.data.vertices[7], cell.data.vertices[8])) >=1 && length(union(cell.data.vertices[5], cell.data.vertices[6], cell.data.vertices[7], cell.data.vertices[8], cell.data.int )) >= 3 # z = zmax
            p[3] = cell.boundary.origin[3]+cell.boundary.widths[1]
            c = abs(p[3]-z.aff)
            p[2] = y.dir*c+ y.aff
            p[1] = x.dir*c+ x.aff
            if OrientedZone(x, (p[1], p[2], p[3])) && OrientedZone(y, (p[1], p[2], p[3])) && OrientedZone(z, (p[1], p[2], p[3]))
            push!(output, (p[1], p[2], p[3]))
            end
        i = 1
        while i <= length(output)
            if !inCell(cell, output[i]) || !OrientedZone(x, output[i]) || !OrientedZone(y, output[i]) || !OrientedZone(z, output[i]) || !(!Zone(r4,output[i]) || (Zone(r4,output[i]) && abs(output[i][r4.coord]- r4.aff)> abs(output[i][r1.coord]- r1.aff)))
                deleteat!(output, i)
                i = i-1
            end
            i +=1
        end

        output = unique(output)

        if length(output)>0 #there is a voronoi edge
            if length(output) == 1
                if inCell(cell, (x.aff, y.aff, z.aff)) && output[1] != (x.aff, y.aff, z.aff) #&& (!Zone(r4,output[1]) || (Zone(r4,output[1]) && abs(output[1][r4.coord]- r4.aff)> abs(output[1][r1.coord]- r1.aff)))
                    push!(cell.data.points, ((x.aff, y.aff, z.aff), [t1,t2,t3], 0))
                    #push!(cell.data.cpoints, ((x.aff, y.aff, z.aff), [t1,t2,t3], 0))
                    push!(cell.data.points, (output[1], [t1,t2,t3], 0))
                else#if (!Zone(r4,output[1]) || (Zone(r4,output[1]) && abs(output[1][r4.coord]- r4.aff)> abs(output[1][r1.coord]- r1.aff)))
                    push!(cell.data.points, (output[1], [t1,t2,t3], 0))
                end
            else#if (!Zone(r4,output[1]) || (Zone(r4,output[1]) && abs(output[1][r4.coord]- r4.aff)> abs(output[1][r1.coord]- r1.aff))) && (!Zone(r4,output[2]) || (Zone(r4,output[2]) && abs(output[2][r4.coord]- r4.aff)> abs(output[2][r1.coord]- r1.aff)))
                push!(cell.data.points, (output[1], [t1,t2,t3], 0))
                push!(cell.data.points,  (output[2], [t1,t2,t3], 0))
                #push!(cell.data.cpoints,  ((output[1].+output[2])./2, [t1,t2,t3], 0))
            end
            # now I have to find all the intersection points of the bisectors with the cell
            # changed orientedZone ---->>> to Zone
            #p1 = output[1]
            #p2 = output[2]
            #=
            I1 = intersection_points(cell, t1,t2,F)
            for p in I1
                if length(findin(output,p))==0 && OrientedZone(r1, p) && OrientedZone(r2, p) && (!Zone(r3,p) || (Zone(r3,p) && abs(p[r3.coord]- r3.aff)> abs(p[r1.coord]- r1.aff))) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t2], 0))
                end
            end
            I2 = intersection_points(cell, t1,t3,F)
            for p in I2
                if length(findin(output,p))==0 && OrientedZone(r3, p) && (!Zone(r2,p) || (Zone(r2,p) && abs(p[r2.coord]- r2.aff)> abs(p[r1.coord]- r1.aff))) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t3], 0))
                end
            end
            I3 = intersection_points(cell, t2,t3,F)
            for p in I3
                if length(findin(output,p))==0 && OrientedZone(r2, p) && OrientedZone(r3, p) && (!Zone(r1,p) || (Zone(r1,p) && abs(p[r1.coord]- r1.aff)> abs(p[r2.coord]- r2.aff))) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r2.coord]- r2.aff)))
                    push!(cell.data.points, (p, [t2,t3], 0))
                end
            end
            =#
        else # there is no voronoi edge
            #bisector!(cell, t1,t2,t3,t4,F)
            #bisector!(cell, t1,t3,t2,t4,F)
            #bisector!(cell, t2,t3,t1,t4,F)
        end

    end # if
end


#-----------VORONOI VERTICES----------------------------------------------------
function edge_endpnts2(cell::Cell, t1::Int64, t2::Int64, t3::Int64, t4::Int64, F::Array{Face, 1})
    output2 = Tuple{Float64,Float64,Float64}[]
    r1 = F[t1]
    r2 = F[t2]
    r3 = F[t3]
    r4 = F[t4]
    p1 = [Inf, Inf, Inf]
    p2 = [Inf, Inf, Inf]
    if length(union(r1.coord, r2.coord, r3.coord))==2
        if r1.coord == r2.coord
            p1[r1.coord] = (r1.aff +r2.aff)/2
            p2[r1.coord] = (r1.aff +r2.aff)/2
            p1[r3.coord] = r3.dir*p1[r1.coord] + r3.aff
            p2[r3.coord] = r3.dir*p1[r1.coord] + r3.aff
            coord = setdiff([1,2,3],union(r1.coord, r2.coord, r3.coord))
            p1[coord] = cell.boundary.origin[coord]
            p2[coord] = cell.boundary.origin[coord] +cell.boundary.widths[1]
        elseif r1.coord == r3.coord
            p1[r1.coord] = (r1.aff +r3.aff)/2
            p2[r1.coord] = (r1.aff +r3.aff)/2
            p1[r2.coord] = r2.dir*p1[r1.coord]+ r2.aff
            p2[r2.coord] = r2.dir*p1[r1.coord]+ r2.aff
            coord = setdiff([1,2,3],union(r1.coord, r3.coord, r2.coord))
            p1[coord] = cell.boundary.origin[coord]
            p2[coord] = cell.boundary.origin[coord] +cell.boundary.widths[1]
        else #r2.coord == r3.coord
            p1[r2.coord] = (r2.aff +r3.aff)/2
            p2[r2.coord] = (r2.aff +r3.aff)/2
            p1[r1.coord] = r1.dir*p1[r2.coord]+ r1.aff
            p2[r1.coord] = r1.dir*p1[r2.coord]+ r1.aff
            coord = setdiff([1,2,3],union(r1.coord, r2.coord, r3.coord))
            p1[coord] = cell.boundary.origin[coord]
            p2[coord] = cell.boundary.origin[coord] +cell.boundary.widths[1]
        end

        p1 = (p1[1], p1[2], p1[3])
        p2 = (p2[1], p2[2], p2[3])
        if inCell(cell,p1) && inCell(cell,p2) && OrientedZone(r1, p1) && OrientedZone(r2, p1) && OrientedZone(r3, p1) && OrientedZone(r1, p2) && OrientedZone(r2, p2) && OrientedZone(r3, p2) && (!Zone(r4,p1) || (Zone(r4,p1) && abs(p1[r4.coord]- r4.aff)> abs(p1[r2.coord]- r2.aff))) && (!Zone(r4,p2) || (Zone(r4,p2) && abs(p2[r4.coord]- r4.aff)> abs(p1[r2.coord]- r2.aff)))
            if p1 != p2
                push!(output2, p1)
                push!(output2,  p2)
                #push!(cell.data.cpoints,  ((p1.+p2)./2, [t1,t2,t3], 0))
            else
                push!(output2, p1)
            end

            # now I have to all the intersection points of the bisectors with the cell
            #=I1 = intersection_points(cell, t1,t2,F)
            for p in I1
                if p !=p1 && p!=p2 && OrientedZone(r1, p) && OrientedZone(r2, p) && (!Zone(r3,p) || (Zone(r3,p) && abs(p[r3.coord]- r3.aff)> abs(p[r1.coord]- r1.aff))) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t2], 0))
                end
            end
            I2 = intersection_points(cell, t1,t3,F)
            for p in I2
                if p !=p1 && p!=p2 && OrientedZone(r1, p) && OrientedZone(r3, p) && (!Zone(r2,p) || (Zone(r2,p) && abs(p[r2.coord]- r2.aff)> abs(p[r1.coord]- r1.aff))) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t3], 0))
                end
            end
            I3 = intersection_points(cell, t2,t3,F)
            for p in I3
                if p !=p1 && p!=p2 && OrientedZone(r2, p) && OrientedZone(r3, p) && (!Zone(r1,p) || (Zone(r1,p) && abs(p[r1.coord]- r1.aff)> abs(p[r2.coord]- r2.aff))) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r2.coord]- r2.aff)))
                    push!(cell.data.points, (p, [t2,t3], 0))
                end
            end
            =#
        else # there is no voronoi edge
            if inCell(cell,p1) && OrientedZone(r1, p1) && OrientedZone(r2, p1) && OrientedZone(r3, p1) && (!Zone(r4,p1) || (Zone(r4,p1) && abs(p1[r4.coord]- r4.aff)> abs(p1[r2.coord]- r2.aff)))
                    push!(output2, p1)
            end
            if inCell(cell,p2) && OrientedZone(r1, p2) && OrientedZone(r2, p2) && OrientedZone(r3, p2) && (!Zone(r4,p2) || (Zone(r4,p2) && abs(p2[r4.coord]- r4.aff)> abs(p2[r2.coord]- r2.aff)))
                    push!(output2, p2)
            end
            #bisector!(cell, t1, t2, t3, t4, F)
            #bisector!(cell, t1, t3, t2, t4, F)
            #bisector!(cell, t2, t3, t1, t4, F)
        end

    elseif length(union(r1.coord, r2.coord, r3.coord))==3
        output = Tuple{Float64,Float64, Float64}[]
        p = [Inf, Inf, Inf]
        if r1.coord ==1
            x= r1
        elseif r1.coord ==2
            y=r1
        else
            z=r1
        end
        if r2.coord ==1
            x= r2
        elseif r2.coord ==2
            y=r2
        else
            z=r2
        end
        if r3.coord ==1
            x= r3
        elseif r3.coord ==2
            y=r3
        else
            z=r3
        end
  #-----------------------------
        if length(union(cell.data.vertices[1], cell.data.vertices[3], cell.data.vertices[5], cell.data.vertices[7])) >=1 && length(union(cell.data.vertices[1], cell.data.vertices[3], cell.data.vertices[5], cell.data.vertices[7], cell.data.int )) >= 3 #x = xmin
            p[1] = cell.boundary.origin[1]
            c = abs(p[1]-x.aff)
            p[2] = y.dir*c- y.dir *y.aff
            p[3] = z.dir*c- z.dir * z.aff
            push!(output, (p[1], p[2], p[3]))
        end
        if length(union(cell.data.vertices[2], cell.data.vertices[4], cell.data.vertices[6], cell.data.vertices[8])) >=1 && length(union(cell.data.vertices[2], cell.data.vertices[4], cell.data.vertices[6], cell.data.vertices[8], cell.data.int )) >= 3 # x = xmax
            p[1] = cell.boundary.origin[1]+cell.boundary.widths[1]
            c = abs(p[1]-x.aff)
            p[2] = y.dir*c- y.dir * y.aff
            p[3] = z.dir*c- z.dir * z.aff
            push!(output, (p[1], p[2], p[3]))
        end
        if length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[5], cell.data.vertices[6])) >= 1&& length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[5], cell.data.vertices[6], cell.data.int )) >= 3 # y = ymin
            p[2] = cell.boundary.origin[2]
            c = abs(p[2]-y.aff)
            p[3] = z.dir*c- z.dir * z.aff
            p[1] = x.dir*c- x.dir * x.aff
            push!(output, (p[1], p[2], p[3]))
        end
        if length(union(cell.data.vertices[3], cell.data.vertices[4], cell.data.vertices[7], cell.data.vertices[8]))>=1 && length(union(cell.data.vertices[3], cell.data.vertices[4], cell.data.vertices[7], cell.data.vertices[8], cell.data.int )) >= 3 # y = ymax
            p[2] = cell.boundary.origin[2]+cell.boundary.widths[1]
            c = abs(p[2]-y.aff)
            p[3] = z.dir*c- z.dir * z.aff
            p[1] = x.dir*c- x.dir * x.aff
            push!(output, (p[1], p[2], p[3]))
        end
        if length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[3], cell.data.vertices[4])) >=1 && length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[3], cell.data.vertices[4], cell.data.int )) >= 3 # z = zmin
            p[3] = cell.boundary.origin[3]
            c = abs(p[3]-z.aff)
            p[2] = y.dir*c- y.dir * y.aff
            p[1] = x.dir*c- x.dir * x.aff
            push!(output, (p[1], p[2], p[3]))
        end
        if length(union(cell.data.vertices[5], cell.data.vertices[6], cell.data.vertices[7], cell.data.vertices[8])) >=1 && length(union(cell.data.vertices[5], cell.data.vertices[6], cell.data.vertices[7], cell.data.vertices[8], cell.data.int )) >= 3 # z = zmax
            p[3] = cell.boundary.origin[3]+cell.boundary.widths[1]
            c = abs(p[3]-z.aff)
            p[2] = y.dir*c- y.dir * y.aff
            p[1] = x.dir*c- x.dir * x.aff
            push!(output, (p[1], p[2], p[3]))
        end
        i = 1
        while i <= length(output)
            if !inCell(cell, output[i]) || !OrientedZone(x, output[i]) || !OrientedZone(y, output[i]) || !OrientedZone(z, output[i]) || !(!Zone(r4,output[1]) || (Zone(r4,output[1]) && abs(output[1][r4.coord]- r4.aff)> abs(output[1][r1.coord]- r1.aff)))
                deleteat!(output, i)
                i = i-1
            end
            i +=1
        end

        output = unique(output)

        if length(output)>0 #there is a voronoi edge
            if length(output) == 1
                if inCell(cell, (x.aff, y.aff, z.aff)) && output[1] != (x.aff, y.aff, z.aff) #&& (!Zone(r4,output[1]) || (Zone(r4,output[1]) && abs(output[1][r4.coord]- r4.aff)> abs(output[1][r1.coord]- r1.aff)))
                    push!(output2, (x.aff, y.aff, z.aff))
                    push!(output2, output[1])
                else#if (!Zone(r4,output[1]) || (Zone(r4,output[1]) && abs(output[1][r4.coord]- r4.aff)> abs(output[1][r1.coord]- r1.aff)))
                    push!(cell.data.points, (output[1], [t1,t2,t3], 0))
                end
            else#if (!Zone(r4,output[1]) || (Zone(r4,output[1]) && abs(output[1][r4.coord]- r4.aff)> abs(output[1][r1.coord]- r1.aff))) && (!Zone(r4,output[2]) || (Zone(r4,output[2]) && abs(output[2][r4.coord]- r4.aff)> abs(output[2][r1.coord]- r1.aff)))
                push!(output2, output[1])
                push!(output2,  output[2])
                #push!(output2,  output[1].+output[2])./2, [t1,t2,t3], 0))
            end
            # now I have to find all the intersection points of the bisectors with the cell
            # changed orientedZone ---->>> to Zone
            #p1 = output[1]
            #p2 = output[2]
            #=
            I1 = intersection_points(cell, t1,t2,F)
            for p in I1
                if length(findin(output,p))==0 && OrientedZone(r1, p) && OrientedZone(r2, p) && (!Zone(r3,p) || (Zone(r3,p) && abs(p[r3.coord]- r3.aff)> abs(p[r1.coord]- r1.aff))) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t2], 0))
                end
            end
            I2 = intersection_points(cell, t1,t3,F)
            for p in I2
                if length(findin(output,p))==0 && OrientedZone(r3, p) && (!Zone(r2,p) || (Zone(r2,p) && abs(p[r2.coord]- r2.aff)> abs(p[r1.coord]- r1.aff))) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t3], 0))
                end
            end
            I3 = intersection_points(cell, t2,t3,F)
            for p in I3
                if length(findin(output,p))==0 && OrientedZone(r2, p) && OrientedZone(r3, p) && (!Zone(r1,p) || (Zone(r1,p) && abs(p[r1.coord]- r1.aff)> abs(p[r2.coord]- r2.aff))) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r2.coord]- r2.aff)))
                    push!(cell.data.points, (p, [t2,t3], 0))
                end
            end
            =#
        else # there is no voronoi edge
            #bisector!(cell, t1,t2,t3,t4,F)
            #bisector!(cell, t1,t3,t2,t4,F)
            #bisector!(cell, t2,t3,t1,t4,F)
        end

    end # if
    return output2
end

function edge_endpnts(cell::Cell, t1::Int64, t2::Int64, t3::Int64,F::Array{Face, 1} )
    output2 = Tuple{Float64,Float64,Float64}[]
    r1 = F[t1]
    r2 = F[t2]
    r3 = F[t3]
    # p1 and p2 are the endpoints of the voronoi edge
    p1 = [Inf, Inf, Inf]
    p2 = [Inf, Inf, Inf]
    if length(union(r1.coord, r2.coord, r3.coord))==2
        if r1.coord == r2.coord
            p1[r1.coord] = (r1.aff +r2.aff)/2
            p2[r1.coord] = (r1.aff +r2.aff)/2
            p1[r3.coord] = r3.dir*p1[r1.coord] + r3.aff
            p2[r3.coord] = r3.dir*p1[r1.coord] + r3.aff
            coord = setdiff([1,2,3],union(r1.coord, r2.coord, r3.coord))
            p1[coord] = cell.boundary.origin[coord]
            p2[coord] = cell.boundary.origin[coord] +cell.boundary.widths[1]
        elseif r1.coord == r3.coord
            p1[r1.coord] = (r1.aff +r3.aff)/2
            p2[r1.coord] = (r1.aff +r3.aff)/2
            p1[r2.coord] = r2.dir*p1[r1.coord] + r2.aff
            p2[r2.coord] = r2.dir*p1[r1.coord] + r2.aff
            coord = setdiff([1,2,3],union(r1.coord, r3.coord, r2.coord))
            p1[coord] = cell.boundary.origin[coord]
            p2[coord] = cell.boundary.origin[coord] +cell.boundary.widths[1]
        else #r2.coord == r3.coord
            p1[r2.coord] = (r2.aff +r3.aff)/2
            p2[r2.coord] = (r2.aff +r3.aff)/2
            p1[r1.coord] = r1.dir*p1[r2.coord] + r3.aff
            p2[r1.coord] = r1.dir*p1[r2.coord] + r3.aff
            coord = setdiff([1,2,3],union(r1.coord, r2.coord, r3.coord))
            p1[coord] = cell.boundary.origin[coord]
            p2[coord] = cell.boundary.origin[coord] +cell.boundary.widths[1]
        end

        p1 = (p1[1], p1[2], p1[3])
        p2 = (p2[1], p2[2], p2[3])
        if inCell(cell,p1) && inCell(cell,p2) && OrientedZone(r1, p1) && OrientedZone(r2, p1) && OrientedZone(r3, p1) && OrientedZone(r1, p2) && OrientedZone(r2, p2) && OrientedZone(r3, p2)
            if p1 != p2
                push!(output2, p1)
                push!(output2, p2)
                #push!(cell.data.cpoints,  ((p1.+p2)./2, [t1,t2,t3], 0))
            else
                # this cannot happen: if I have one intersection point, then there must be a voronoi vertex; but a
                # voronoi vertex is defined by three planes orthogonal per 2
                #push!(cell.data.points, (p1, [t1,t2,t3], 0))
            end

            # now I have to all the intersection points of the bisectors with the cell
            #=I1 = intersection_points(cell, t1,t2,F)
            for p in I1
                if p !=p1 && p!=p2 && OrientedZone(r1, p) && OrientedZone(r2, p) && (!Zone(r3,p) || (Zone(r3,p) && abs(p[r3.coord]- r3.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t2], 0))
                end
            end
            I2 = intersection_points(cell, t1,t3,F)
            for p in I2
                if p !=p1 && p!=p2 && OrientedZone(r1, p) && OrientedZone(r3, p) && (!Zone(r2,p) || (Zone(r2,p) && abs(p[r2.coord]- r2.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t3], 0))
                end
            end
            I3 = intersection_points(cell, t2,t3,F)
            for p in I3
                if p !=p1 && p!=p2 && OrientedZone(r2, p) && OrientedZone(r3, p) && (!Zone(r1,p) || (Zone(r1,p) && abs(p[r1.coord]- r1.aff)> abs(p[r2.coord]- r2.aff)))
                    push!(cell.data.points, (p, [t2,t3], 0))
                end
            end =#
        else # there is no voronoi edge
            #bisector!(cell, t1,t2,t3,F)
            #bisector!(cell, t1,t3,t2,F)
            #bisector!(cell, t2,t3,t1,F)
        end

    elseif length(union(r1.coord, r2.coord, r3.coord))==3
        output = Tuple{Float64,Float64, Float64}[]
        p = [Inf, Inf, Inf]
        if r1.coord ==1
            x= r1
        elseif r1.coord ==2
            y=r1
        else
            z=r1
        end
        if r2.coord ==1
            x= r2
        elseif r2.coord ==2
            y=r2
        else
            z=r2
        end
        if r3.coord ==1
            x= r3
        elseif r3.coord ==2
            y=r3
        else
            z=r3
        end
  #-----------------------------
        if length(union(cell.data.vertices[1], cell.data.vertices[3], cell.data.vertices[5], cell.data.vertices[7])) >=1 && length(union(cell.data.vertices[1], cell.data.vertices[3], cell.data.vertices[5], cell.data.vertices[7], cell.data.int )) >= 3 #x = xmin
            p[1] = cell.boundary.origin[1]
            c = abs(p[1]-x.aff)
            p[2] = y.dir*c+ y.aff
            p[3] = z.dir*c+ z.aff
            push!(output, (p[1], p[2], p[3]))
        end
        if length(union(cell.data.vertices[2], cell.data.vertices[4], cell.data.vertices[6], cell.data.vertices[8])) >=1 && length(union(cell.data.vertices[2], cell.data.vertices[4], cell.data.vertices[6], cell.data.vertices[8], cell.data.int )) >= 3 # x = xmax
            p[1] = cell.boundary.origin[1]+cell.boundary.widths[1]
            c = abs(p[1]-x.aff)
            p[2] = y.dir*c+ y.aff
            p[3] = z.dir*c+ z.aff
            push!(output, (p[1], p[2], p[3]))
        end
        if length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[5], cell.data.vertices[6])) >= 1&& length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[5], cell.data.vertices[6], cell.data.int )) >= 3 # y = ymin
            p[2] = cell.boundary.origin[2]
            c = abs(p[2]-y.aff)
            p[3] = z.dir*c+ z.aff
            p[1] = x.dir*c+ x.aff
            push!(output, (p[1], p[2], p[3]))
        end
        if length(union(cell.data.vertices[3], cell.data.vertices[4], cell.data.vertices[7], cell.data.vertices[8]))>=1 && length(union(cell.data.vertices[3], cell.data.vertices[4], cell.data.vertices[7], cell.data.vertices[8], cell.data.int )) >= 3 # y = ymax
            p[2] = cell.boundary.origin[2]+cell.boundary.widths[1]
            c = abs(p[2]-y.aff)
            p[3] = z.dir*c+ z.aff
            p[1] = x.dir*c+ x.aff
            push!(output, (p[1], p[2], p[3]))
        end
        if length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[3], cell.data.vertices[4])) >=1 && length(union(cell.data.vertices[1], cell.data.vertices[2], cell.data.vertices[3], cell.data.vertices[4], cell.data.int )) >= 3 # z = zmin
            p[3] = cell.boundary.origin[3]
            c = abs(p[3]-z.aff)
            p[2] = y.dir*c+ y.aff
            p[1] = x.dir*c+ x.aff
            push!(output, (p[1], p[2], p[3]))
        end
        if length(union(cell.data.vertices[5], cell.data.vertices[6], cell.data.vertices[7], cell.data.vertices[8])) >=1 && length(union(cell.data.vertices[5], cell.data.vertices[6], cell.data.vertices[7], cell.data.vertices[8], cell.data.int )) >= 3 # z = zmax
            p[3] = cell.boundary.origin[3]+cell.boundary.widths[1]
            c = abs(p[3]-z.aff)
            p[2] = y.dir*c+ y.aff
            p[1] = x.dir*c+ x.aff
            push!(output, (p[1], p[2], p[3]))
        end
        i = 1
        while i <= length(output)
            if !inCell(cell, output[i]) || !OrientedZone(x, output[i]) || !OrientedZone(y, output[i]) || !OrientedZone(z, output[i])
                deleteat!(output, i)
                i = i-1
            end
            i +=1
        end

        output = unique(output)

        if length(output)>0 #there is a voronoi edge
            if length(output) == 1 # there must be a corner
                if inCell(cell, (x.aff, y.aff, z.aff)) && output[1] != (x.aff, y.aff, z.aff)
                    push!(output2, (x.aff, y.aff, z.aff))
                    push!(output2, output[1])
                else
                    # this cannot happen (?)
                    #push!(cell.data.points, (output[1], [t1,t2,t3], 0))
                end
            else
                push!(output2, output[1])
                push!(output2, output[2])
                #push!(cell.data.cpoints,  ((output[1].+output[2])./2, [t1,t2,t3], 0))
            end
            # now I have to find all the intersection points of the bisectors with the cell
            # changed orientedZone ---->>> to Zone
            #p1 = output[1]
            #p2 = output[2]
            #=
            I1 = intersection_points(cell, t1,t2,F)
            for p in I1
                if length(findin(output,p))==0 && OrientedZone(r1, p) && OrientedZone(r2, p) && (!Zone(r3,p) || (Zone(r3,p) && abs(p[r3.coord]- r3.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t2], 0))
                end
            end
            I2 = intersection_points(cell, t1,t3,F)
            for p in I2
                if length(findin(output,p))==0 && OrientedZone(r3, p) && (!Zone(r2,p) || (Zone(r2,p) && abs(p[r2.coord]- r2.aff)> abs(p[r1.coord]- r1.aff)))
                    push!(cell.data.points, (p, [t1,t3], 0))
                end
            end
            I3 = intersection_points(cell, t2,t3,F)
            for p in I3
                if length(findin(output,p))==0 && OrientedZone(r2, p) && OrientedZone(r3, p) && (!Zone(r1,p) || (Zone(r1,p) && abs(p[r1.coord]- r1.aff)> abs(p[r2.coord]- r2.aff)))
                    push!(cell.data.points, (p, [t2,t3], 0))
                end
            end
            =#
        else # there is no voronoi edge
            #bisector!(cell, t1,t2,t3,F)
            #bisector!(cell, t1,t3,t2,F)
            #bisector!(cell, t2,t3,t1,F)
        end

    end # if
    return output2
end

function vor_vertex(l1::Array{Tuple{Float64, Float64, Float64},1}, l2::Array{Tuple{Float64, Float64, Float64},1})
    println("voronoi vertex")
    pr1 = [ (l1[1][1],l1[1][2]), (l1[2][1], l1[2][2])]
    pr2 = [ (l2[1][1],l2[1][2]), (l2[2][1], l2[2][2])]
    p = (Inf,Inf) #intersection in the xy plane
    if pr1[1]==pr1[2]
        if pr2[1] == pr2[2]
             if pr1[1] == pr2[1]
                 p = pr1[1]
            end
        else
            if pr1[1][1] >= min(pr2[1][1], pr2[2][1]) && pr1[1][1] <= max(pr2[1][1], pr2[2][1]) && pr1[1][2] >= min(pr2[1][2], pr2[2][2]) && pr1[1][2] <= max(pr2[1][2], pr2[2][2]) #on segment
                p = pr1[1]
            end
        end
    else
        if pr2[1] == pr2[2]
            if pr2[1][1] >= min(pr1[1][1], pr1[2][1]) && pr2[1][1] <= max(pr1[1][1], pr1[2][1]) && pr2[1][2] >= min(pr1[1][2], pr1[2][2]) && pr2[1][2] <= max(pr1[1][2], pr1[2][2]) #on segment
                p = pr1[1]
            end
        else
            a1 = pr1[2][2]-pr1[1][2]#l1.e.y - l1.s.y
            b1 = pr1[1][1]-pr1[2][1] #l1.s.x - l1.e.x
            c1 = a1 * pr1[1][1] + b1 * pr1[1][2] #a1 * l1.s.x + b1 * l1.s.y

            a2 = pr2[2][2]-pr2[1][2] #l2.e.y - l2.s.y
            b2 = pr2[1][1]-pr2[2][1] #l2.s.x - l2.e.x
            c2 = a2 * pr2[1][1] + b2 * pr2[1][2] #a2 * l2.s.x + b2 * l2.s.y

            Δ = a1 * b2 - a2 * b1
            # If lines are parallel, intersection point will contain infinite values
            p = ((b2 * c1 - b1 * c2) / Δ, (a1 * c2 - a2 * c1) / Δ)
        end
    end
    pp = (Inf,Inf)
    if p[1] != Inf && p[2] != Inf
        pr1 = [ (l1[1][1],l1[1][3]), (l1[2][1], l1[2][3])]
        pr2 = [ (l2[1][1],l2[1][3]), (l2[2][1], l2[2][3])]
         #intersection in the xz plane
        if pr1[1]==pr1[2]
            if pr2[1] == pr2[2]
                 if pr1[1] == pr2[1]
                     pp = pr1[1]
                end
            else
                if pr1[1][1] >= min(pr2[1][1], pr2[2][1]) && pr1[1][1] <= max(pr2[1][1], pr2[2][1]) && pr1[1][2] >= min(pr2[1][2], pr2[2][2]) && pr1[1][2] <= max(pr2[1][2], pr2[2][2]) #on segment
                    pp = pr1[1]
                end
            end
        else
            if pr2[1] == pr2[2]
                if pr2[1][1] >= min(pr1[1][1], pr1[2][1]) && pr2[1][1] <= max(pr1[1][1], pr1[2][1]) && pr2[1][2] >= min(pr1[1][2], pr1[2][2]) && pr2[1][2] <= max(pr1[1][2], pr1[2][2]) #on segment
                    pp = pr1[1]
                end
            else
                a1 = pr1[2][2]-pr1[1][2]#l1.e.y - l1.s.y
                b1 = pr1[1][1]-pr1[2][1] #l1.s.x - l1.e.x
                c1 = a1 * pr1[1][1] + b1 * pr1[1][2] #a1 * l1.s.x + b1 * l1.s.y

                a2 = pr2[2][2]-pr2[1][2] #l2.e.y - l2.s.y
                b2 = pr2[1][1]-pr2[2][1] #l2.s.x - l2.e.x
                c2 = a2 * pr2[1][1] + b2 * pr2[1][2] #a2 * l2.s.x + b2 * l2.s.y

                Δ = a1 * b2 - a2 * b1
                # If lines are parallel, intersection point will contain infinite values
                pp = ((b2 * c1 - b1 * c2) / Δ, (a1 * c2 - a2 * c1) / Δ)
            end
        end
    end

    if pp[1] != Inf && pp[2] != Inf && pp[1] == p[1]
        return (p[1], p[2], pp[2])
    end

    return (Inf, Inf, Inf)
end


function vertex!(cell::Cell, t1::Int64, t2::Int64, t3::Int64, t4::Int64, F::Array{Face, 1})
    r1 = F[t1]
    r2 = F[t2]
    r3 = F[t3]
    r4 = F[t4]
    I1 = edge_endpnts(cell, t1, t2,t3, F)
    I2 = edge_endpnts(cell, t1, t2,t4, F)
    I3 = edge_endpnts(cell, t1, t3,t4, F)
    I4 = edge_endpnts(cell, t2, t3,t4, F)
    i12 = intersect(I1,I2)
    i13 = intersect(I1,I3)
    i14 = intersect(I1,I4)
    i23 = intersect(I2,I3)
    i24 = intersect(I2,I4)
    i34 = intersect(I3,I4)

    if length(I1) ==0 || length(I2) == 0 || length(I3) == 0 || length(I4) == 0 #there is no voronoi vertex
        #=if length(I1) == 2
            trisector!(cell, t1, t2, t3, t4, F)
            #bisector!(cell, t4, t1, t2, t3, F)
            #bisector!(cell, t4, t2, t1, t3, F)
            #bisector!(cell, t4, t3, t1, t2, F)
        elseif length(I2) ==2
            trisector!(cell, t1, t2, t4, t3, F)
            #bisector!(cell, t3, t1, t2, t4, F)
            #bisector!(cell, t3, t2, t1, t4, F)
            #bisector!(cell, t3, t4, t1, t2, F)
        elseif length(I3) ==2
            trisector!(cell, t1, t3, t4, t2, F)
            #bisector!(cell, t2, t1, t3, t4, F)
            #bisector!(cell, t2, t3, t1, t4, F)
            #bisector!(cell, t2, t4, t1, t3, F)
        elseif length(I4) ==2
            trisector!(cell, t2, t3, t4, t1, F)
            #bisector!(cell, t1, t3, t2, t4, F)
            #bisector!(cell, t1, t2, t3, t4, F)
            #bisector!(cell, t1, t4, t3, t2, F)
        else
            #bisector!(cell, t1, t3, t2, t4, F)
            #bisector!(cell, t1, t2, t3, t4, F)
            #bisector!(cell, t1, t4, t3, t2, F)
            #bisector!(cell, t2, t3, t1, t4, F)
            #bisector!(cell, t2, t4, t1, t3, F)
            #bisector!(cell, t3, t4, t1, t2, F)
        end =#
        trisector!(cell, t1, t2, t3, t4, F)
        trisector!(cell, t1, t2, t4, t3,F)
        trisector!(cell, t1, t3, t4, t2,F)
        trisector!(cell, t2, t3, t4, t1,F)

        return
    end

    # so far I know there are 4 voronoi edges in the cell - but I did not check the distance of every edge to the other site
    # maybe this contains redundant conditions - if there are 2 edges that intersect, then all of them will intersect
    # in this case the voronoi vertex is on the boundary
    #=if length(i12) == 1
        push!(cell.data.cpoints, (i12[1], [t1,t2,t3,t4], 0))
    elseif length(i13) == 1
        push!(cell.data.cpoints, (i13[1], [t1,t2,t3,t4], 0))
    elseif length(i14) == 1
        push!(cell.data.cpoints, (i14[1], [t1,t2,t3,t4], 0))
    elseif length(i23) == 1
        push!(cell.data.cpoints, (i23[1], [t1,t2,t3,t4], 0))
    elseif length(i24) == 1
        push!(cell.data.cpoints, (i24[1], [t1,t2,t3,t4], 0))
    elseif length(i34) == 1
        push!(cell.data.cpoints, (i34[1], [t1,t2,t3,t4], 0))
        =#
     # I have to intersect two voronoi edges to find the vor vertex
        # any two!
        v = vor_vertex(I1, I2)
        if v[1] != Inf && v[2] !=Inf && v[3] != Inf
            push!(cell.data.cpoints, (v, [t1,t2,t3,t4], 0))
        else # there is no voronoi vertex ( ??? that can happen at this point??)
            trisector!(cell, t1, t2, t3, t4, F)
            trisector!(cell, t1, t2, t4, t3,F)
            trisector!(cell, t1, t3, t4, t2,F)
            trisector!(cell, t2, t3, t4, t1,F)
            return
        end

    # choose which edge intersection points are to keep
    #=v= cell.data.cpoints[1]
    for p in I1
        if p !=v && OrientedZone(r1, p) && OrientedZone(r2, p) && OrientedZone(r3, p) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r1.coord]- r1.aff)))
            push!(cell.data.cpoints, (p, [t1,t2,t3], 0))
        end
    end

    for p in I2
        if p !=v && OrientedZone(r1, p) && OrientedZone(r2, p) && OrientedZone(r4, p) && (!Zone(r3,p) || (Zone(r3,p) && abs(p[r3.coord]- r3.aff)> abs(p[r1.coord]- r1.aff)))
            push!(cell.data.cpoints, (p, [t1,t2,t4], 0))
        end
    end

    for p in I3
        if p !=v && OrientedZone(r1, p) && OrientedZone(r3, p) && OrientedZone(r4, p) && (!Zone(r2,p) || (Zone(r2,p) && abs(p[r2.coord]- r2.aff)> abs(p[r1.coord]- r1.aff)))
            push!(cell.data.cpoints, (p, [t1,t3,t4], 0))
        end
    end

    for p in I4
        if p !=v && OrientedZone(r2, p) && OrientedZone(r3, p) && OrientedZone(r4, p) && (!Zone(r1,p) || (Zone(r1,p) && abs(p[r1.coord]- r1.aff)> abs(p[r2.coord]- r2.aff)))
            push!(cell.data.cpoints, (p, [t2,t3,t4], 0))
        end
    end

    # add points from the intersection of the bisectors with the cell

    I = intersection_points(cell, t1,t2,F)
    for p in I
        if OrientedZone(r1, p) && OrientedZone(r2, p) && (!Zone(r3,p) || (Zone(r3,p) && abs(p[r3.coord]- r3.aff)> abs(p[r1.coord]- r1.aff))) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r1.coord]- r1.aff)))
            push!(cell.data.points, (p, [t1,t2], 0))
        end
    end
    I = intersection_points(cell, t1,t3,F)
    for p in I
        if OrientedZone(r1, p) && OrientedZone(r3, p) && (!Zone(r2,p) || (Zone(r2,p) && abs(p[r2.coord]- r2.aff)> abs(p[r1.coord]- r1.aff))) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r1.coord]- r1.aff)))
            push!(cell.data.points, (p, [t1,t3], 0))
        end
    end
    I = intersection_points(cell, t1,t4,F)
    for p in I
        if OrientedZone(r1, p) && OrientedZone(r4, p) && (!Zone(r3,p) || (Zone(r3,p) && abs(p[r3.coord]- r3.aff)> abs(p[r1.coord]- r1.aff))) && (!Zone(r2,p) || (Zone(r2,p) && abs(p[r2.coord]- r2.aff)> abs(p[r1.coord]- r1.aff)))
            push!(cell.data.points, (p, [t1,t3], 0))
        end
    end
    I = intersection_points(cell, t2,t3,F)
    for p in I
        if OrientedZone(r2, p) && OrientedZone(r3, p) && (!Zone(r1,p) || (Zone(r1,p) && abs(p[r1.coord]- r1.aff)> abs(p[r2.coord]- r2.aff))) && (!Zone(r4,p) || (Zone(r4,p) && abs(p[r4.coord]- r4.aff)> abs(p[r2.coord]- r2.aff)))
            push!(cell.data.points, (p, [t2,t3], 0))
        end
    end
    I = intersection_points(cell, t2,t4,F)
    for p in I
        if OrientedZone(r2, p) && OrientedZone(r4, p) && (!Zone(r1,p) || (Zone(r1,p) && abs(p[r1.coord]- r1.aff)> abs(p[r2.coord]- r2.aff))) && (!Zone(r3,p) || (Zone(r3,p) && abs(p[r3.coord]- r3.aff)> abs(p[r2.coord]- r2.aff)))
            push!(cell.data.points, (p, [t1,t3], 0))
        end
    end
    I = intersection_points(cell, t3,t4,F)
    for p in I
        if OrientedZone(r3, p) && OrientedZone(r4, p) && (!Zone(r1,p) || (Zone(r1,p) && abs(p[r1.coord]- r1.aff)> abs(p[r3.coord]- r3.aff))) && (!Zone(r2,p) || (Zone(r2,p) && abs(p[r2.coord]- r2.aff)> abs(p[r3.coord]- r3.aff)))
            push!(cell.data.points, (p, [t2,t3], 0))
        end
    end
    =#
    #if inCell(cell, ())
end

function new_vertex!(cell::Cell, t1::Int64, t2::Int64, t3::Int64, t4::Int64, F::Array{Face, 1})
    r1 = F[t1]
    r2 = F[t2]
    r3 = F[t3]
    r4 = F[t4]


    if length(union(r1.coord, r2.coord, r3.coord, r4.coord)) == 2
        degen_trisector!(cell, t1, t2, t3, t4, F)
    #elseiflength(union(r1.coord, r2.coord, r3.coord, r4.coord)) == 3 # there is no voronoi vertex
        trisector!(cell, t1, t2, t3, t4, F)
        trisector!(cell, t1, t2, t4, t3, F)
        trisector!(cell, t1, t3, t4, t2, F)
        trisector!(cell, t2, t3, t4, t1, F)
    else #there can be a voronoi vertex
        p = [Inf, Inf, Inf]
        if r1.coord ==1
            x= r1
        elseif r1.coord ==2
            y=r1
        else
            z=r1
        end
        if r2.coord ==1
            x= r2
        elseif r2.coord ==2
            y=r2
        else
            z=r2
        end
        if r3.coord ==1
            x= r3
        elseif r3.coord ==2
            y=r3
        else
            z=r3
        end
        if r4.coord ==1
            x= r4
        elseif r4.coord ==2
            y=r4
        else
            z=r4
        end

        if r1.coord == r2.coord
            p[r1.coord] = (r1.aff+r2.aff)/2
        elseif r1.coord == r3.coord
            p[r1.coord] = (r1.aff+r3.aff)/2
        elseif r1.coord == r4.coord
            p[r1.coord] = (r1.aff+r4.aff)/2
        elseif r2.coord == r3.coord
            p[r2.coord] = (r2.aff+r3.aff)/2
        elseif r2.coord == r4.coord
            p[r2.coord] = (r2.aff+r4.aff)/2
        elseif r3.coord == r4.coord
            p[r3.coord] = (r3.aff+r4.aff)/2
        end

        if p[1] != Inf #
            p[2] = y.dir*abs(p[1]-x.aff) + y.aff
            p[3] = z.dir*abs(p[1]-x.aff) + z.aff
        elseif p[2] != Inf
            p[1] = x.dir*abs(p[2]-y.aff) + x.aff
            p[3] = z.dir*abs(p[2]-y.aff) + z.aff
        else
            p[1] = x.dir*abs(p[3]-z.aff) + x.aff
            p[2] = y.dir*abs(p[3]-z.aff) + y.aff
        end

        p = (p[1], p[2], p[3])
        if inCell(cell, p) && OrientedZone(r1, p) && OrientedZone(r2, p) && OrientedZone(r3, p) && OrientedZone(r4, p)
            println(p)
            push!(cell.data.points, (p, [t1,t2,t3,t4], 0))
            if corner(cell, t1,t2,t3,F) != (Inf,Inf,Inf)
                cor = corner(cell, t1,t2,t3,F)
                if inCell(cell,cor)
                    push!(cell.data.points, (cor, [t1,t2,t3], 0))
                end
            end
            if corner(cell, t1,t2,t4,F) != (Inf,Inf,Inf)
                cor = corner(cell, t1,t2,t4,F)
                if inCell(cell,cor)
                    push!(cell.data.points, (cor, [t1,t2,t4], 0))
                end
            end
            if corner(cell, t1,t3,t4,F) != (Inf,Inf,Inf)
                cor = corner(cell, t1,t3,t4,F)
                if inCell(cell,cor)
                    push!(cell.data.points, (cor, [t1,t3,t4], 0))
                end
            end
            if corner(cell, t2,t3,t4,F) != (Inf,Inf,Inf)
                cor = corner(cell, t2,t3,t4,F)
                if inCell(cell,cor)
                    push!(cell.data.points, (cor, [t2,t3,t4], 0))
                end
            end
        else
            trisector!(cell, t1, t2, t3, t4, F)
            trisector!(cell, t1, t2, t4, t3, F)
            trisector!(cell, t1, t3, t4, t2, F)
            trisector!(cell, t2, t3, t4, t1, F)
        end
    end
end

function new_degen_vertex!(cell::Cell, t1::Int64, t2::Int64, t3::Int64, t4::Int64, F::Array{Face, 1})
    r1 = F[t1]
    r2 = F[t2]
    r3 = F[t3]
    r4 = F[t4]
    #r5 = F[t5]
    #initialization
    x=r1
    y=r1
    z=r1
    #if length(union(r1.coord, r2.coord, r3.coord, r4.coord)) < 3 # there is no voronoi vertex
    #    if length(union(r1.coord, r2.coord, r3.coord, r5.coord)) == 3
    #        new_degen_vertex!(cell, t1,t2,t3,t5,t4,F)
    #    end
    #else #there can be a voronoi vertex
        p = [Inf, Inf, Inf]
        if r1.coord ==1
            x= r1
        elseif r1.coord ==2
            y=r1
        else
            z=r1
        end
        if r2.coord ==1
            x= r2
        elseif r2.coord ==2
            y=r2
        else
            z=r2
        end
        if r3.coord ==1
            x= r3
        elseif r3.coord ==2
            y=r3
        else
            z=r3
        end
        if r4.coord ==1
            x= r4
        elseif r4.coord ==2
            y=r4
        else
            z=r4
        end

        if r1.coord == r2.coord
            p[r1.coord] = (r1.aff+r2.aff)/2
        elseif r1.coord == r3.coord
            p[r1.coord] = (r1.aff+r3.aff)/2
        elseif r1.coord == r4.coord
            p[r1.coord] = (r1.aff+r4.aff)/2
        elseif r2.coord == r3.coord
            p[r2.coord] = (r2.aff+r3.aff)/2
        elseif r2.coord == r4.coord
            p[r2.coord] = (r2.aff+r4.aff)/2
        elseif r3.coord == r4.coord
            p[r3.coord] = (r3.aff+r4.aff)/2
        end

        if p[1] != Inf #
            p[2] = y.dir*abs(p[1]-x.aff) + y.aff
            p[3] = z.dir*abs(p[1]-x.aff) + z.aff
        elseif p[2] != Inf
            p[1] = x.dir*abs(p[2]-y.aff) + x.aff
            p[3] = z.dir*abs(p[2]-y.aff) + z.aff
        else
            p[1] = x.dir*abs(p[3]-z.aff) + x.aff
            p[2] = y.dir*abs(p[3]-z.aff) + y.aff
        end

        p = (p[1], p[2], p[3])
        return p
        #if OrientedZone(r1, p) && OrientedZone(r2, p) && OrientedZone(r3, p) && OrientedZone(r4, p)
            #println(p)
            #push!(cell.data.points, (p, [t1,t2,t3,t4, t5], 0))
        #else
        #    trisector!(cell, t1, t2, t3, t4, F)
        #    trisector!(cell, t1, t2, t4, t3, F)
        #    trisector!(cell, t1, t3, t4, t2, F)
        #    trisector!(cell, t2, t3, t4, t1, F)
        #end
    #end
end
#-----------function to place vertices-----------------------------------------
function place_vertices!(cell::Cell, F::Array{Face, 1})
    if cell.data.in || cell.data.out || length(cell.data.int) + length(cell.data.near) <=1
         return
    end
    temp = deepcopy(cell.data.int)
    append!(temp, cell.data.near)
    temp = unique(temp)
    #=if length(temp) ==2
         #s1 = E[temp[1][1]][temp[1][2]]
         #s2 = E[temp[2][1]][temp[2][2]]
         bisector!(cell,temp[1], temp[2],F) #adds points to "cpoints" and "points"
        #--------  **Place vertices on mesh, and edges accordingly
        return
    end # end of if length(cell.data.int) + length(cell.data.near) ==2
    =#
    if length(temp) ==3
        trisector!(cell,temp[1],temp[2],temp[3],F)
     return
    end

    if length(temp) ==4
        #output = Tuple{Tuple{Float64, Float64, Float64}, Array{Int64,1}, Int64}[]
        #println(temp[1], temp[2], temp[3], temp[4])
        new_vertex!(cell, temp[1], temp[2], temp[3], temp[4], F)
    end

    if length(temp) >=5
        t1 = temp[1]
        flag = false
        i = 2
        t2 = 0
        t3=0
        t4 =0
        while !flag && i <=length(temp)
            if F[temp[i]].coord != F[t1].coord
                t2 = temp[i]
                flag = true
            end
            i+=1
        end
        if t2 !=0
            flag = false
            t3 = 0
            while !flag && i <=length(temp)
                if F[temp[i]].coord != F[t2].coord && F[temp[i]].coord != F[t1].coord
                    t3 = temp[i]
                    flag = true
                end
                i+=1
            end
            if t3 !=0
                flag = false
                t4 = 0
                i = 2
                while !flag && i <=length(temp)
                    if temp[i] != t3 && temp[i] != t2 && temp[i] != t1
                        t4 = temp[i]
                        flag = true
                    end
                    i+=1
                end
            end
        end
        if t1!=0 && t2!=0 && t3!=0 && t4!=0
            labels = Int64[]
         p = new_degen_vertex!(cell, t1, t2, t3, t4, F)
         flag = true
         for i = 1:length(temp)
             if OrientedZone(F[temp[i]], p)
                 push!(labels, temp[i])
             else
                 flag = false
            end
        end
        if flag == true
                push!(cell.data.points, (p, labels, 0))
        end
    end
    end
end
# ---------------DUAL CONTOURING-----------------------------------------------


function cellProc(m::Mesh, cell::Cell,  F::Array{Face, 1})
    if !isleaf(cell)
        cellProc(m,children(cell)[1],F)
        cellProc(m,children(cell)[2],F)
        cellProc(m,children(cell)[3],F)
        cellProc(m,children(cell)[4],F)
        cellProc(m,children(cell)[5],F)
        cellProc(m,children(cell)[6],F)
        cellProc(m,children(cell)[7],F)
        cellProc(m,children(cell)[8],F)

        faceProcH(m,children(cell)[1], children(cell)[2])
        faceProcH(m,children(cell)[3], children(cell)[4])
        faceProcH(m,children(cell)[5], children(cell)[6])
        faceProcH(m,children(cell)[7], children(cell)[8])

        faceProcV(m,children(cell)[1], children(cell)[3])
        faceProcV(m,children(cell)[2], children(cell)[4])
        faceProcV(m,children(cell)[5], children(cell)[7])
        faceProcV(m,children(cell)[6], children(cell)[8])

        faceProcBD(m,children(cell)[1], children(cell)[5])
        faceProcBD(m,children(cell)[2], children(cell)[6])
        faceProcBD(m,children(cell)[3], children(cell)[7])
        faceProcBD(m,children(cell)[4], children(cell)[8])
 else
        place_vertices!(cell, F)

        #remove vertices that are wrong
        #=cell.data.points = unique(cell.data.points)
        cell.data.cpoints = unique(cell.data.cpoints)

        i =1
        while i <= length(cell.data.points)
            for f in cell.data.points[i][2]
                if !OrientedZone(F[f], cell.data.points[i][1])
                    deleteat!(cell.data.points, i)
                    i -= 1
                    break
                end
            end
            i +=1
        end
        i = 1
        while i<=length(cell.data.cpoints)
            for f in cell.data.cpoints[i][2]
                if !OrientedZone(F[f], cell.data.cpoints[i][1])
                    deleteat!(cell.data.cpoints, i)
                    i -=1
                    break
                end
            end
            i+=1
        end =#

        # add central vertices to the mesh
        for i = 1:length(cell.data.points)
            p = cell.data.points[i]
            push_vertex!(m, [p[1][1], p[1][2], p[1][3]])
            cell.data.points[i] = Base.setindex(cell.data.points[i], nbv(m), 3)
        end
        #add edges among central vertices
        for p in cell.data.points
            for q in cell.data.points
                #if p != q && ( (subset(p[2],q[2]) || subset(q[2], p[2])) || length(intersect(p[2],q[2]))>=3)
                if p != q && length(intersect(p[2],q[2]))>=3
                    push_edge!(m, [p[3], q[3]])
                end
            end
        end
end
end

function faceProcH(m::Mesh, cell1::Cell, cell2::Cell)
        if !isleaf(cell1) && !isleaf(cell2)
                faceProcH(m,children(cell1)[2], children(cell2)[1])
                faceProcH(m,children(cell1)[4], children(cell2)[3])
                faceProcH(m,children(cell1)[6], children(cell2)[5])
                faceProcH(m,children(cell1)[8], children(cell2)[7])
        elseif isleaf(cell1) && !isleaf(cell2)
                faceProcH(m,cell1, children(cell2)[1])
                faceProcH(m,cell1, children(cell2)[3])
                faceProcH(m,cell1, children(cell2)[5])
                faceProcH(m,cell1, children(cell2)[7])
        elseif !isleaf(cell1) && isleaf(cell2)
                faceProcH(m,children(cell1)[2], cell2)
                faceProcH(m,children(cell1)[4], cell2)
                faceProcH(m,children(cell1)[6], cell2)
                faceProcH(m,children(cell1)[8], cell2)
        else # isleaf(cell1) && isleaf(cell2)
                #println("faceProcH")
                #dual_edge_H!(m, cell1, cell2)
                #return
                for p in cell1.data.points
                    for q in cell2.data.points
                        #if p != q && ( (subset(p[2],q[2]) || subset(q[2], p[2])) || length(intersect(p[2],q[2]))>=3)
                        if p != q && length(intersect(p[2],q[2]))>=3
                            push_edge!(m, [p[3], q[3]])
                        end
                    end
                end
        end
end

function faceProcV(m::Mesh, cell1::Cell, cell2::Cell)
        if !isleaf(cell1) && !isleaf(cell2)
                faceProcV(m,children(cell1)[3], children(cell2)[1])
                faceProcV(m,children(cell1)[4], children(cell2)[2])
                faceProcV(m,children(cell1)[7], children(cell2)[5])
                faceProcV(m,children(cell1)[8], children(cell2)[6])
        elseif isleaf(cell1) && !isleaf(cell2)
                faceProcV(m,cell1, children(cell2)[1])
                faceProcV(m,cell1, children(cell2)[2])
                faceProcV(m,cell1, children(cell2)[5])
                faceProcV(m,cell1, children(cell2)[6])
        elseif !isleaf(cell1) && isleaf(cell2)
                faceProcV(m,children(cell1)[3], cell2)
                faceProcV(m,children(cell1)[4], cell2)
                faceProcV(m,children(cell1)[7], cell2)
                faceProcV(m,children(cell1)[8], cell2)
        else # isleaf(cell1) && isleaf(cell2)
                #println("faceProcV")
                #dual_edge_V!(m, cell1, cell2)
                #return
                for p in cell1.data.points
                    for q in cell2.data.points
                        #if p != q && ( (subset(p[2],q[2]) || subset(q[2], p[2])) || length(intersect(p[2],q[2]))>=3)
                        if p != q && length(intersect(p[2],q[2]))>=3
                            push_edge!(m, [p[3], q[3]])
                        end
                    end
                end
        end
end

function faceProcBD(m::Mesh, cell1::Cell, cell2::Cell)
        if !isleaf(cell1) && !isleaf(cell2)
                faceProcBD(m,children(cell1)[5], children(cell2)[1])
                faceProcBD(m,children(cell1)[6], children(cell2)[2])
                faceProcBD(m,children(cell1)[7], children(cell2)[3])
                faceProcBD(m,children(cell1)[8], children(cell2)[4])
        elseif isleaf(cell1) && !isleaf(cell2)
                faceProcBD(m,cell1, children(cell2)[1])
                faceProcBD(m,cell1, children(cell2)[2])
                faceProcBD(m,cell1, children(cell2)[3])
                faceProcBD(m,cell1, children(cell2)[4])
        elseif !isleaf(cell1) && isleaf(cell2)
                faceProcBD(m,children(cell1)[5], cell2)
                faceProcBD(m,children(cell1)[6], cell2)
                faceProcBD(m,children(cell1)[7], cell2)
                faceProcBD(m,children(cell1)[8], cell2)
        else # isleaf(cell1) && isleaf(cell2)
                #println("faceProcBD")
                #dual_edge_BD!(m, cell1, cell2)
                #return
                for p in cell1.data.points
                    for q in cell2.data.points
                        #if p != q && ( (subset(p[2],q[2]) || subset(q[2], p[2])) || length(intersect(p[2],q[2]))>=3)
                        if p != q && length(intersect(p[2],q[2]))>=3
                            push_edge!(m, [p[3], q[3]])
                        end
                    end
                end
        end
end


#--- visualization

function poly_facet(f::Face)
    if f.coord ==1
        polygon_verts = [[f.aff, f.contour[i][1], f.contour[i][2]] for i in 1:length(f.contour)]
    elseif f.coord ==2
        polygon_verts = [[f.contour[i][1], f.aff, f.contour[i][2]] for i in 1:length(f.contour)]
    else #f.coord == 3
        polygon_verts = [[f.contour[i][1],  f.contour[i][2], f.aff] for i in 1:length(f.contour)]
    end
    polygon_edges = push!([[i,i+1] for i in 1:length(f.contour)-1], [length(f.contour), 1])
    pol = mesh(polygon_verts, polygon_edges)
    push_face!(pol, [1,2,3,4])
     pol[:color] = Color(0,0,0)
     pol[:size] = 0.3
     pol
end

function cube2(c::Vector{T}, r::T; args...) where T
    m = mesh(T)
    push_vertex!(m,c+[-r,-r,-r])
    push_vertex!(m,c+[r,-r,-r])
    push_vertex!(m,c+[r,r,-r])
    push_vertex!(m,c+[-r,r,-r])

    push_vertex!(m,c+[-r,-r,r])
    push_vertex!(m,c+[r,-r,r])
    push_vertex!(m,c+[r,r,r])
    push_vertex!(m,c+[-r,r,r])

    push_edge!(m, [1,2])
    push_edge!(m, [2,3])
    push_edge!(m, [3,4])
    push_edge!(m, [1,4])
    push_edge!(m, [5,6])
    push_edge!(m, [6,7])
    push_edge!(m, [7,8])
    push_edge!(m, [5,8])
    push_edge!(m, [1,5])
    push_edge!(m, [2,6])
    push_edge!(m, [3,7])
    push_edge!(m, [4,8])

    for arg in args m[arg[1]]=arg[2] end
    return m
end
