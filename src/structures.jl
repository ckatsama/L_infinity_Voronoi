mutable struct Vertex
    coords::Tuple{Float64, Float64}
    reflex::Bool # set to 1 if vertex is reflex, 0 otherwise
    next::Union{Vertex, Void} # pointer to next vertex in the contour
    prev::Union{Vertex, Void} # pointer to previous vertex in the contour
    visited::Bool
    indx::Int64 # index of the vertex in the array that will store all the vertices
end

# constructors for Vertex
function Vertex(x::Float64, y::Float64)
    return Vertex((x,y),false, nothing, nothing, false, 0)
end

function Vertex(coords::Tuple{Float64,Float64})
    return Vertex(coords,false, nothing, nothing, false, 0)
end

mutable struct MySegment
    v1::Float64 # value of nonconstant coordinate of the first endpoint
    v2::Float64 # value of nonconstant coordinate of the second endpoint
    ind1::Int64 # index of first endpoint in the array of all vertices
    ind2::Int64 # index of second endpoint in the array of all vertices
    aff::Float64 # value of constant coordinate
    orient::Bool # set to 0 for horizontal segments, to 1 for vertical
end

# constructors for MySegment
function MySegment(p1::Tuple{Float64, Float64}, p2::Tuple{Float64, Float64}, i1::Int64, i2::Int64)
    ind1 = i1
    ind2 = i2
    if p1[1] == p2[1]
        v1 = p1[2]
        v2 = p2[2]
        aff = p1[1]
        orient = 1
    else
        v1 = p1[1]
        v2 = p2[1]
        aff = p1[2]
        orient = 0
    end
 return MySegment(v1,v2,ind1, ind2, aff,orient)
end

function MySegment(p1::Vertex, p2::Vertex)
    if p1.coords[1] == p2.coords[1]
        v1 = p1.coords[2]
        v2 = p2.coords[2]
        aff = p1.coords[1]
        orient = 1
    else
        v1 = p1.coords[1]
        v2 = p2.coords[1]
        aff = p1.coords[2]
        orient = 0
    end
 return MySegment(v1,v2,p1.indx, p2.indx, aff,orient)
end
