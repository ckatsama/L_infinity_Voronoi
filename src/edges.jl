function getEdges(p::Array{Tuple{Float64, Float64},1}, bot::Int64)
    edges = Array{MySegment,1}()
    for i=1:length(p)-1
        push!(edges, MySegment(p[i],p[i+1], bot+i, bot+i+1))
    end
    push!(edges, MySegment(p[length(p)], p[1], bot+length(p), bot+1))
    return edges
end

function getHorizontalEdges(v::Array{Tuple{Float64, Float64},1}, bot::Int64)
    edges = Array{MySegment,1}()
    for i=1:2:length(v)-1
        push!(edges, MySegment(v[i],v[i+1], bot+i , bot+i+1))
    end
    return edges
end

function getVerticalEdges(v::Array{Tuple{Float64, Float64},1}, bot::Int64)
    edges = Array{MySegment,1}()
    for i=2:2:length(v)-1
        push!(edges, MySegment(v[i],v[i+1], bot+i , bot+i+1))
    end
    push!(edges, MySegment(v[length(v)], v[1], bot+length(v), bot+1))
    return edges
end

function compare(s1::MySegment, s2::MySegment)
    if s1.orient == s2.orient
        return s1.aff < s2.aff
    else
        error("segments are not parallel")
    end
end

# sorts an array of parallel edges wrt their constant value
function SortEdges( E:: Array{MySegment})
    sort!(E, lt=compare)
    return E
end

# returns a point on the carrier of s whose non constant coordinate equals val
function point_on_segment(s::MySegment, val::Float64)
    if s.orient == 0
        return (val, s.aff)
    else
        return (s.aff, val)
    end
end

#computes the Linf distance as long as p \in Z(s)
function dist_from_seg(s::MySegment, p::Tuple{Float64, Float64})
        return abs(p[2-s.orient] - s.aff)
end
