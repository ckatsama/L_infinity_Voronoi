# ZONE PREDICATES
function Zone(f::Face, p::Tuple{Float64,Float64,Float64})
    d = abs(p[f.coord]-f.aff)
    if f.coord ==1
        Q = Rectangle(p[2]-d, p[2]+d, p[3]-d, p[3]+d)
    elseif f.coord == 2
        Q = Rectangle(p[1]-d, p[1]+d, p[3]-d, p[3]+d)
    else
        Q = Rectangle(p[1]-d, p[1]+d, p[2]-d, p[2]+d)
    end
    return rectangleIntersects(f.root, Q)
end

function OrientedZone(f::Face, p::Tuple{Float64,Float64,Float64})
    return Zone(f,p) && f.dir*(p[f.coord]-f.aff) >=0
end

# intersection of cell with OrientedZone

function ZoneCellIntersection(f::Face, cell::Cell)
    if f.coord == 1 # x is constant
        min = abs(cell.boundary.origin[1]-f.aff)
        max = abs(cell.boundary.origin[1]+cell.boundary.widths[1] - f.aff)
        Q = Rectangle(cell.boundary.origin[2]-min, cell.boundary.origin[2]+cell.boundary.widths[2] + min, cell.boundary.origin[3]-min, cell.boundary.origin[3]+cell.boundary.widths[3] + min)
        R = Rectangle(cell.boundary.origin[2]-max, cell.boundary.origin[2]+cell.boundary.widths[2] + max, cell.boundary.origin[3]-max, cell.boundary.origin[3]+cell.boundary.widths[3] + max)
    elseif f.coord == 2 # y is constant
        min = abs(cell.boundary.origin[2]-f.aff)
        max = abs(cell.boundary.origin[2]+cell.boundary.widths[1] - f.aff)
        Q = Rectangle(cell.boundary.origin[1]-min, cell.boundary.origin[1]+cell.boundary.widths[1] + min, cell.boundary.origin[3]-min, cell.boundary.origin[3]+cell.boundary.widths[3] + min)
        R = Rectangle(cell.boundary.origin[1]-max, cell.boundary.origin[1]+cell.boundary.widths[1] + max, cell.boundary.origin[3]-max, cell.boundary.origin[3]+cell.boundary.widths[3] + max)
    else # z is constant
        min = abs(cell.boundary.origin[3]-f.aff)
        max = abs(cell.boundary.origin[3]+cell.boundary.widths[3] - f.aff)
        Q = Rectangle(cell.boundary.origin[1]-min, cell.boundary.origin[1]+cell.boundary.widths[1] + min, cell.boundary.origin[2]-min, cell.boundary.origin[2]+cell.boundary.widths[2] + min)
        R = Rectangle(cell.boundary.origin[1]-max, cell.boundary.origin[1]+cell.boundary.widths[1] + max, cell.boundary.origin[2]-max, cell.boundary.origin[2]+cell.boundary.widths[2] + max)
    end

    if rectangleIntersects(f.root, Q) && f.dir*(cell.boundary.origin[f.coord]-f.aff) >=0 #&& min <= 2 * cell.boundary.widths[1] + minimum(cell.data.dist)
        return true
    elseif rectangleIntersects(f.root, R) && f.dir*(cell.boundary.origin[f.coord]+cell.boundary.widths[1]-f.aff) >=0 #&& max <= 2 * cell.boundary.widths[1] + minimum(cell.data.dist)
        return true
    else
        return false
    end
end

#------SUBDIVISION---------------------
mutable struct Data
    dist::Array{Float64,1}
    vertices::NTuple{8, Array{Int64,1}}#{4, Array{Tuple{Int64, Int64},1}}
    int::Array{Int64,1}
    near::Array{Int64,1}
    center:: Int64 #1 in, 0 out
    clabel::Array{Int64,1}
    points::Array{Tuple{Tuple{Float64, Float64, Float64}, Array{Int64,1}, Int64},1}
    cpoints::Array{Tuple{Tuple{Float64, Float64, Float64}, Array{Int64,1}, Int64},1}
    out::Bool
    in::Bool

    Data() = new()
end

function closest_dist_unoriented(p::Tuple{Float64, Float64, Float64}, faces::Array{Int64, 1}, F::Array{Face, 1})
    dist = Inf
    for i = 1:length(faces)
        f = F[faces[i]]
        if Zone(f, p) && abs(p[f.coord]-f.aff) < dist
            dist = abs(p[f.coord]-f.aff)
        end
    end
    return dist
end

# keeps the first coor of the vertices of the face whose second coordinate is equal to val
function project2first(ar::Array{Tuple{Float64, Float64}, 1}, val::Float64)
    output = Float64[]
    for e in ar
        if e[2] == val
            push!(output, e[1])
        end
    end
    return output
end

function project2second(ar::Array{Tuple{Float64, Float64}, 1}, val::Float64)
    output = Float64[]
    for e in ar
        if e[1] == val
            push!(output, e[2])
        end
    end
    return output
end

function areAdjacent(l1::Int64, l2::Int64, F::Array{Face, 1})
    if F[l1].coord == F[l2].coord
        return false
    end

    if F[l1].coord == 1 && F[l2].coord == 2
        #l1: x=x0, l2: y=y0
        return length(intersect(project2second(F[l1].contour, F[l2].aff),project2second(F[l2].contour, F[l1].aff))) >=2
    elseif F[l1].coord == 1 && F[l2].coord == 3
        return length(intersect(project2first(F[l1].contour, F[l2].aff),project2second(F[l2].contour, F[l1].aff)))>=2
    elseif F[l1].coord == 2 && F[l2].coord == 3
        return length(intersect(project2first(F[l1].contour, F[l2].aff),project2first(F[l2].contour, F[l1].aff)))>=2
    else
        areAdjacent(l2,l1,F)
    end
end


function locate(p::Tuple{Float64,Float64,Float64}, sites::Array{Int64,1}, d::Float64, F::Array{Face, 1})
    #p = (v[1], v[2], v[3])
    label = Int64[]

    if d == Inf
        return label
    end

    for i=1:length(sites)
        r = F[sites[i]]
        if Zone(r,p) && abs(p[r.coord]-r.aff) == d
                push!(label, sites[i])
        end
    end

    # find adjcent faces
    #flag = false
    i = 1
    j = i+1
    while i<= length(label)
        #j = i+1
        while j<=length(label)
            if areAdjacent(label[i], label[j], F)
                #flag = true

                r1 = F[label[i]]
                r2 = F[label[j]]

                #choose coordinate --added on 15/10
                if r2.coord == 1
                    c = 1
                elseif r2.coord == 3
                    c = 2
                else
                    if r1.coord ==1
                        c = 1
                    else
                        c = 2
                    end
                end
                #e1 = E[s[1][1]][s[1][2]]
                #if s[1].orient != s[2].orient
                   #if s[1].v2 == s[2].aff && s[1].aff == s[2].v1
                   k = 1
                   while r2.dir*(r1.contour[k][c]-r2.aff)  == 0 #sum(r1.vertices[k].*r2.normals)+r2.d ==0
                       k +=1
                   end
                   if r2.dir*(r1.contour[k][c]-r2.aff) > 0 #sum(r1.vertices[k].*r2.normals)+r2.d >0 # ccw so angle is convex
                           if !OrientedZone(r1,p) || !OrientedZone(r2,p) #eprepe na isuoyn kai ta 2
                               label = Int64[]
                               return label
                           end
                     else # angle is concave
                         if !OrientedZone(r1,p) && !OrientedZone(r2,p) #eprepe na isuoyn kai ta 2
                             label = Int64[]
                             return label
                         end
                    end

              end
        j +=1
        end
    i +=1
    j = i+1
    end

    i=1
    while i <=length(label)
        r3 = F[label[i]]
        if !OrientedZone(r3, p)
            deleteat!(label,i)
            i += -1
        end
    i +=1
    end
    return label
end

function locate(v::StaticArrays.SArray{Tuple{3},Float64,1,3}, sites::Array{Int64,1}, d::Float64, F::Array{Face, 1})
    p = (v[1], v[2], v[3])
    label = Int64[]

    if d == Inf
        return label
    end

    for i=1:length(sites)
        r = F[sites[i]]
        if Zone(r,p) && abs(p[r.coord]-r.aff) == d
                push!(label, sites[i])
        end
    end

    # find adjcent faces
    #flag = false
    i = 1
    j = i+1
    while i<= length(label)
        #j = i+1
        while j<=length(label)
            if areAdjacent(label[i], label[j], F)
                #flag = true

                r1 = F[label[i]]
                r2 = F[label[j]]

                #choose coordinate --added on 15/10
                if r2.coord == 1
                    c = 1
                elseif r2.coord == 3
                    c = 2
                else
                    if r1.coord ==1
                        c = 1
                    else
                        c = 2
                    end
                end
                #e1 = E[s[1][1]][s[1][2]]
                #if s[1].orient != s[2].orient
                   #if s[1].v2 == s[2].aff && s[1].aff == s[2].v1
                   k = 1
                   while r2.dir*(r1.contour[k][c]-r2.aff)  == 0 #sum(r1.vertices[k].*r2.normals)+r2.d ==0
                       k +=1
                   end
                   if r2.dir*(r1.contour[k][c]-r2.aff) > 0 #sum(r1.vertices[k].*r2.normals)+r2.d >0 # ccw so angle is convex
                           if !OrientedZone(r1,p) || !OrientedZone(r2,p) #eprepe na isuoyn kai ta 2
                               label = Int64[]
                               return label
                           end
                     else # angle is concave
                         if !OrientedZone(r1,p) && !OrientedZone(r2,p) #eprepe na isuoyn kai ta 2
                             label = Int64[]
                             return label
                         end
                    end

              end
        j +=1
        end
    i +=1
    j = i+1
    end

    i=1
    while i <=length(label)
        r3 = F[label[i]]
        if !OrientedZone(r3, p)
            deleteat!(label,i)
            i += -1
        end
    i +=1
    end
    return label
end

function is_strictly_intersecting(t::Int64, cell::Cell, F::Array{Face, 1})
    r = F[t]
    a = cell.boundary.origin[r.coord]
    p = cell.boundary.origin
    d = cell.boundary.widths[1]
    if r.coord == 1
        Q = Rectangle(p[2], p[2]+d, p[3], p[3]+d)
    elseif r.coord == 2
        Q = Rectangle(p[1], p[1]+d, p[3], p[3]+d)
    else
        Q = Rectangle(p[1], p[1]+d, p[2], p[2]+d)
    end
    if (a< r.aff && r.aff < a + d) && rectangleIntersects(r.root, Q)
        #(a <= s.v1 && s.v1 <= a + cell.boundary.widths[1] || a <= s.v2 && s.v2 <= a + cell.boundary.widths[1]) || (b == s.aff &&
            return true
    else
        return false
    end
end

function is_intersecting(t::Int64,p::Tuple{Float64, Float64,Float64}, d::Float64, F::Array{Face, 1})
    r = F[t]
    a = p[r.coord]
    #p = cell.boundary.origin
    #d = cell.boundary.widths[1]
    if r.coord == 1
        Q = Rectangle(p[2], p[2]+d, p[3], p[3]+d)
    elseif r.coord == 2
        Q = Rectangle(p[1], p[1]+d, p[3], p[3]+d)
    else
        Q = Rectangle(p[1], p[1]+d, p[2], p[2]+d)
    end
    if (a<= r.aff && r.aff <= a + d) && rectangleIntersects(r.root, Q)
        #(a <= s.v1 && s.v1 <= a + cell.boundary.widths[1] || a <= s.v2 && s.v2 <= a + cell.boundary.widths[1]) || (b == s.aff &&
            return true
    else
        return false
    end
end


function is_intersecting(t::Int64, cell::Cell, F::Array{Face, 1})
    r = F[t]
    a = cell.boundary.origin[r.coord]
    p = cell.boundary.origin
    d = cell.boundary.widths[1]
    if r.coord == 1
        Q = Rectangle(p[2], p[2]+d, p[3], p[3]+d)
    elseif r.coord == 2
        Q = Rectangle(p[1], p[1]+d, p[3], p[3]+d)
    else
        Q = Rectangle(p[1], p[1]+d, p[2], p[2]+d)
    end
    if (a<= r.aff && r.aff <= a + d) && rectangleIntersects(r.root, Q)
        #(a <= s.v1 && s.v1 <= a + cell.boundary.widths[1] || a <= s.v2 && s.v2 <= a + cell.boundary.widths[1]) || (b == s.aff &&
            return true
    else
        return false
    end
end

function vertextest(cell::Cell, t1::Int64, t2::Int64, t3::Int64, t4::Int64, F::Array{Face, 1})
    r1 = F[t1]
    r2 = F[t2]
    r3 = F[t3]
    r4 = F[t4]
    #r5 = F[t5]
    #initialization
    x=r1
    y=r1
    z=r1
    #if length(union(r1.coord, r2.coord, r3.coord, r4.coord)) < 3 # there is no voronoi vertex
    #    if length(union(r1.coord, r2.coord, r3.coord, r5.coord)) == 3
    #        new_degen_vertex!(cell, t1,t2,t3,t5,t4,F)
    #    end
    #else #there can be a voronoi vertex
        p = [Inf, Inf, Inf]
        if r1.coord ==1
            x= r1
        elseif r1.coord ==2
            y=r1
        else
            z=r1
        end
        if r2.coord ==1
            x= r2
        elseif r2.coord ==2
            y=r2
        else
            z=r2
        end
        if r3.coord ==1
            x= r3
        elseif r3.coord ==2
            y=r3
        else
            z=r3
        end
        if r4.coord ==1
            x= r4
        elseif r4.coord ==2
            y=r4
        else
            z=r4
        end

        if r1.coord == r2.coord
            p[r1.coord] = (r1.aff+r2.aff)/2
        elseif r1.coord == r3.coord
            p[r1.coord] = (r1.aff+r3.aff)/2
        elseif r1.coord == r4.coord
            p[r1.coord] = (r1.aff+r4.aff)/2
        elseif r2.coord == r3.coord
            p[r2.coord] = (r2.aff+r3.aff)/2
        elseif r2.coord == r4.coord
            p[r2.coord] = (r2.aff+r4.aff)/2
        elseif r3.coord == r4.coord
            p[r3.coord] = (r3.aff+r4.aff)/2
        end

        if p[1] != Inf #
            p[2] = y.dir*abs(p[1]-x.aff) + y.aff
            p[3] = z.dir*abs(p[1]-x.aff) + z.aff
        elseif p[2] != Inf
            p[1] = x.dir*abs(p[2]-y.aff) + x.aff
            p[3] = z.dir*abs(p[2]-y.aff) + z.aff
        else
            p[1] = x.dir*abs(p[3]-z.aff) + x.aff
            p[2] = y.dir*abs(p[3]-z.aff) + y.aff
        end

        p = (p[1], p[2], p[3])
        return p
        #if OrientedZone(r1, p) && OrientedZone(r2, p) && OrientedZone(r3, p) && OrientedZone(r4, p)
            #println(p)
            #push!(cell.data.points, (p, [t1,t2,t3,t4, t5], 0))
        #else
        #    trisector!(cell, t1, t2, t3, t4, F)
        #    trisector!(cell, t1, t2, t4, t3, F)
        #    trisector!(cell, t1, t3, t4, t2, F)
        #    trisector!(cell, t2, t3, t4, t1, F)
        #end
    #end
end

function inCell(cell::Cell, p::Tuple{Float64, Float64, Float64})
    return cell.boundary.origin[1] <= p[1] && p[1] <= cell.boundary.origin[1] + cell.boundary.widths[1] && cell.boundary.origin[2] <= p[2] && p[2] <= cell.boundary.origin[2] + cell.boundary.widths[1] && cell.boundary.origin[3] <= p[3] && p[3] <= cell.boundary.origin[3] + cell.boundary.widths[1]
end

function needs_refinement(r::MyRefinery, cell::Cell, F::Array{Face, 1})
     temp = deepcopy(cell.data.int)
     temp = append!(temp, cell.data.near)
     center = (cell.boundary.origin[1]+cell.boundary.widths[1]/2, cell.boundary.origin[2]+cell.boundary.widths[2]/2, cell.boundary.origin[3]+cell.boundary.widths[3]/2)

     if length(cell.data.int) == 0
         temp1 = closest_dist_unoriented(center, temp,F)
         temp2 = locate(center, temp, temp1, F)
         if length(temp2) == 0 || cell.data.center == 0 #point is out
             cell.data.out = 1
             return false
         end
     else
         temp1 = closest_dist_unoriented(center, cell.data.int,F)
         temp2 = locate(center,cell.data.int, temp1, F)
         if length(temp2) == 0 || cell.data.center == 0 #point is out
             flag = true
             i = 1
             while i <= length(cell.data.int) && flag
                 flag = !is_strictly_intersecting(cell.data.int[i],cell,F)
                 i +=1
             end
             if flag == true
                 cell.data.out = 1
                 return false
             end
         end
     end


     if length(cell.data.int)+ length(cell.data.near) <=4
         return false
     elseif length(cell.data.int)+ length(cell.data.near) <=7
         temp = deepcopy(cell.data.int)
         append!(temp, cell.data.near)
         t1 = temp[1]
         flag = false
         i = 2
         t2 = 0
         t3=0
         t4 =0
         while !flag && i <=length(temp)
             if F[temp[i]].coord != F[t1].coord
                 t2 = temp[i]
                 flag = true
             end
             i+=1
         end
         if t2 !=0
             flag = false
             t3 = 0
             while !flag && i <=length(temp)
                 if F[temp[i]].coord != F[t2].coord && F[temp[i]].coord != F[t1].coord
                     t3 = temp[i]
                     flag = true
                 end
                 i+=1
             end
             if t3 !=0
                 flag = false
                 t4 = 0
                 i = 2
                 while !flag && i <=length(temp)
                     if temp[i] != t3 && temp[i] != t2 && temp[i] != t1
                         t4 = temp[i]
                         flag = true
                     end
                     i+=1
                 end
             end
         end
         if t1!=0 && t2!=0 && t3!=0 && t4!=0
          p = vertextest(cell, t1, t2, t3, t4, F)
          if p[1]!=Inf && p[2]!=Inf && p[3]!=Inf && inCell(cell,p)
              dist = abs(p[F[t1].coord]-F[t1].aff)
              flag = true
              for i = 1:length(temp)
                  if !OrientedZone(F[temp[i]], p) || abs(p[F[temp[i]].coord]-F[temp[i]].aff) != dist

                      flag = false
                  end
              end
              if flag == true
                 return false
             else
                 #println("stuck")
                 return true
            end
        else
            return true
         end
        else
            return true
        end
    else
        return true
    end


    #if maximum(cell.boundary.widths) > r.tolerance && length(cell.data.int)+ length(cell.data.near) >=5
    #    return true
    #else
    #    return false
    #    end
end

function refine_cell!(cell::Cell, F::Array{Face, 1})
    cell.data.int = Int64[]
    cell.data.near = Int64[]
    temp = deepcopy(parent(cell).data.int)
    temp = append!(temp, parent(cell).data.near)
    center = (cell.boundary.origin[1]+cell.boundary.widths[1]/2, cell.boundary.origin[2]+cell.boundary.widths[2]/2, cell.boundary.origin[3]+cell.boundary.widths[3]/2)

    if length(parent(cell).data.int) == 0
        temp1 = closest_dist_unoriented(center, temp,F)
        temp2 = locate(center, temp, temp1, F)
        if length(temp2) == 0 #point is out
            d = cell.boundary.widths[1]
        else
        #temp1 = closest_dist_unoriented(center, temp,E)
            d = cell.boundary.widths[1] + temp1
        end
    else
        temp1 = closest_dist_unoriented(center, parent(cell).data.int,F)
        temp2 = locate(center, parent(cell).data.int, temp1, F)
        if length(temp2) == 0 #point is out
            d = cell.boundary.widths[1]
            cell.data.center = 0
        else
            temp1 = closest_dist_unoriented(center, temp,F)
            d = cell.boundary.widths[1] + temp1
        end
    end

    origin2 = (center[1]- d, center[2]-d, center[3]- d)
    for i=1:length(temp)
        if is_intersecting(temp[i], cell,F)
            #println("is intersecting")
            push!(cell.data.int, temp[i])
        elseif ZoneCellIntersection(F[temp[i]], cell) && is_intersecting(temp[i],origin2, 2*d, F)
            #println("is in radius")
            push!(cell.data.near, temp[i])
        end
    end
end

function adaptivesampling!(root::Cell, r::MyRefinery, F::Array{Face, 1})
    refinement_queue = [root]
    while !isempty(refinement_queue)
        cell = pop!(refinement_queue)
        if needs_refinement(r, cell, F)
            # data is inherited to the children
            L1 = deepcopy(cell.data)
            L2 = deepcopy(cell.data)
            L3 = deepcopy(cell.data)
            L4 = deepcopy(cell.data)
            L5 = deepcopy(cell.data)
            L6 = deepcopy(cell.data)
            L7 = deepcopy(cell.data)
            L8 = deepcopy(cell.data)

            split!(cell, [L1, L2, L3, L4, L5, L6, L7, L8])

            #a = Array{Tuple,1}(12)
            #a[1] = (1,2)
            #a[2] = (1,3)
            #a[3] = (1,5)
            #a[4] = (2,4)
            #a[5] = (2,6)
            #a[6] = (3,4)
            #a[7] = (3,7)
            #a[8] = (4,8)
            #a[9] = (5,6)
            #a[10] = (5,7)
            #a[11] = (6,8)
            #a[12] = (7,8)

            #b = Array{Array{Tuple,1},1}(6)
            #b[1] = [(1,6), (2,5)]
            #b[2] = [(2,8), (4,6)]
            #b[3] = [(3,8), (4,7)]
            #b[4] = [(1,7), (3,5)]
            #b[5] = [(1,4), (2,3)]
            #b[6] = [(5,8), (6,7)]

            #c = [(1,8), (2,7), (3,6), (4,5)]

            #temp = deepcopy(cell.data.int)
            #temp = append!(temp, cell.data.near)

            for child in children(cell)
                child.data.center = 2
                refine_cell!(child, F)
                child.data.int = unique(child.data.int)
                child.data.near = unique(child.data.near)
            end
            append!(refinement_queue, children(cell))
        end
    end
    root
end

# ----exported function
function subdivide!(F::Array{Face,1}, r::MyRefinery)
  i = 1
  Xmin = Inf
  Ymin = Inf
  Zmin = Inf
  Xmax = -Inf
  Ymax = -Inf
  Zmax = -Inf
  for f in F
      decompose!(f)
      #println(i)
      i +=1
      if f.coord == 1 && f.aff < Xmin
          Xmin = f.aff
      elseif f.coord ==2 && f.aff < Ymin
          Ymin = f.aff
      elseif f.coord ==3 && f.aff < Zmin
          Zmin = f.aff
      end
      if f.coord == 1 && f.aff > Xmax
          Xmax = f.aff
      elseif f.coord ==2 && f.aff > Ymax
          Ymax = f.aff
      elseif f.coord ==3 && f.aff > Zmax
          Zmax = f.aff
      end
   end
      dat = Data()
      dat.dist = Array{Float64,1}(8)
      dat.int = Array{Int64}(length(F))
      for i =1:length(F)
      dat.int[i] = i
      end
      dat.near = Int64[]
      dat.vertices = (Int64[], Int64[], Int64[], Int64[], Int64[], Int64[], Int64[] , Int64[])
      dat.out = false
      dat.in = false
      dat.center = 2
      dat.clabel = Int64[]
      dat.points = Tuple{Tuple{Float64, Float64, Float64}, Array{Int64,1}, Int64}[]
      dat.cpoints = Tuple{Tuple{Float64, Float64, Float64}, Array{Int64,1}, Int64}[]
      wid = max(Xmax - Xmin, Ymax-Ymin, Zmax - Zmin)
      root = Cell(SVector(Xmin - 10.0, Ymin - 10.0,Zmin - 10.0 ), SVector(wid + 20.0, wid + 20.0, wid + 20.0), dat)


      adaptivesampling!(root, r, F)
      root
end
