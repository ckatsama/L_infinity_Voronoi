function SemiAlgebraicTypes.mesh(root::RegionTrees.Cell)
           m = SemiAlgebraicTypes.mesh(Float64)
           for leaf in allleaves(root)
               v = hcat(collect(vertices(leaf.boundary))...)
               if size(v,1) == 2
                   v = vcat(v,fill(0.,1,size(v,2)))
               end
               n = nbv(m)
               for j in 1:size(v,2)
                   push_vertex!(m, v[1:3,j])
               end
               push_edge!(m, [n+1,n+2])
               push_edge!(m, [n+2,n+4])
               push_edge!(m, [n+4,n+3])
               push_edge!(m, [n+3,n+1])
           end
           m
end

function create_right_point(coord::Bool, val1::Float64, val2::Float64)
    #is the segment is horizontal (coord=0) then assign val1 to the first coordinate
    if coord == 0
        return (val1, val2)
    else
        return (val2, val1)
    end
end

function f_1_3(z::Float64, a::Float64, b::Float64, coord::Int64)
    if coord ==1
        return z+b-a
    else
        return z-b+a
    end
end

function f_2_4(z::Float64, a::Float64, b::Float64, coord::Int64)
    if coord ==1
        return -z+b+a
    else
        return -z+b+a
    end
end

function corner(t1::Tuple{Int64, Int64}, t2::Tuple{Int64, Int64}, E::Vector{Vector{MySegment}})
    output = (Inf,Inf)
    s1 = E[t1[1]][t1[2]]
    s2 = E[t2[1]][t2[2]]
    temp1 = Vector{Tuple{Float64,Float64}}(2)
    temp2 = Vector{Tuple{Float64,Float64}}(2)
    temp1[1] = point_on_segment(s1,s1.v1)
    temp1[2] = point_on_segment(s1,s1.v2)
    temp2[1] = point_on_segment(s2,s2.v1)
    temp2[2] = point_on_segment(s2,s2.v2)
    if length(intersect(temp1,temp2))==1
        return intersect(temp1,temp2)[1]
    else
        return output
    end
end

function inCell(t::Tuple{Float64, Float64}, cell::Cell)
    return t[1] >= cell.boundary.origin[1]  && t[1] <= cell.boundary.origin[1] +cell.boundary.widths[1] && t[2] >= cell.boundary.origin[2]  && t[2] <= cell.boundary.origin[2] +cell.boundary.widths[1]
end

function point_outside(p::Tuple{Float64, Float64},cell::Cell, E::Vector{Vector{MySegment}})
    temp = copy(cell.data.int)
    temp = append!(temp, cell.data.near)
    flag = true
    if length(cell.data.int) == 0
        return false
    else
        temp1 = closest_dist_unoriented(p, cell.data.int,E)
        temp2 = locate(p, cell.data.int, temp1, E)
        if length(temp2) == 0
            return true # point outside
        else
            return false
        end
    end
end

function place_vertices!(m::Mesh, cell::Cell, E::Vector{Vector{MySegment}})

    if cell.data.in || cell.data.out || length(cell.data.int) + length(cell.data.near) <=1
         return
    end

    if length(cell.data.int) + length(cell.data.near) ==2
         temp = deepcopy(cell.data.int)
         append!(temp, cell.data.near)
         if E[temp[1][1]][temp[1][2]].aff == E[temp[2][1]][temp[2][2]].aff &&  E[temp[1][1]][temp[1][2]].orient == E[temp[2][1]][temp[2][2]].orient
             error("Colinear edges detected:", temp[1], temp[2])
         end
         pob = points_on_bis!(m,cell,temp[1], temp[2],E) #this function call also puts the vertices to the mesh
         cell.data.points = deepcopy(pob) #create_bis(cell,s1,s2)
        return
    end # end of if length(cell.data.int) + length(cell.data.near) ==2

    if length(cell.data.int)+ length(cell.data.near) == 3
        temp = deepcopy(cell.data.int)
        append!(temp, cell.data.near)
        list1 = Tuple{Float64, Float64}[]
        list2 = Tuple{Float64, Float64}[]
        list3 = Tuple{Float64, Float64}[]
        list1 = intersection_points(cell, temp[1], temp[2], temp[3], E)
        list2 = intersection_points(cell, temp[1], temp[3], temp[2], E)
        list3 = intersection_points(cell, temp[2], temp[3], temp[1], E)


        if length(list1) == 0
            #create_bis(cell, s1, s3)
            if length(list2)== 2
                pob = points_on_bis!(m,cell,temp[1], temp[3], E)
                if length(pob) >=1 && point_outside(pob[1][1], cell, E) == false
                    cell.data.points = append!(cell.data.points, pob) #create_bis(cell,s1,s2)
                end
            end
            #create_bis(cell, s2, s3)
            if length(list3)== 2
                pob = points_on_bis!(m,cell,temp[2], temp[3], E)
                if length(pob) >=1 && point_outside(pob[1][1], cell, E) == false
                    cell.data.points = append!(cell.data.points, pob) #create_bis(cell,s1,s2)
                end
            end
        elseif length(list2) == 0
            #create_bis(cell, s1, s2)
            if length(list1)== 2
                pob = points_on_bis!(m,cell,temp[1], temp[2], E)
                if length(pob) >=1 && point_outside(pob[1][1], cell, E) == false
                    cell.data.points = append!(cell.data.points, pob) #create_bis(cell,s1,s2)
                end
            end
            #create_bis(cell, s2, s3)
            if length(list3)== 2
                pob = points_on_bis!(m,cell,temp[2], temp[3], E)
                if length(pob) >=1 && point_outside(pob[1][1], cell, E) == false
                    cell.data.points = append!(cell.data.points, pob) #create_bis(cell,s1,s2)
                end
            end
        elseif length(list3) == 0
            #create_bis(cell, s1, s2)
            if length(list1)== 2
                pob = points_on_bis!(m,cell,temp[1], temp[2], E)
                if length(pob) >=1 && point_outside(pob[1][1], cell, E) == false
                    cell.data.points = append!(cell.data.points, pob) #create_bis(cell,s1,s2)
                end
            end
            #create_bis(cell, s1, s3)
            if length(list2)== 2
                pob = points_on_bis!(m,cell,temp[1], temp[3], E)
                if length(pob) >=1 && point_outside(pob[1][1], cell, E) == false
                    cell.data.points = append!(cell.data.points, pob) #create_bis(cell,s1,s2)
                end
            end
       

        else # now there exists a voronoi vertex
            if (E[temp[1][1]][temp[1][2]].aff == E[temp[2][1]][temp[2][2]].aff && E[temp[1][1]][temp[1][2]].orient == E[temp[2][1]][temp[2][2]].orient)  ||
                (E[temp[2][1]][temp[2][2]].aff == E[temp[3][1]][temp[3][2]].aff && E[temp[2][1]][temp[2][2]].orient == E[temp[3][1]][temp[3][2]].orient)  ||
                (E[temp[1][1]][temp[1][2]].aff == E[temp[3][1]][temp[3][2]].aff && E[temp[1][1]][temp[1][2]].orient == E[temp[3][1]][temp[3][2]].orient)
                error("Colinear edges detected among:", temp[1], temp[2], temp[3])
            end
            vertex = find_vor_vertex(m,cell, temp[1], temp[2], temp[3], E)
            v = vertex[1]
            if OrientedZone(E[temp[1][1]][temp[1][2]], v) && OrientedZone(E[temp[2][1]][temp[2][2]], v) OrientedZone(E[temp[3][1]][temp[3][2]], v)
                push!(cell.data.points, vertex)
            end
            

        end
    end # end of condition with 3 sites =#
    if length(cell.data.int)+ length(cell.data.near) >=4
        temp = deepcopy(cell.data.int)
        append!(temp, cell.data.near)
        vertex = find_vor_vertex2(temp[1], temp[2], temp[3], E)
        push_vertex!(m, [vertex[1], vertex[2], 0.0])
        push!(cell.data.points, (vertex, temp, Int(length(m.points)/3)))
        index = Int(length(m.points)/3)
        for t in temp
            for s in temp
                if corner(t,s,E) != (Inf, Inf) && inCell(corner(t,s,E), cell)
                    corn = corner(t,s,E)
                    push_vertex!(m, [corn[1], corn[2], 0.0])
                    push_edge!(m, [index, Int(length(m.points)/3)])
                end
            end
        end
   end
end #function

function on_boundary(p::Tuple{Float64, Float64}, cell::Cell)
    x0 = vertices(cell)[1][1]
    y0 = vertices(cell)[1][2]
    if x0 <= p[1] && p[1] <= x0 + cell.boundary.widths[1] && y0 <= p[2] && p[2] <= y0 + cell.boundary.widths[1] #is inside the closed cell
        if x0 == p[1] || p[1] == x0 + cell.boundary.widths[1] || y0 == p[2] || p[2] == y0 + cell.boundary.widths[1]
            return true
        else
            return false
        end
    end
    return false
end

function on_seg(p::Tuple{Float64, Float64}, s::MySegment)
    if s.orient ==0
        return p[2] == s.aff && min(s.v1, s.v2) <= p[1] && max(s.v1,s.v2) >= p[1]
    else
        return p[1] == s.aff && min(s.v1, s.v2) <= p[2] && max(s.v1,s.v2) >= p[2]
    end
end

function points_on_bis!(m::Mesh, cell::Cell,t1::Tuple{Int64, Int64}, t2::Tuple{Int64, Int64}, E::Vector{Vector{MySegment}} )
        output = Tuple{Tuple{Float64, Float64}, Array{Tuple{Int64, Int64},1}, Int64}[]
        s1 = E[t1[1]][t1[2]]
        s2 = E[t2[1]][t2[2]]
    if s1.orient == s2.orient && s1.aff != s2.aff# sites are parallel
        c = s1.aff/2 + s2.aff/2
        if  vertices(cell)[1][2-s1.orient] <= c && c <= vertices(cell)[1][2-s1.orient]+ cell.boundary.widths[1]
            p1 = create_right_point(s1.orient, vertices(cell)[1][1+s1.orient], c)
            p2 = create_right_point(s1.orient, vertices(cell)[1][1+s1.orient] + cell.boundary.widths[1], c)
            if OrientedZone(s1, p1) && OrientedZone(s1, p2) && OrientedZone(s2, p1) && OrientedZone(s2, p2)
                if (p1[1]+p2[1])/2 != cell.boundary.origin[1]+cell.boundary.widths[1] && (p1[2]+p2[2])/2 != cell.boundary.origin[2]+cell.boundary.widths[2]
                    push_vertex!(m, [(p1[1]+p2[1])/2, (p1[2]+p2[2])/2, 0.0])
                    push!(output,(((p1[1]+p2[1])/2, (p1[2]+p2[2])/2), [t1, t2], Int(length(m.points)/3)))
                end
                return output
            end
        end

    elseif s1.orient == s2.orient && s1.aff == s2.aff
        d = min(abs(s1.v1-s2.v1), abs(s1.v1-s2.v2), abs(s1.v2-s2.v1), abs(s1.v2-s2.v2))
        if abs(s1.v1-s2.v1) == d
            c = min(s1.v1,s2.v1) + d/2
        elseif abs(s1.v1-s2.v2)==d
            c = min(s1.v1,s2.v2) + d/2
        elseif abs(s1.v2-s2.v1) == d
            c = min(s1.v2,s2.v1) + d/2
        elseif abs(s1.v2-s2.v2) == d
            c = min(s1.v2,s2.v2) + d/2
        end
        if  vertices(cell)[1][1+s1.orient] <= c && c <= vertices(cell)[1][1+s1.orient]+ cell.boundary.widths[1]
            p1 = create_right_point(Bool(1-s1.orient), vertices(cell)[1][2-s1.orient], c)
            p2 = create_right_point(Bool(1-s1.orient), vertices(cell)[1][2-s1.orient] + cell.boundary.widths[1], c)
            if OrientedZone(s1, p1) && OrientedZone(s1, p2) && OrientedZone(s2, p1) && OrientedZone(s2, p2)
                #n = mesh( [[p1.x, p1.y, 0.0], [p2.x, p2.y, 0.0]], [[1,2]])
                if (p1[1]+p2[1])/2 != cell.boundary.origin[1]+cell.boundary.widths[1] && (p1[2]+p2[2])/2 != cell.boundary.origin[2]+cell.boundary.widths[2]
                    push_vertex!(m, [(p1[1]+p2[1])/2, (p1[2]+p2[2])/2, 0.0])
                    push!(output,(((p1[1]+p2[1])/2, (p1[2]+p2[2])/2), [t1, t2], Int(length(m.points)/3)))
                end
                return output
            end
        end
    else # segments perpendicular
        if s1.orient == 0
            a = s2.aff
            b = s1.aff
        else
            a = s1.aff
            b = s2.aff
        end

        c = 0
        #expr = Expr[]
        x = 0.0
        y = 0.0
        if  (s1.v1-s1.v2)*(s2.v1-s2.v2) < 0 # y = x+b-a  (1rst and 3rd quadrant)
            c = 0
        else # y = -x+b+a  (2nd and 4rth quadrant)
            c = 1
        end
        x0 = vertices(cell)[1][1]
        y0 = vertices(cell)[1][2]

        y = y0
        if c == 0 # 1-3 quadrants

            int1 = f_1_3(y, a, b, 2)

            if x0 <= int1 && int1 <= x0 + cell.boundary.widths[1] #it intersects faces 1-2
                x = x0 + cell.boundary.widths[1]
                int2 = f_1_3(x, a, b, 1)

                p1 = (int1,y0)
                p2 = (x, int2)



                if OrientedZone(s1, p1) && OrientedZone(s1, p2) && OrientedZone(s2, p1) && OrientedZone(s2, p2)
                   if p1 != p2
                       if p1 == (a,b) && on_seg((a,b), s1) && on_seg((a,b), s2)
                           push_vertex!(m, [p1[1], p1[2], 0.0])
                        
                           push!(output,(p1, [t1, t2], Int(length(m.points)/3)))
                       elseif p2 == (a,b) && on_seg((a,b), s1) && on_seg((a,b), s2)
                           push_vertex!(m, [p2[1], p2[2], 0.0])
                           push!(output,(p2, [t1, t2], Int(length(m.points)/3)))
                       else
                           push_vertex!(m, [(p1[1]+p2[1])/2, (p1[2]+p2[2])/2, 0.0])
                           push!(output,(((p1[1]+p2[1])/2, (p1[2]+p2[2])/2), [t1, t2], Int(length(m.points)/3)))
                       end
                   else #p1 == p2

                           push_vertex!(m, [p1[1], p1[2], 0.0])
                           push!(output,(p1, [t1, t2], Int(length(m.points)/3)))

                    end

                   return output

                    
                elseif x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1] && on_seg((a,b), s1) && on_seg((a,b), s2)
                   push_vertex!(m, [a, b, 0.0])
                   push!(output,((a, b), [t1, t2], Int(length(m.points)/3)))
                   return output
               end
           else  # see if it intersects faces 3-4

            y = y0 +cell.boundary.widths[1]
           
            int1 = f_1_3(y, a, b, 2)

            if x0 <= int1 && int1 <= x0 + cell.boundary.widths[1] #it intersects faces 3-4
                x = x0
                int2 = f_1_3(x, a, b, 1)

                p1 = (int1,y0+ cell.boundary.widths[1])
                p2 = (x, int2)

                if OrientedZone(s1, p1) && OrientedZone(s1, p2) && OrientedZone(s2, p1) && OrientedZone(s2, p2)
                    #n = mesh( [[p1.x, p1.y, 0.0], [p2.x, p2.y, 0.0]], [[1,2]])
                    if p1 != p2
                    if p1 == (a,b) && on_seg((a,b), s1) && on_seg((a,b), s2)
                        push_vertex!(m, [p1[1], p1[2], 0.0])
                        push!(output,(p1, [t1, t2], Int(length(m.points)/3)))
                    elseif p2 == (a,b) && on_seg((a,b), s1) && on_seg((a,b), s2)
                        push_vertex!(m, [p2[1], p2[2], 0.0])

                        push!(output,(p2, [t1, t2], Int(length(m.points)/3)))
                    else
                        push_vertex!(m, [(p1[1]+p2[1])/2, (p1[2]+p2[2])/2, 0.0])
                        push!(output,(((p1[1]+p2[1])/2, (p1[2]+p2[2])/2), [t1, t2], Int(length(m.points)/3)))
                    end
                 end
                    return output
                elseif x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1] && on_seg((a,b), s1) && on_seg((a,b), s2)

                   push_vertex!(m, [a, b, 0.0])
                   push!(output,((a, b), [t1, t2], Int(length(m.points)/3)))
                   return output
               end
           end
       end # end of subcase with intersections of faces1-2 , 3-4
        else # c ==1
            int1 = f_2_4(y, a, b, 1)

            if x0 <= int1 && int1 <= x0 + cell.boundary.widths[1] #it intersects faces 1-4
                x = x0
                #int2 = eval(expr[2])
                int2 = f_2_4(x, a, b, 2)

                p1 = (int1,y0)
                p2 = (x, int2)

                if OrientedZone(s1, p1) && OrientedZone(s1, p2) && OrientedZone(s2, p1) && OrientedZone(s2, p2)
                    if p1 != p2
                    if p1 == (a,b) && on_seg((a,b), s1) && on_seg((a,b), s2)
                        push_vertex!(m, [p1[1], p1[2], 0.0])
                        push!(output,(p1, [t1, t2], Int(length(m.points)/3)))
                    elseif p2 == (a,b) && on_seg((a,b), s1) && on_seg((a,b), s2)
                        push_vertex!(m, [p2[1], p2[2], 0.0])
                        push!(output,(p2, [t1, t2], Int(length(m.points)/3)))
                    else
                        push_vertex!(m, [(p1[1]+p2[1])/2, (p1[2]+p2[2])/2, 0.0])
                        push!(output,(((p1[1]+p2[1])/2, (p1[2]+p2[2])/2), [t1, t2], Int(length(m.points)/3)))
                    end
                else #p1 == p2
                        push_vertex!(m, [p1[1], p1[2], 0.0])
                        push!(output,(p1, [t1, t2], Int(length(m.points)/3)))
                 end
                    return output
                elseif x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1] && on_seg((a,b), s1) && on_seg((a,b), s2)

                   push_vertex!(m, [a, b, 0.0])
                   push!(output,((a, b), [t1, t2], Int(length(m.points)/3)))
                   return output
                end
            else  #see if it intersects faces 2-3

                x = x0 +cell.boundary.widths[1]
                #int1 = eval(expr[2])
                int1 = f_2_4(x, a, b, 2)

                if y0 <= int1 && int1 <= y0 + cell.boundary.widths[1] #it intersects faces 3-2
                    y = y0 + cell.boundary.widths[1]
                    #int2 = eval(expr[1])
                    int2 = f_2_4(y, a, b, 1)

                    p1 = (x0+ cell.boundary.widths[1], int1)
                    p2 = (int2,y)

                    if OrientedZone(s1, p1) && OrientedZone(s1, p2) && OrientedZone(s2, p1) && OrientedZone(s2, p2)
                        if p1 != p2
                        #n = mesh( [[p1.x, p1.y, 0.0], [p2.x, p2.y, 0.0]], [[1,2]])
                        if p1 == (a,b) && on_seg((a,b), s1) && on_seg((a,b), s2)
                            push_vertex!(m, [p1[1], p1[2], 0.0])
                            push!(output,(p1, [t1, t2], Int(length(m.points)/3)))
                        elseif p2 == (a,b) && on_seg((a,b), s1) && on_seg((a,b), s2)
                            push_vertex!(m, [p2[1], p2[2], 0.0])
                            push!(output,(p2, [t1, t2], Int(length(m.points)/3)))
                        else
                            push_vertex!(m, [(p1[1]+p2[1])/2, (p1[2]+p2[2])/2, 0.0])
                            push!(output,(((p1[1]+p2[1])/2, (p1[2]+p2[2])/2), [t1, t2], Int(length(m.points)/3)))
                        end
                     end
                        return output
                    elseif x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1] && on_seg((a,b), s1) && on_seg((a,b), s2)

                       push_vertex!(m, [a, b, 0.0])
                       push!(output,((a, b), [t1, t2], Int(length(m.points)/3)))
                       return output
                    end
                end
            end # end of subcases with intesections of faces 2-3 or 1-4
       end  # end of if c==0 or 1
    end  # end of if that checks th eorientations of s1,s2
    return output
end

function intersection_points(cell::Cell, t1::Tuple{Int64, Int64}, t2::Tuple{Int64, Int64}, t3::Tuple{Int64, Int64}, E::Vector{Vector{MySegment}})
    output = Tuple{Float64, Float64}[]
    s1 = E[t1[1]][t1[2]]
    s2 = E[t2[1]][t2[2]]
    s3 = E[t3[1]][t3[2]]
    if s1.orient == s2.orient && s1.aff != s2.aff# sites are parallel
        c = s1.aff/2 + s2.aff/2
        if  vertices(cell)[1][2-s1.orient] <= c && c <= vertices(cell)[1][2-s1.orient]+ cell.boundary.widths[1]
            p1 = create_right_point(s1.orient, vertices(cell)[1][1+s1.orient], c)
            p2 = create_right_point(s1.orient, vertices(cell)[1][1+s1.orient] + cell.boundary.widths[1], c)
            if OrientedZone(s1, p1) &&  OrientedZone(s2, p1) && ((Zone(s3, p1) && dist_from_seg(s3,p1) >= dist_from_seg(s1,p1)) || !Zone(s3, p1))
                push!(output, p1)
            end
            if OrientedZone(s1, p2) &&  OrientedZone(s2, p2) && ((Zone(s3, p2) && dist_from_seg(s3,p2) >= dist_from_seg(s1,p2)) || !Zone(s3, p2))
                push!(output, p2)
            end
        end

    elseif s1.orient == s2.orient && s1.aff == s2.aff
        d = min(abs(s1.v1-s2.v1), abs(s1.v1-s2.v2), abs(s1.v2-s2.v1), abs(s1.v2-s2.v2))
        if abs(s1.v1-s2.v1) == d
            c = min(s1.v1,s2.v1) + d/2
        elseif abs(s1.v1-s2.v2)==d
            c = min(s1.v1,s2.v2) + d/2
        elseif abs(s1.v2-s2.v1) == d
            c = min(s1.v2,s2.v1) + d/2
        elseif abs(s1.v2-s2.v2) == d
            c = min(s1.v2,s2.v2) + d/2
        end
        if  vertices(cell)[1][1+s1.orient] <= c && c <= vertices(cell)[1][1+s1.orient]+ cell.boundary.widths[1]
            p1 = create_right_point(Bool(1-s1.orient), vertices(cell)[1][2-s1.orient], c)
            p2 = create_right_point(Bool(1-s1.orient), vertices(cell)[1][2-s1.orient] + cell.boundary.widths[1], c)
            if OrientedZone(s1, p1) && OrientedZone(s1, p2) && OrientedZone(s2, p1) && OrientedZone(s2, p2)
                if (p1[1]+p2[1])/2 != cell.boundary.origin[1]+cell.boundary.widths[1] && (p1[2]+p2[2])/2 != cell.boundary.origin[2]+cell.boundary.widths[2]

                    push!(output,p1)
                    push!(output,p2)
                end
                return output
            end
        end
    else # segments perpendicular
        if s1.orient == 0
            a = s2.aff
            b = s1.aff
        else
            a = s1.aff
            b = s2.aff
        end

        c = 0
        x = 0.0
        y = 0.0
        if  (s1.v1-s1.v2)*(s2.v1-s2.v2) < 0 # y = x+b-a  (1rst and 3rd quadrant)
            
            c = 0
        else # y = -x+b+a  (2nd and 4rth quadrant)
            
            c = 1
        end
        x0 = vertices(cell)[1][1]
        y0 = vertices(cell)[1][2]

        y = y0
        if c == 0 # 1-3 quadrants

          
            int1 = f_1_3(y, a, b, 2)

            if x0 <= int1 && int1 <= x0 + cell.boundary.widths[1] #it intersects faces 1-2
                x = x0 + cell.boundary.widths[1]
                int2 = f_1_3(x, a, b, 1)

                p1 = (int1,y0)
                p2 = (x, int2)

                if OrientedZone(s1, p1) &&  OrientedZone(s2, p1) && ((Zone(s3, p1) && dist_from_seg(s3,p1) >= dist_from_seg(s1,p1)) || !Zone(s3, p1))
                    push!(output, p1)
                end
                if OrientedZone(s1, p2) &&  OrientedZone(s2, p2) && ((Zone(s3, p2) && dist_from_seg(s3,p2) >= dist_from_seg(s1,p2)) || !Zone(s3, p2))
                    push!(output, p2)
                end

                if x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1]
                    if (a,b) !=p1 && (a,b)!=p2
                     push!(output, (a,b))
                    end
                end

           else  #see if it intersects faces 3-4

            y = y0 +cell.boundary.widths[1]
            int1 = f_1_3(y, a, b, 2)

            if x0 <= int1 && int1 <= x0 + cell.boundary.widths[1] #it intersects faces 3-4
                x = x0
                int2 = f_1_3(x, a, b, 1)

                p1 = (int1,y0+ cell.boundary.widths[1])
                p2 = (x, int2)

                if OrientedZone(s1, p1) &&  OrientedZone(s2, p1) && ((Zone(s3, p1) && dist_from_seg(s3,p1) >= dist_from_seg(s1,p1)) || !Zone(s3, p1))
                    push!(output, p1)
                end
                if OrientedZone(s1, p2) &&  OrientedZone(s2, p2) && ((Zone(s3, p2) && dist_from_seg(s3,p2) >= dist_from_seg(s1,p2)) || !Zone(s3, p2))
                    
                    push!(output, p2)
                end

                if x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1]
                    if (a,b) !=p1 && (a,b)!=p2
                     push!(output, (a,b))
                    end
                end
           end
       end # end of subcase with intersections of faces1-2 , 3-4
        else # c ==1
            
            int1 = f_2_4(y, a, b, 1)

            if x0 <= int1 && int1 <= x0 + cell.boundary.widths[1] #it intersects faces 1-4
                x = x0
                #int2 = eval(expr[2])
                int2 = f_2_4(x, a, b, 2)

                p1 = (int1,y0)
                p2 = (x, int2)

                if OrientedZone(s1, p1) &&  OrientedZone(s2, p1) && ((Zone(s3, p1) && dist_from_seg(s3,p1) >= dist_from_seg(s1,p1)) || !Zone(s3, p1))
                    push!(output, p1)
                end
                if OrientedZone(s1, p2) &&  OrientedZone(s2, p2) && ((Zone(s3, p2) && dist_from_seg(s3,p2) >= dist_from_seg(s1,p2)) || !Zone(s3, p2))
                    push!(output, p2)
                end

                if x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1]
                    if (a,b) !=p1 && (a,b)!=p2
                     push!(output, (a,b))
                    end
                end

            else  #i must see if it intersects faces 2-3

                x = x0 +cell.boundary.widths[1]
                #int1 = eval(expr[2])
                int1 = f_2_4(x, a, b, 2)

                if y0 <= int1 && int1 <= y0 + cell.boundary.widths[1] #it intersects faces 3-2
                    y = y0 + cell.boundary.widths[1]
                    #int2 = eval(expr[1])
                    int2 = f_2_4(y, a, b, 1)

                    p1 = (x0+ cell.boundary.widths[1], int1)
                    p2 = (int2,y)

                    if OrientedZone(s1, p1) &&  OrientedZone(s2, p1) && ((Zone(s3, p1) && dist_from_seg(s3,p1) >= dist_from_seg(s1,p1)) || !Zone(s3, p1))
                        push!(output, p1)
                    end
                    if OrientedZone(s1, p2) &&  OrientedZone(s2, p2) && ((Zone(s3, p2) && dist_from_seg(s3,p2) >= dist_from_seg(s1,p2)) || !Zone(s3, p2))
                        push!(output, p2)
                    end

                    if x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1]
                        if (a,b) != p1 && (a,b) != p2
                         push!(output, (a,b))
                     end
                    end
                end
            end # end of subcases with intesections of faces 2-3 or 1-4
       end  # end of if c==0 or 1
    end  # end of if that checks th eorientations of s1,s2
    return output
end

function find_vor_vertex(m::Mesh, cell::Cell, t1::Tuple{Int64, Int64}, t2::Tuple{Int64, Int64}, t3::Tuple{Int64, Int64}, E::Vector{Vector{MySegment}})
    # intersect the bisectors (s1,s2) and (s1, s3)
    # bis of (s1, s2): a1y+b1x+c1=0

    # first,check if indeed two different bisectors will occur

    s1 = E[t1[1]][t1[2]]
    s2 = E[t2[1]][t2[2]]
    s3 = E[t3[1]][t3[2]]

    if s2.orient == s3.orient && s3.aff == s2.aff
        t = s1
        s1 = s2
        s2 = t
    end

    a1 = Float64
    b1 = Float64
    c1 = Float64
    a2 = Float64
    b2 = Float64
    c2 = Float64
    if s1.orient == s2.orient && s1.aff != s2.aff# sites are parallel
        c1 = -s1.aff/2 - s2.aff/2
        if  s1.orient == 0
            a1 = 1.0
            b1 = 0.0
        else
            a1 = 0.0
            b1 = 1.0
        end
    elseif s1.orient == s2.orient && s1.aff == s2.aff
        d = min(abs(s1.v1-s2.v1), abs(s1.v1-s2.v2), abs(s1.v2-s2.v1), abs(s1.v2-s2.v2))
        if abs(s1.v1-s2.v1) == d
            c1 = -min(s1.v1,s2.v1) - d/2
        elseif abs(s1.v1-s2.v2)==d
            c1 = -min(s1.v1,s2.v2) - d/2
        elseif abs(s1.v2-s2.v1) == d
            c1 = -min(s1.v2,s2.v1) - d/2
        elseif abs(s1.v2-s2.v2) == d
            c1 = -min(s1.v2,s2.v2) - d/2
        end
        if  s1.orient == 1
            a1 = 1.0
            b1 = 0.0
        else
            a1 = 0.0
            b1 = 1.0
        end
    else # sites are perpendicular
        if s1.orient == 0
            a = s2.aff
            b = s1.aff
        else
            a = s1.aff
            b = s2.aff
        end

        if  (s1.v1-s1.v2)*(s2.v1-s2.v2) < 0 # y - x-b+a =0 (1rst and 3rd quadrant)
            a1 = 1.0
            b1 = -1.0
            c1 = -b+a
        else # y +x-b-a =0 (2nd and 4rth quadrant)
            a1 = 1.0
            b1 = 1.0
            c1 = -b -a
        end
    end

    #coefficients of the(s1, s3) bisector
    if s1.orient == s3.orient && s1.aff != s3.aff # sites are parallel
        c2 = -s1.aff/2 - s3.aff/2
        if  s1.orient == 0
            a2 = 1.0
            b2 = 0.0
        else
            a2 = 0.0
            b2 = 1.0
        end

    elseif s1.orient == s3.orient && s1.aff == s3.aff
        d = min(abs(s1.v1-s3.v1), abs(s1.v1-s3.v2), abs(s1.v2-s3.v1), abs(s1.v2-s3.v2))
        if abs(s1.v1-s3.v1) == d
            c2 = -min(s1.v1,s3.v1) - d/2
        elseif abs(s1.v1-s3.v2)==d
            c2 = -min(s1.v1,s3.v2) - d/2
        elseif abs(s1.v2-s3.v1) == d
            c2 = -min(s1.v2,s3.v1) - d/2
        elseif abs(s1.v2-s3.v2) == d
            c2 = -min(s1.v2,s3.v2) - d/2
        end
        if  s1.orient == 1
            a2 = 1.0
            b2 = 0.0
        else
            a2 = 0.0
            b2 = 1.0
        end
    else # sites are perpendicular

        if s1.orient == 0
            a = s3.aff
            b = s1.aff
        else
            a = s1.aff
            b = s3.aff
        end

        if  (s1.v1-s1.v2)*(s3.v1-s3.v2) < 0 # y - x-b+a =0 (1rst and 3rd quadrant)
            a2 = 1.0
            b2 = -1.0
            c2 = - b + a
        else # y +x-b-a =0 (2nd and 4rth quadrant)
            a2 = 1.0
            b2 = 1.0
            c2 = - b - a
        end
    end

    # now i determine the coordinates of the voronoi vertex

    if a1 == 0 && b1 !=0
        x = - c1 / b1
        y = ( - c2 - b2 * x )  / a2
    elseif a1 != 0 && b1 == 0
        y = -c1/a1
        x = ( - c2 - a2 * y ) / b2
    elseif a1 !=0 && b1 !=0
        x = ( - c2 +  a2 * c1  / a1 ) / ( b2 -  a2 * b1  / a1 )
        y = ( - b1 * x - c1 ) / a1
    end

    push_vertex!(m, [x, y, 0.0])
    index = Int(length(m.points)/3)
    output = ((x,y), [t1, t2, t3], Int(length(m.points)/3))
    # check for corner existence inside the cell
    x0 = vertices(cell)[1][1]
    y0 = vertices(cell)[1][2]

    # see if there i a corner inside the cell
    if s1.orient != s2.orient
        if s1.orient == 0
            a = s2.aff
            b = s1.aff
        else
            a = s1.aff
            b = s2.aff
        end
        if x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1] && on_seg((a,b), s1) && on_seg((a,b), s2)
            push_vertex!(m, [a,b,0.0])
            push_edge!(m, [index, Int(length(m.points)/3)])
        end
    end

    if s1.orient != s3.orient
        if s1.orient == 0
            a = s3.aff
            b = s1.aff
        else
            a = s1.aff
            b = s3.aff
        end
        if x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1] && on_seg((a,b), s1) && on_seg((a,b), s3)
            push_vertex!(m, [a,b,0.0])
            push_edge!(m, [index, Int(length(m.points)/3)])
        end
    end

    if s3.orient != s2.orient
        if s3.orient == 0
            a = s2.aff
            b = s3.aff
        else
            a = s3.aff
            b = s2.aff
        end
        if x0 <= a && a <= x0 + cell.boundary.widths[1] && y0 <= b && b <= y0 + cell.boundary.widths[1] && on_seg((a,b), s2) && on_seg((a,b), s3)
            push_vertex!(m, [a,b,0.0])
            push_edge!(m, [index, Int(length(m.points)/3)])
        end
    end

    return output
end

#------------------------------------------------
function faceProc(m::Mesh, cell::Cell, E::Vector{Vector{MySegment}})
        if !isleaf(cell)
                faceProc(m,children(cell)[1],E)
                faceProc(m,children(cell)[2],E)
                faceProc(m,children(cell)[3],E)
                faceProc(m,children(cell)[4],E)

                edgeProcH(m,children(cell)[1], children(cell)[2])
                edgeProcH(m,children(cell)[3], children(cell)[4])

                edgeProcV(m,children(cell)[1], children(cell)[3])
                edgeProcV(m,children(cell)[2], children(cell)[4])
        else
                place_vertices!(m, cell, E)
        end
end

function edgeProcH(m::Mesh, cell1::Cell, cell2::Cell)
        if !isleaf(cell1) && !isleaf(cell2)
                edgeProcH(m,children(cell1)[2], children(cell2)[1])
                edgeProcH(m,children(cell1)[4], children(cell2)[3])
        elseif isleaf(cell1) && !isleaf(cell2)
                edgeProcH(m,cell1, children(cell2)[1])
                edgeProcH(m,cell1, children(cell2)[3])
        elseif !isleaf(cell1) && isleaf(cell2)
                edgeProcH(m,children(cell1)[2], cell2)
                edgeProcH(m,children(cell1)[4], cell2)
        else # isleaf(cell1) && isleaf(cell2)
                dual_edge!(m, cell1, cell2)
                return
        end
end

function edgeProcV(m::Mesh, cell1::Cell, cell2::Cell)
        if !isleaf(cell1) && !isleaf(cell2)
                edgeProcV(m,children(cell1)[3], children(cell2)[1])
                edgeProcV(m,children(cell1)[4], children(cell2)[2])
        elseif isleaf(cell1) && !isleaf(cell2)
                edgeProcV(m,cell1, children(cell2)[1])
                edgeProcV(m,cell1, children(cell2)[2])
        elseif !isleaf(cell1) && isleaf(cell2)
                edgeProcV(m,children(cell1)[3], cell2)
                edgeProcV(m,children(cell1)[4], cell2)
        else # isleaf(cell1) && isleaf(cell2)
                dual_edge!(m, cell1, cell2)
                return count
        end
end

function dual_edge!(m::Mesh, cell1::Cell, cell2::Cell)
    if length(cell1.data.points) ==0 || length(cell2.data.points)==0
        return
    end

    for i = 1:length(cell1.data.points)
        for j = 1:length(cell2.data.points)
            if  subset(cell1.data.points[i][2], cell2.data.points[j][2]) || subset(cell2.data.points[j][2], cell1.data.points[i][2])
                    push_edge!(m, [cell1.data.points[i][3], cell2.data.points[j][3]])
                
            #  connection between vertices
           elseif length(cell1.data.points[i][2]) >=3 && length(cell2.data.points[j][2]) >= 3 && length(intersect(cell1.data.points[i][2], cell2.data.points[j][2])) >= 2
                 push_edge!(m, [cell1.data.points[i][3], cell2.data.points[j][3]])
            end
        #-----------------------------------------------------
        end #end for
    end # end for
end #end function


#---------------------Exported functions----------------------------------------
function reconstruct!(root::Cell, E::Vector{Vector{MySegment}})
    m = mesh(Float64)
   tic()
    faceProc(m, root, E)
   toc()
   return m
end

function vis_cell(cell::Cell)
           m = mesh(cell)
           if cell.data.out == 1
               m[:color]=Color(0,0,0)
               m[:size] = 0.1
           elseif cell.data.in == 1
               m[:color]=Color(0, 140, 0)
               m[:size] = 0.1
           elseif length(cell.data.int)+length(cell.data.near) <=3
                m[:color] = Color(255,0,0)
                m[:size] = 0.1
            else
                m[:color] = Color(255,127,80)
                m[:size] = 0.3
            end # end of if that checks if cell.data.in = 0 or 1 and cell.data.out

         m[:opacity] = 0.002
         return m
end

function vis_face(f::Array{Tuple{Float64,Float64}})
        polygon_verts = [[f[i][1], f[i][2], 0.0] for i in 1:length(f)]
        polygon_edges = push!([[i,i+1] for i in 1:length(f)-1], [length(f), 1])
        pol  = mesh(polygon_verts, polygon_edges)
        pol[:color] = Color(0,0,0)
        pol[:size] = 0.3
        return pol
end

function vis_voronoi(m::Mesh)
    m[:color] = Color(0,0,255)
    m[:size] = 0.2
    return m
end
