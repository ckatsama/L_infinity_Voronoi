# LinfVoronoi.jl: Voronoi Diagram of Orthogonal Objects in 2D and 3D using the max-norm


We consider collections of axis-aligned orthogonal polyhedra in 2 and 3 dimensions, not necessarily convex, under the max-norm.
We construct the Voronoi diagram among them and within a bounding box or, equivalently, inside such a polyhedron with holes that may be defined by such polyhedra.

For theoretical details on the algorithm see: **https://arxiv.org/abs/1905.08691**
### Dependencies: 
RegionTrees.jl, StaticArrays.jl, SemiAlgebraicTypes.jl, Axl

### To install the package within Julia:
```julia
using Pkg
Pkg.clone("https://gitlab.inria.fr/ckatsama/L_infinity_Voronoi.git")
```

# Usage for 2D input
Example of usage for a simple orthogonal polygon in 2D `/2dexamples/polygon1.jl`:

(See in `/2dexamples` folder for more input polygons) 
```julia
using Linf_voronoi
global K = 2 # number of different closed polygon contours
F =  Vector{Array{Tuple{Float64,Float64}}}(K) # vector to store all the contours

# outer contour is CCW orientd    
F[1] =[(0.0, 0.0),  (6.0, 0.0),  (6.0, 1.0),  (5.0, 1.0), (5.0, 3.0),  (8.0, 3.0),  (8.0, -1.0),
       (12.0, -1.0), (12.0, 1.0),  (11.0, 1.0),  (11.0, 5.0), (3.0, 5.0), (3.0, 2.0),  (1.0, 2.0),
       (1.0, 8.0),  (7.0, 8.0),  (7.0, 10.0),  (0.0, 10.0)] 
# inner contours are CW oriented
F[2] = [(10.0, 2.5),(8.75, 2.5),  (8.75, 4.0),  (10.0, 4.0)]
    
E = Vector{Vector{MySegment}}(K) # vector to store Voronoi sites (polygon edges)
c = 0
for i=1:K
    E[i] = getEdges(F[i],c)
    c += length(E[i])
end

# specify minimum cell size (side length)
# if chosen sufficiently small, algorithm will terminate before reaching
# this cell size
r = MyRefinery(0.0005)

# plane subdivision
root = subdivide!(E, r)

# m is a mesh representing the Voronoi diagram
m = reconstruct!(root, E)

# visualisation:
@axl start

# 1) of the subdivision cells
for c in allleaves(root)
    @axl vis_cell(c)
end 

# 2) of the polygon
for f in F
    @axl vis_face(f)
end

# 3) of the Voronoi diagram
@axl vis_voronoi(m)

@axl view
```   
**Output:**

    elapsed time: 0.007105812 seconds

    elapsed time: 0.002036695 seconds
    
(First time meter refers to the subdivision, and second to the reconstruction phase)

![ex3](/uploads/965809e75df221fe848ca14aa8338521/ex3.PNG)

# Alternate version

Decomposes the polygon to rectangles and constructs a Bounding Volume Hierarchy (BVH) on the rectangles of the decomposition.
This decomposition is used during the subdivision to determine the Voronoi regions that intersect each subdivision cell.
Preferably used when input polygon consists of more than 500 edges.

One can also use a combination of the two versions: BVH is used as long as the number 
of Voronoi regions intersected by a subdivision cell is more than k, where k is user-specified parameter.

```julia
using Linf_voronoi
global K = 2 # number of different closed polygon contours
F =  Vector{Array{Tuple{Float64,Float64}}}(K) # vector to store all the contours

# outer contour is CCW orientd    
F[1] =[(0.0, 0.0),  (6.0, 0.0),  (6.0, 1.0),  (5.0, 1.0), (5.0, 3.0),  (8.0, 3.0),  (8.0, -1.0),
       (12.0, -1.0), (12.0, 1.0),  (11.0, 1.0),  (11.0, 5.0), (3.0, 5.0), (3.0, 2.0),  (1.0, 2.0),
       (1.0, 8.0),  (7.0, 8.0),  (7.0, 10.0),  (0.0, 10.0)] 
# inner contours are CW oriented
F[2] = [(10.0, 2.5),(8.75, 2.5),  (8.75, 4.0),  (10.0, 4.0)]
    
E = Vector{Vector{MySegment}}(K) # vector to store Voronoi sites (polygon edges)
c = 0
for i=1:K
    E[i] = getEdges(F[i],c)
    c += length(E[i])
end

# Polygon Decomposition and BVH

root2 = decomposition!(F,E)


# specify minimum cell size (side length)
# if chosen sufficiently small, algorithm will terminate either way
# before reaching this cell size
r = MyRefinery(0.0005)

# plane subdivision
# the BVH is used if and only if the number of active sites is more than num
# otherwise, the standard method is used
num = 1
root = subdivide2!(E, r, root2, num)



# m is a mesh representing the Voronoi diagram
m = reconstruct!(root, E)

# visualisation:
@axl start

# 1) of the subdivision cells
for c in allleaves(root)
    @axl vis_cell(c)
end 

# 2) of the polygon
for f in F
    @axl vis_face(f)
end

# 3) of the Voronoi diagram
@axl vis_voronoi(m)

@axl view
```

# Gallery

![ex1](/uploads/c2527466f0dc14a9b026be4900577805/ex1.PNG)

![ex7](/uploads/1b69211c6e79d54c24d0439c13776e6b/ex7.PNG)

![ex2](/uploads/7c67c31bac2ebb86400d76cefad02ebd/ex2.PNG)

# Usage for 3D input
Example of usage for a simple orthogonal polyhedron in 3D `/3dexamples/polyhedron1.jl`:
```julia
using Linf_voronoi

F = Linf_voronoi.Face[]

d = [ 0., 13., 25., 0., 0., 10., 5., 8., 12., 5., 5., 8. ]
f1 =  [ (0., 0.), (10., 0.), (10., 25.), (0., 25.)]
f2 = [ (0., 0.), (10., 0.), (10., 25.), (0., 25.)]
f3 = [ (0., 0.), (10., 0.), (10., 13.), (0., 13.)]
f4 = [ (0., 0.), (10., 0.), (10., 13.), (0., 13.)]
f5 = [  (0., 0.), (25., 0.), (25., 13.),(0., 13.)]
f6 = [  (0., 0.), (25., 0.), (25., 13.),(0., 13.)]
f7=  [ (5., 5.), (8., 5.), (8., 12.), (5., 12.)]
f8 = [ (5., 5.), (8., 5.), (8., 12.), (5., 12.)]
f9 = [ (5., 5.), (8., 5.), (8., 8.), (5., 8.)]
f10 = [ (5., 5.), (8., 5.), (8., 8.), (5., 8.)]
f11 = [  (5., 5.), (12., 5.), (12., 8.),(5., 8.)]
f12 = [  (5., 5.), (12., 5.), (12., 8.),(5., 8.)]
push!(F, Linf_voronoi.Face(f1, 3, 1, d[1]))
push!(F, Linf_voronoi.Face(f2, 3, -1, d[2]))
push!(F, Linf_voronoi.Face(f3, 2, -1, d[3]))
push!(F, Linf_voronoi.Face(f4, 2, 1, d[4]))
push!(F, Linf_voronoi.Face(f5, 1, 1, d[5]))
push!(F, Linf_voronoi.Face(f6, 1, -1, d[6]))
push!(F, Linf_voronoi.Face(f7, 3, -1, d[7]))
push!(F, Linf_voronoi.Face(f8, 3, 1, d[8]))
push!(F, Linf_voronoi.Face(f9, 2, 1, d[9]))
push!(F, Linf_voronoi.Face(f10, 2, -1, d[10]))
push!(F, Linf_voronoi.Face(f11, 1, -1, d[11]))
push!(F, Linf_voronoi.Face(f12, 1, 1, d[12]))


r = MyRefinery(0.05)
root = subdivide!(F,r)

m = mesh(Float64)

cellProc(m, root, F)

@axl start
@axl m


for f in F
    @axl poly_facet(f)
end

@axl view
```

# Gallery

![capt3d](/uploads/9f9ece427bc158c3d0724961908e389f/capt3d.PNG)


